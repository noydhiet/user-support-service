/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 7/11/2019, 6:28 PM
 */


const technicianService = require('../bin/services/technicianService');

let chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const log4js = require('log4js');
const logger = log4js.getLogger('Test');
logger.level = 'info';

const {dateFrom, dateTo, sto, nDays} = require('./testData/technicianService/avaiablityData');
const bookingData = require('./testData/technicianService/bookingData');
const {COMPLETED, IN_PROGRESS} = require('../bin/config/const/technicianStatus');

describe('Technician Appointment', () => {
    describe('Appointments', () => {
        it('Should Get available time slots for the installation appointment', function () {
            this.timeout(100000);
            let availability = technicianService.getAvailableInstallationTimes(7, sto);

            return availability.then(times => {
                expect(times).not.to.be.empty;
                logger.info('time slots count: ', times.length);
            });
        });

        it('Should Get available time slots for the service appointment', function () {
            this.timeout(100000);
            let availability = technicianService.getAvailableServiceTimes(nDays, sto);

            return availability.then(times => {
                expect(times).not.to.be.empty;
                logger.info('time slots count: ', times.length);
            });
        });

        it('Should Get available time slots for the available installation appointment and reserve one', function () {
            this.timeout(100000);
            let availability = technicianService.getAvailableInstallationTimes(7, sto);

            return availability.then(times => {
                expect(times).not.to.be.empty;
                let availableTimes = times.filter(t => t['availability']);
                expect(availableTimes).not.to.be.empty;
                let selectedTime = availableTimes[0];
                logger.info('selected time: ', selectedTime);
                technicianService.reserveInstallationAppointment({userId: ''}, selectedTime.bookingId).then(
                    status => {
                        expect(status['status']).not.to.be.null;
                        expect(status['bookingId']).to.be.equal(selectedTime.bookingId);
                    }
                );
            });

        });

        it('Should Get available time slots for the available installation appointment and reserve one and update ' +
                'the statues and get the updated status', function () {
            this.timeout(100000);
            let availability = technicianService.getAvailableInstallationTimes(7, sto);

            return availability.then(
                times => {
                    expect(times).not.to.be.empty;
                    let availableTimes = times.filter(t => t['availability']);
                    expect(availableTimes).not.to.be.empty;
                    let selectedTime = availableTimes[0];
                    logger.info('selected time: ', selectedTime);
                    let selectedBookingId = selectedTime.bookingId;
                    const testUserId = 'test-user-1';
                    return technicianService.reserveInstallationAppointment({userId: testUserId}, selectedBookingId).then(
                        status => {
                            expect(status['status']).not.to.be.null;
                            expect(status['bookingId']).to.be.equal(selectedTime.bookingId);
                            const selectedTransactionId = status['transactionId'];
                            expect(selectedTransactionId).not.to.be.null;

                            return technicianService.updateAppointmentStatus(selectedBookingId, IN_PROGRESS.status).then(
                                inProgressStatus => {
                                    expect(inProgressStatus['status']).to.be.equal(IN_PROGRESS.status);
                                    expect(inProgressStatus['bookingId']).to.be.equal(selectedTime.bookingId);

                                    return technicianService.getAppointmentStatus(testUserId, selectedBookingId).then(
                                        bookings => {
                                            let {appointments} = bookings;
                                            expect(appointments).not.to.be.empty;
                                            logger.info('updated appointment', appointments[0]);

                                            return technicianService.getTechnicianInformation(selectedBookingId).then(
                                                technician => {
                                                    expect(technician).not.to.be.null;
                                                    logger.info('data: ', technician);
                                                    expect(technician['displayname']).not.to.be.null;
                                                    expect(technician['email']).not.to.be.null;
                                                    expect(technician['phone']).not.to.be.null;

                                                    //  close the appointment
                                                    return technicianService.updateAppointmentStatus(selectedBookingId, COMPLETED.status).then(
                                                        competedStatus => {
                                                            expect(competedStatus['status']).to.be.equal(COMPLETED.status);
                                                            expect(competedStatus['bookingId']).to.be.equal(selectedTime.bookingId);

                                                            return technicianService.closeBooking(selectedBookingId, testUserId, true)
                                                                .then(closedStatus => {
                                                                    expect(closedStatus).not.to.be.null;
                                                                    expect(closedStatus.data).not.to.be.null;
                                                                });
                                                        });
                                                });
                                        });
                                });
                        });
                });
        });

        it('Should Get same-day early available booking slots for given booking', function () {
            this.timeout(100000);
            let slots = technicianService.getEarlyInstallationTimes(bookingData.bookingIds.currentBookingId);

            return slots.then(times => {
                expect(times).not.to.be.empty;
            });
        });

        it('Should not get any more early available booking slots for given Valid bookingId', function () {
            this.timeout(100000);
            let slots = technicianService.getEarlyInstallationTimes(bookingData.bookingIds.newBookingId);

            return slots.catch(times => {
                expect(times).to.be.empty;
            });
        });

        it('Should not get early available booking slots for given Invalid bookingId', function () {
            this.timeout(100000);
            let slots = technicianService.getEarlyInstallationTimes(bookingData.bookingIds.invalidBookingId);

            return slots.catch(times => {
                expect(times).to.be.empty;
            });
        });

        it('Should Reschedule installation booking successfully', function () {
            this.timeout(100000);
            let slots = technicianService.rescheduleInstallationBooking(bookingData.reschedule.newBookingId, bookingData.reschedule.userId);

            return slots.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should Not Reschedule installation booking. Max attempts reached', function () {
            this.timeout(100000);
            let slots = technicianService.rescheduleInstallationBooking(bookingData.reschedule.newBookingId, bookingData.reschedule.userId);

            return slots.catch(err => {
                expect(err).to.be.empty;
            });
        });

        it('Should Fail to Reschedule installation booking. Invalid params', function () {
            this.timeout(100000);
            let slots = technicianService.rescheduleInstallationBooking(bookingData.reschedule.newBookingId, null);

            return slots.catch(err => {
                expect(err).to.be.empty;
            });
        });

        it('Should Send early installation notification to user successfully', function () {
            this.timeout(100000);
            let resp = technicianService.sendEarlyInstallationRequest(bookingData.earlyRescheduleNotification.bookingId,
                bookingData.earlyRescheduleNotification.dateTime);

            return resp.then(data => {
                expect(data).not.to.be.empty;
                expect(data.message).not.to.be.empty;
            });
        });

        it('Should fail to Send early installation notification to user. Empty params', function () {
            this.timeout(100000);
            let resp = technicianService.sendEarlyInstallationRequest(bookingData.earlyRescheduleNotification.bookingId);

            return resp.catch(err => {
                expect(err).to.be.empty;
            });
        });

        it('Should fail to Send early installation notification to user.  Invalid params', function () {
            this.timeout(100000);
            let resp = technicianService.sendEarlyInstallationRequest(bookingData.earlyRescheduleNotification.bookingId,
                bookingData.earlyRescheduleNotification.invalidDateTime);

            return resp.catch(err => {
                expect(err).to.be.empty;
            });
        });

        it('Should Reschedule early installation successfully', function () {
            this.timeout(100000);
            let resp = technicianService.rescheduleEarlyInstallationBooking(bookingData.earlyRescheduleRequest, bookingData.userId);

            return resp.then(data => {
                expect(data).not.to.be.empty;
                expect(data.message).not.to.be.empty;
                expect(data.schedule).not.to.be.empty;
            });
        });

        it('Should fail to Reschedule early installation. Invalid request body', function () {
            this.timeout(100000);
            //alter request body
            bookingData.earlyRescheduleRequest.schedule = null;
            let resp = technicianService.rescheduleEarlyInstallationBooking(bookingData.earlyRescheduleRequest, bookingData.userId);

            return resp.catch(err => {
                expect(err).to.be.empty;
            });
        });

        it('Should fail to Reschedule early installation. Invalid schedule data in request body', function () {
            this.timeout(100000);
            //alter request body
            bookingData.earlyRescheduleRequest.schedule = '2019-12-22T11:00:00.000Z';
            let resp = technicianService.rescheduleEarlyInstallationBooking(bookingData.earlyRescheduleRequest, bookingData.userId);

            return resp.catch(err => {
                expect(err).to.be.empty;
            });
        });
    });
}
);
