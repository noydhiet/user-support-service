/**
 * Created by nishanif on 13/11/19
 */

/**
 * Test data for support Issue reporting tests
 */
module.exports = {
    selectedIndiHomeNum: '122604200712',
    language: {
        EN: 'en',
        ID: 'id'
    },
    ticketData: {
        "issueId": "issue_tv_03_en",
        "categoryId": "cat_02_en",
        "message": "test issue"
    },
    fisikTicketData: {
        "issueId": "issue_installation_03_en",
        "categoryId": "cat_05_en",
        "message": "fisik test issue",
        "bookingId": "1578290689336_6"
    },
    userData: {
        email: "pccw@dev.com",
        userId: "5e05772ee7c2d70012408d09",
        userRole: [
            "generalUser"
        ],
        mobile: "123213124",
        iat: 1573634638,
        exp: 1573638238
    },
    transactionId: "MYINX-1577693074804",
    newTransactionId: "MYINX-1577693074800",
    ticketId: "MYINX-1577693074804",
    ticketStatus: "SOLVED",
    openStatus: true,
    invalidTicketId: 'invalidticketqwe123',
    ticketAction : {
        reopen: 'REOPEN',
        closed: 'CLOSED'
    },
    updateIncident: {
        bookingId: '1577327543350_2'
    },
    hardware: {
        indiHomeNum: '02129668329',
        invalidIndiHomeNum: 'invalid123',
        iBooster: {
            indiHomeNum: '122604200712',
            service: 'VOICE',
            invalidService: 'Invalid',
            apiData: {
                "id_ukur": "1577028591729756",
                "nas_ip": "172.16.252.76",
                "frame_ip": "110.138.138.90",
                "clid": "GPON02-D2-GBI-3 pon 0/1/03/3/24/3:3870",
                "type": "ZXPON C300v2",
                "shelf": "1",
                "slot": "3",
                "port": "3",
                "host_id": "392",
                "hostname": "172.28.116.10",
                "identifier": "GPON02-D2-GBI-3",
                "description": "GPON02-D2-GBI-3",
                "onu": "24",
                "vport": "3",
                "vendor_id": "ZTEG",
                "fiber_length": "164",
                "version_id": "V5.3",
                "desc_name": "ONU-3:24",
                "reg_type": "ZTEG-F609",
                "oper_status": "ONLINE",
                "admin_status": "ENABLE",
                "bw_up": "UP-2253KB0",
                "bw_down": "DOWN-11264KB0",
                "onu_pwr_spl": "3.22",
                "onu_bias_curr": "13.55",
                "onu_temp": "47.719",
                "onu_rx_pwr": "-24.318",
                "onu_tx_pwr": "1.95",
                "olt_tx_pwr": "3.346",
                "olt_rx_pwr": "-24.822",
                "olt_temp": "34.421",
                "olt_pwr_spl": "3.228",
                "olt_bias_curr": "27.071",
                "serial_number": "ZTEGC84838E4",
                "indikasi": "",
                "gangguan": "Logic",
                "desc": "",
                "suggestion": "",
                "status_cpe": "ONLINE",
                "tipejaringan": "Fiber",
                "status_koneksi": "Offline",
                "status_jaringan": "OK",
                "usage_download": "0.00 KB",
                "usage_upload": "0.00 KB",
                "session_start": "2019-12-20 15:23:44",
                "connection_status": "Stop",
                "ip_ams": "172.28.1.26",
                "log": "-Teknologi : Fiber\n                                -Rx Power  : -24.318 dBm\n\n                                OLT\n                                -Rx Power  : -24.822 dBm\n\n                                OLT\n                                -Hostname : GPON02-D2-GBI-3\n                                -IP       : 172.28.116.10\n                                -Port     : 1/3/3/24\n                                -Type       : ZTEG-F609\n                                -Status ONU : ONLINE\n                                -Fiber Length : 164"
            }
        }
    }
};