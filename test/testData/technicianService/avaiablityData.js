/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 19/11/2019, 5:06 PM
 */


const dateFrom = new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 30)).toISOString();
const dateTo = new Date(Date.parse(dateFrom) + (1000 * 60 * 60 * 24 * 7)).toISOString();
const sto = 'LBG';

module.exports = {
    dateFrom, dateTo, sto
};
