/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 25/11/19
*/
/**
 * Test data for technician re-schedule booking
 */
module.exports = {
    bookingIds: {
        currentBookingId: "1574664101128_1",
        newBookingId: "1574664101272_1",
        invalidBookingId: "1574524415501_0",
    },
    reschedule: {
        newBookingId: "1574664101272_2",
        userId: "5dcbc146024ae600194f0c85"
    },
    earlyRescheduleNotification: {
        bookingId: "1576644578982_2",
        dateTime: "2019-12-26T12:30:00.000Z",
        invalidDateTime: "2019-12-26T13:00:00.000Z"
    },
    earlyRescheduleRequest: {
        bookingId: "1576644578982_2",
        schedule: "2019-12-26T12:30:00.000Z"
    },
    userId: "5dcbc146024ae600194f0c85"
};
