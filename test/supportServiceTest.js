/**
 * Created by nishanif on 13/11/19
 */

const supportService = require('../bin/services/supportService');
const testData = require('./testData/supportServiceData/data');
let chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;

describe('Issue Reporting', function () {
    this.timeout(100000);
    let ticket = null;
    let ticketId = null;
    const userData = Object.assign(testData.userData, {});
    //generate new userId so that ticket creation will not fail due to having pending tickets
    userData.userId = new Date().getTime().toString();
    // create a ticket before
    before(() => {
        this.timeout(100000);
        supportService.resetModem(testData.selectedIndiHomeNum, testData.userData.userId);

        ticket = supportService.reportIssue(testData.selectedIndiHomeNum, testData.language.EN, testData.ticketData, testData.userData);

        return ticket.then(data => {
            ticketId = data.ticketId;
        });
    });
    describe('Create Support Ticket', () => {
        it('Should CREATE a Support Ticket successfully', function () {
            this.timeout(100000);

            return ticket.then(data => {
                expect(data).not.to.be.empty;
                expect(data.status).to.be.equal('success');
            });
        });

        it('Should FAIL to create a Support Ticket', function () {
            this.timeout(100000);
            let response = supportService.reportIssue(testData.selectedIndiHomeNum, testData.language.EN, testData.ticketData, null);

            return response.catch(status => {
                expect(status).to.be.empty;
            });
        });
    });

    describe('Get All Support Tickets', () => {
        it('Should fetch all Support Tickets successfully. Open: true', function () {
            this.timeout(100000);
            let response = supportService.getAllTickets(testData.openStatus);
            return response.then(data => {
                expect(data.data).not.to.be.empty;
            });
        });
    });

    describe('Get All Support Tickets by user', () => {
        it('Should fetch all Support Tickets successfully', function () {
            this.timeout(100000);
            let response = supportService.getAllTicketsByUserId(userData.userId, true);
            return response.then(data => {
                expect(data.data).not.to.be.equal([]);
            });
        });
    });

    describe('Ticket status Retrieval and Update', () => {

        it('Should fetch Tickets status data successfully', function () {
            this.timeout(100000);
            let response = supportService.getTicketStatus(ticketId);

            return response.then(data => {
                expect(data.data).not.to.be.empty;
                expect(data.data.ticketStatus).not.to.be.empty;
            });
        });

        it('Should update Tickets status successfully', function () {
            this.timeout(100000);
            let response = supportService.updateTicketStatus(ticketId, testData.ticketStatus);

            return response.then(data => {
                expect(data.data).not.to.be.empty;
            });
        });

        it('Should update Ticket Id successfully', function () {
            this.timeout(100000);
            let response = supportService.updateTicketId(testData.transactionId, testData.newTransactionId);
            return response.then(data => {
                expect(data.data).not.to.be.empty;
            });
        });
        it('Should fail to Ticket Id. Empty params', function () {
            this.timeout(100000);
            let response = supportService.updateTicketId(testData.transactionId, null);
            return response.catch(data => {
                expect(data.data).to.be.empty;
            });
        });
        it('Should fail to Ticket Id. Same ticketId exists', function () {
            this.timeout(100000);
            let response = supportService.updateTicketId(testData.transactionId, testData.transactionId);
            return response.catch(data => {
                expect(data.data).to.be.empty;
            });
        });
    });

    describe('Notify Support Ticket status', () => {
        it('Should notify ticket status successfully', function () {
            this.timeout(100000);
            supportService.notifyTicketStatus(ticketId, testData.userData.userId)
                .then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to notify ticket status. Empty params', function () {
            this.timeout(100000);
            let response = supportService.notifyTicketStatus(null, testData.userData.userId);

            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to notify ticket status. Invalid params', function () {
            this.timeout(100000);
            let response = supportService.notifyTicketStatus(testData.invalidTicketId, testData.userData.userId);

            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe('Close Support Ticket', () => {
        it('Should close ticket successfully', function () {
            this.timeout(100000);
            let response = supportService.reopenOrCloseFisikTicket(ticketId, testData.ticketAction.closed, userData.userId);

            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to close ticket. Empty params', function () {
            this.timeout(100000);
            let response = supportService.reopenOrCloseFisikTicket(null, testData.ticketAction.closed, userData.userId);

            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to close ticket. Invalid params', function () {
            this.timeout(100000);
            let response = supportService.reopenOrCloseFisikTicket(testData.invalidTicketId, testData.ticketAction.closed, userData.userId);

            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });

    describe('Reopen Support Ticket', () => {
        it('Should reopen ticket successfully', function () {
            this.timeout(100000);
            let response = supportService.reopenOrCloseFisikTicket(ticketId, testData.ticketAction.reopen, userData.userId);

            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });

        it('Should fail to reopen ticket. Empty params', function () {
            this.timeout(100000);
            let response = supportService.reopenOrCloseFisikTicket(null, testData.ticketAction.reopen, userData.userId);

            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });

        it('Should fail to reopen ticket. Invalid params', function () {
            this.timeout(100000);
            let response = supportService.reopenOrCloseFisikTicket(testData.invalidTicketId, testData.ticketAction.reopen, userData.userId);

            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });
});

describe('Reopen Fisik Support Ticket', function() {
    this.timeout(100000);
    let fisikTicket = supportService.reportIssue(testData.selectedIndiHomeNum, testData.language.EN, testData.fisikTicketData, testData.userData, false);
    it('Should reschedule/reopen ticket successfully', function () {
        this.timeout(100000);
        return fisikTicket.then(data => {
            this.timeout(100000);
            let response = supportService.reopenOrCloseFisikTicket(data.ticketId, testData.ticketAction.reopen, testData.userData.userId, testData.fisikTicketData.bookingId);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });
    });
    it('Should reschedule/reopen ticket successfully. Final Attempt', function () {
        this.timeout(100000);
        return fisikTicket.then(data => {
            this.timeout(100000);
            let response = supportService.reopenOrCloseFisikTicket(data.ticketId, testData.ticketAction.reopen, testData.userData.userId, testData.fisikTicketData.bookingId);
            return response.then(data => {
                expect(data).not.to.be.empty;
            });
        });
    });
    it('Should fail to reschedule/reopen ticket. Max Attempts reached', function () {
        this.timeout(100000);
        return fisikTicket.catch(data => {
            this.timeout(100000);
            let response = supportService.reopenOrCloseFisikTicket(data.ticketId, testData.ticketAction.reopen, testData.userData.userId, testData.fisikTicketData.bookingId);
            return response.catch(data => {
                expect(data).to.be.empty;
            });
        });
    });
    it('Should fail to reschedule/reopen ticket. Empty params', function () {
        this.timeout(100000);
        let response = supportService.reopenOrCloseFisikTicket(null, testData.ticketAction.reopen, testData.userData.userId, testData.fisikTicketData.bookingId);
        return response.catch(data => {
            expect(data).to.be.empty;
        });
    });

    it('Should fail to reschedule/reopen ticket. Invalid params', function () {
        this.timeout(100000);
        let response = supportService.reopenOrCloseFisikTicket(testData.invalidTicketId, testData.ticketAction.reopen, testData.userData.userId, testData.fisikTicketData.bookingId);

        return response.catch(data => {
            expect(data).to.be.empty;
        });
    });
});

describe('Hardware status check', () => {
    describe('CableTye check', ()=> {
        it('should successfully reset modem', function () {
            this.timeout(100000);
            return supportService.checkCableType(testData.hardware.indiHomeNum, testData.hardware.iBooster.service).then(data => {
                expect(data).not.to.be.empty;
            });

        });
        it('should fail to reset modem. Empty params', function () {
            this.timeout(100000);
            return supportService.checkCableType(null, testData.hardware.iBooster.service).catch(data => {
                expect(data).to.be.empty;
            });

        });
        it('should fail to reset modem. Invalid params', function () {
            this.timeout(100000);
            return supportService.checkCableType(testData.hardware.indiHomeNum, testData.hardware.iBooster.invalidService).catch(data => {
                expect(data).to.be.empty;
            });

        });
    });

    describe('IBooster check', ()=> {
        it('should successfully get iBooster info', function () {
            this.timeout(100000);
            return supportService.getIboosterInfo(testData.hardware.iBooster.indiHomeNum).then(data => {
                expect(data).not.to.be.empty;
            });

        });
        it('should fail to get iBooster info. Empty params', function () {
            this.timeout(100000);
            return supportService.getIboosterInfo(null).catch(data => {
                expect(data).to.be.empty;
            });

        });
        it('should fail to get iBooster info. Invalid params', function () {
            this.timeout(100000);
            return supportService.getIboosterInfo(testData.hardware.invalidIndiHomeNum).catch(data => {
                expect(data).to.be.empty;
            });

        });
    });

    describe('Self troubleshooting Support', () => {
        it('should successfully reset modem', function () {
            this.timeout(100000);
            let response =  supportService.resetModem(testData.hardware.indiHomeNum, testData.userData.userId);
            response.then(data => {
                expect(data).not.to.be.empty;
            });

        });
        it('should fail to reset modem. Empty params', function () {
            this.timeout(100000);
            let response = supportService.resetModem(null, testData.userData.userId);
            response.catch(data => {
                expect(data).to.be.empty;
            });

        });
        it('should fail to reset modem. Invalid params', function () {
            this.timeout(100000);
            let response = supportService.resetModem(testData.hardware.invalidIndiHomeNum, testData.userData.userId);
            response.catch(data => {
                expect(data).to.be.empty;
            });
        });
        it('should successfully get modem reset status', function () {
            this.timeout(100000);
            const status =  supportService.checkACSResetStatus(testData.hardware.indiHomeNum);
            status.then(data => {
                expect(data).to.be.equal(true);
            });
        });
        it('should fail get modem reset status. Empty params', function () {
            this.timeout(100000);
            return supportService.checkACSResetStatus(null).catch(data => {
                expect(data).to.be.empty;
            });

        });
        it('should fail to get modem reset status. Invalid params', function () {
            this.timeout(100000);
            return supportService.checkACSResetStatus(testData.hardware.invalidIndiHomeNum).catch(data => {
                expect(data).to.be.empty;
            });
        });
    });
});

describe('Support Issue Listing', () => {
    it('should load all', () => {
        return supportService.getAllSupportTicketCategories().then(options => {
            expect(options).not.to.be.empty;
        });

    });

    it('should load all english', () => {
        return supportService.getAllSupportTicketCategories('en').then(options => {
            expect(options).not.to.be.empty;
        });

    });

    it('should load all ID', () => {
        return supportService.getAllSupportTicketCategories('id').then(options => {
            expect(options).not.to.be.empty;
        });

    });

    it('load all selected in category', () => {
        return supportService.getAllSupportTicketsInCategory('internet').then(options => {
            expect(options).not.to.be.empty;
        });

    });

    it('load symptom by id', () => {
        return supportService.getSupportTicketSymptomById(1, false, true).then(sypmtom => {
            expect(sypmtom).not.to.be.null;
            expect(sypmtom.symptom).not.to.be.null;
        });
    });

    it('load symptom by name', () => {
        return supportService.getSupportTicketSymptomByName('Kecepatan internet sangat lambat', true, true).then(symptom => {
            expect(symptom).not.to.be.null;
            expect(symptom['symptom']).not.to.be.null;
            expect(symptom['ownerGroup']).not.to.be.null;
            expect(symptom['type']).not.to.be.null;
        });
    });

    it('should fail on invalid language', () => {
        return supportService.getAllSupportTicketCategories('invalid-language').catch(reason => {
            expect(reason).not.to.be.null;
        });
    });

});
