/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 24/12/2019, 2:13 AM
 */

const log4js = require('log4js');
const config = require('../../config/index');
const logLevel = config.get('/logLevel') || 'info';

class Logger {

    constructor(logger = 'User Support Service', level = logLevel) {
        this.logger = log4js.getLogger(logger);
        this.logger.level = level;
    }

    trace(message, ...args) {
        this.logger.trace(message, ...args);
    }

    debug(message, ...args) {
        this.logger.debug(message, ...args);
    }

    info(message, ...args) {
        this.logger.info(message, ...args);
    }

    error(message, ...args) {
        this.logger.error(message, ...args);
    }

    warn(message, ...args) {
        this.logger.warn(message, ...args);
    }

    fatal(message, ...args) {
        this.logger.fatal(message, ...args);
    }

}

module.exports = Logger;
