const {
    NotFoundError, InternalServerError, BadRequestError, ConflictError, ConditionNotMetError, PreconditionFailedError,
    ForbiddenError, MethodNotAllowedError, UnauthorizedError, ServiceUnavailableError,
    LogicTicketAlreadyExistsError, FisikTicketAlreadyExistsError, AdminTicketAlreadyExistsError, AcsHasNotResetError,
    FisikTicketFoundError, MaxAttemptsReachedError, PendingTicketRequestFoundError
} = require('../error');
const {ERROR: httpError} = require('../http-status/status_code');

const data = (data) => ({err: null, data});

const paginationData = (data, meta) => ({err: null, data, meta});

const error = (err) => ({err, data: null});

const response = (res, type, result, message = '', statusCode = 200, code = statusCode) => {
    let ok = true;
    let {data} = result;
    if (type === 'fail') {
        ok = false;
        data = null;
        message = result.err.message || message;
        statusCode = checkErrorCode(result.err);
        // code = checkErrorCode(result.err);
    }
    if (statusCode !== 200) {
        code = statusCode;
    }
    res.status(statusCode).send({
        ok,
        data,
        message,
        status: code,
    });
};

const paginationResponse = (res, type, result, message = '', code = 200) => {
    let ok = true;
    let {data} = result;
    if (type === 'fail') {
        ok = false;
        data = '';
        message = result.err.message || message;
        code = checkErrorCode(result.err);
    }
    res.status(code).send({
        ok,
        data,
        meta: result.meta,
        status: code,
        message,
    });
};

const checkErrorCode = (error) => {
    switch (error.constructor) {
    case BadRequestError:
        return httpError.BAD_REQUEST;
    case ConflictError:
        return httpError.CONFLICT;
    case ConditionNotMetError:
        return httpError.CONDITION_NOT_MET;
    case ForbiddenError:
        return httpError.FORBIDDEN;
    case InternalServerError:
        return httpError.INTERNAL_ERROR;
    case NotFoundError:
        return httpError.NOT_FOUND;
    case PreconditionFailedError:
        return httpError.PRECONDITION_FAILED;
    case MethodNotAllowedError:
        return httpError.METHOD_NOT_ALLOWED;
    case UnauthorizedError:
        return httpError.UNAUTHORIZED;
    case ServiceUnavailableError:
        return httpError.SERVICE_UNAVAILABLE;
    case LogicTicketAlreadyExistsError:
        return httpError.ISSUE_REPORTING_LOGIC_TICKETS_ALREADY_EXISTS;
    case FisikTicketAlreadyExistsError:
        return httpError.ISSUE_REPORTING_FISIK_TICKETS_ALREADY_EXISTS;
    case AdminTicketAlreadyExistsError:
        return httpError.ISSUE_REPORTING_ADMIN_TICKETS_ALREADY_EXISTS;
    case AcsHasNotResetError:
        return httpError.ISSUE_REPORTING_ACS_HAS_NOT_RESET;
    case FisikTicketFoundError:
        return httpError.ISSUE_REPORTING_FISIK_TICKET_FOUND;
    case MaxAttemptsReachedError:
        return httpError.MAX_RESCHEDULE_ATTEMPTS_REACHED;
    case PendingTicketRequestFoundError:
        return httpError.ISSUE_REPORTING_PENDING_REQUEST_FOUND;    
    default:
        return httpError.CONFLICT;
    }
};

module.exports = {
    data,
    paginationData,
    error,
    response,
    paginationResponse,
    checkErrorCode
};
