const BadRequestError = require('./bad_request_error');

class FisikTicketFoundError extends BadRequestError {
    constructor(message) {
        super(message || 'Fisik Ticket found');
    }
}

module.exports = FisikTicketFoundError;
