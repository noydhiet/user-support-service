const ConflictError = require('./conflict_error');
const ConditionNotMetError = require('./condition_not_met_error');
const ForbiddenError = require('./forbidden_error');
const InternalServerError = require('./internal_server_error');
const NotFoundError = require('./not_found_error');
const UnauthorizedError = require('./unauthorized_error');
const PreconditionFailedError = require('./precondition_failed_error');
const MethodNotAllowedError = require('./method_not_allowed');
const BadRequestError = require('./bad_request_error');
const ServiceUnavailableError= require('./service_unavaiable_error');
const FisikTicketAlreadyExistsError= require('./fisik_ticket_already_exists');
const LogicTicketAlreadyExistsError= require('./logic_ticket_already_exists');
const AdminTicketAlreadyExistsError= require('./admin_ticket_already_exists');
const AcsHasNotResetError= require('./acs_has_not_reset');
const FisikTicketFoundError= require('./fisik_ticket_found');
const MaxAttemptsReachedError= require('./reschedule_max_attempts_reached');
const PendingTicketRequestFoundError= require('./pending_ticket_creation_req_found');

module.exports = {
    ConflictError,
    ConditionNotMetError,
    ForbiddenError,
    InternalServerError,
    NotFoundError,
    UnauthorizedError,
    PreconditionFailedError,
    MethodNotAllowedError,
    BadRequestError,
    ServiceUnavailableError,
    FisikTicketAlreadyExistsError,
    LogicTicketAlreadyExistsError,
    AdminTicketAlreadyExistsError,
    AcsHasNotResetError,
    FisikTicketFoundError,
    MaxAttemptsReachedError,
    PendingTicketRequestFoundError
};
