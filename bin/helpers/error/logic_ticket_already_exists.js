const BadRequestError = require('./bad_request_error');

class LogicTicketAlreadyExistsError extends BadRequestError {
    constructor(message) {
        super(message || 'Logic Ticket Already Exists');
    }
}

module.exports = LogicTicketAlreadyExistsError;

