const BadRequestError = require('./bad_request_error');

class AcsHasNotResetError extends BadRequestError {
    constructor(message) {
        super(message || 'ACS has not reset in past hour');
    }
}

module.exports = AcsHasNotResetError;
