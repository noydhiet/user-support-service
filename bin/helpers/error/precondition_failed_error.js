const CommonError = require('./common_error');

class PreconditionFailedError extends CommonError {
    constructor(message) {
        super(message || 'Precondition Failed');
    }
}

module.exports = PreconditionFailedError;
