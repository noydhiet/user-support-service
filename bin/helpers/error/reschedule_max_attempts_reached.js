const ForbiddenError = require('./forbidden_error');

class MaxAttemptsReachedError extends ForbiddenError {
    constructor(message) {
        super(message || 'Max reschedule/reopen attempts reached.');
    }
}

module.exports = MaxAttemptsReachedError;
