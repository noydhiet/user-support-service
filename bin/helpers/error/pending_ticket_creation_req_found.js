const BadRequestError = require('./bad_request_error');

class PendingTicketRequestFoundError extends BadRequestError {
    constructor(message) {
        super(message || 'Already submitted ticket and processing.');
    }
}

module.exports = PendingTicketRequestFoundError;
