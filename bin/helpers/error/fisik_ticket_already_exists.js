const BadRequestError = require('./bad_request_error');

class FisikTicketAlreadyExistsError extends BadRequestError {
    constructor(message) {
        super(message || 'Fisik Ticket Already Exists');
    }
}

module.exports = FisikTicketAlreadyExistsError;
