const BadRequestError = require('./bad_request_error');

class AdminTicketAlreadyExistsError extends BadRequestError {
    constructor(message) {
        super(message || 'Admin Ticket Already Exists');
    }
}

module.exports = AdminTicketAlreadyExistsError;
