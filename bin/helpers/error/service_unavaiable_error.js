/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 3/12/2019, 2:59 PM
 */

const InternalServerError = require('./internal_server_error');

class ServiceUnavailableError extends InternalServerError {
    constructor(message) {
        super(message || 'ServiceUnavailable Error');
    }
}

module.exports = ServiceUnavailableError;
