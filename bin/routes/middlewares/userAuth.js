/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 8/11/2019, 11:45 AM
 */

const jwt = require('jsonwebtoken');
const config = require('../../config');
const statusCode = require('../../helpers/http-status/status_code');
const wrapper = require('../../helpers/utils/wrapper');
const {UnauthorizedError} = require('../../helpers/error');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        req.userData = jwt.verify(token, config.get('/authentication').token);
        req.userData.token = token;
        next();
    } catch (error) {
        let response = {};
        response.data = {};
        response.message = 'Auth failed';
        response.status = statusCode.ERROR.UNAUTHORIZED;
        response.ok = false;

        return wrapper.response(res, 'fail', wrapper.error(new UnauthorizedError('Auth failed')));
    }
};
