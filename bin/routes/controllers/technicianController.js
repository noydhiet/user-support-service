/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 8/11/2019, 11:37 AM
 */


const technicianService = require('../../services/technicianService');
const supportService = require('../../services/supportService');
const config = require('../../config');
const wrapper = require('../../helpers/utils/wrapper');
const _ = require('lodash');
const {BadRequestError} = require('../../helpers/error');
const Logger = require('../../helpers/utils/logger');
const {LANGUAGE_HEADER, LANGUAGE} = require('../../config/const/commonConstants');
const logger = new Logger('TechnicianController');
const defaultAvailableDays = config.get('/technician').schedule.listAvailableInstallationDefaultDays;

/**
 * get available technician service slots
 */
module.exports.getAvailableServiceTimesForDays = async (req, res) => {

    let nDays = req.query.days || req.params.days || parseInt(defaultAvailableDays);
    let sto = req.query.sto;
    logger.info(`searching available assurance times for the user: ${req.userData.userId}`);

    technicianService.getAvailableServiceTimes(nDays, sto, req.userData)
        .then(availableTimes => {
            if (availableTimes && availableTimes.length > 0) {
                wrapper.response(res, 'success', wrapper.data({availableTimes}), 'appointment times found');
            } else {
                wrapper.response(res, 'success', wrapper.data({availableTimes: []}),
                    'no appointment times found');
            }
        })
        .catch(err => {
            logger.error('Error in get installation appointments', err);
            wrapper.response(res, 'fail', wrapper.error(err));
        });
};

/**
 * get technician available slots
 */
module.exports.getAvailableInstallationTimesForDays = async (req, res) => {

    let days = req.query.days || req.params.days || parseInt(defaultAvailableDays);
    let sto = req.query.sto;

    logger.info(`searching available times for user :${req.userData.userId} `);

    if (days < 1) {
        wrapper.response(res, 'success', days, 'Invalid number given for days', 400);
    }
    try {
        let availableTimes = await technicianService.getAvailableInstallationTimes(days, sto, req.userData);
        if (availableTimes && availableTimes.length > 0) {
            return wrapper.response(res, 'success', wrapper.data({availableTimes}), 'appointment times found');
        }
        return wrapper.response(res, 'success', wrapper.data({availableTimes: []}),
            'no appointment times found');

    } catch (err) {
        logger.error('Error in get service appointments.', err);
        return wrapper.response(res, 'fail', wrapper.error(err));
    }
};

/**
 *  book/ reserve installation appointment
 */
module.exports.reserveInstallationAppointment = (req, res) => {
    let user = req.userData;
    let bookingId = req.query.bookingId;

    logger.info(`reserving installation appointment for user ${user.userId} bookingId: ${bookingId}`);

    technicianService.reserveInstallationAppointment(user, bookingId)
        .then(status => {
            wrapper.response(res, 'success', wrapper.data(status), 'appointment successfully reserved', 201);
        })
        .catch(err => {
            logger.error('Error in booking installation.', err);
            wrapper.response(res, 'fail', wrapper.error(err));
        });
};

/**
 * @deprecated
 * book/ reserve service appointment
 */
module.exports.reserveServiceAppointment = (req, res) => {
    let userId = req.userData.userId;
    let bookingId = req.query.bookingId;

    technicianService.reserveServiceAppointment(userId, bookingId)
        .then(status => {
            wrapper.response(res, 'success', wrapper.data(status), 'appointment successfully reserved', 201);
        })
        .catch(err => {
            logger.error('Error in booking service.', err);
            wrapper.response(res, 'fail', wrapper.error(err));
        });
};

/**
 *  update installation appointment status
 */
module.exports.updateInstallationAppointmentStatus = (req, res) => {
    let transactionId = req.query.transactionId;
    let status = req.query.status;

    logger.info(`updating installation transactionId ${transactionId} status: ${status}`);
    technicianService.updateAppointmentStatus(transactionId, status)
        .then(response => {
            return wrapper.response(res, 'success', wrapper.data(response), 'appointment status updated');
        })
        .catch(err => {
            logger.error('Error in update installation appointments.', err);
            return wrapper.response(res, 'fail', wrapper.error(err));
        });
};

/**
 * provide star click order number for the booking
 */
module.exports.updateBookingsOrderNumber = (req, res) => {
    let transactionId = req.body['transactionId'];
    let orderNumber = req.body['orderNumber'];

    logger.info(`updating order number for transactionId ${transactionId} orderNumber: ${orderNumber}`);
    technicianService.updateBookingsOrderNumber(transactionId, orderNumber)
        .then(response => {
            return wrapper.response(res, 'success', wrapper.data(response), 'appointment status updated');
        })
        .catch(err => {
            logger.error('Error in update booking.', err);
            return wrapper.response(res, 'fail', wrapper.error(err));
        });
};

/**
 * @deprecated
 * update service appointment
 */
module.exports.updateServiceAppointmentStatus = (req, res) => {
    let bookingId = req.query.bookingId;
    let status = req.query.status;

    technicianService.updateServiceAppointmentStatus(bookingId, null, status)
        .then(response => {
            return wrapper.response(res, 'success', wrapper.data(response), 'appointment status updated');
        })
        .catch(err => {
            logger.error('Error in update service appointment status.', err);
            return wrapper.response(res, 'fail', wrapper.error(err));
        });
};

/**
 * close or reopen installation appointment
 */
module.exports.closeInstallationAppointment = (req, res) => {

    let userId = req.userData.userId || req.query.userId;
    //fixme: validation
    let bookingId = req.query.bookingId;
    let closed = req.query.closed === 'true';

    logger.info(`closing installation appointment for user ${userId} bookingId: ${bookingId}`);
    technicianService.closeBooking(bookingId, userId, closed).then(response => {
        wrapper.response(res, 'success', wrapper.data(response));
    }).catch(err => {
        logger.error('Error in closing appointments.', err);
        wrapper.response(res, 'fail', wrapper.error(err));
    });
};

/**
 *  get the open installation status for the user
 */
module.exports.getInstallationAppointmentStatus = (req, res) => {
    let userId = req.userData.userId;
    let bookingId = req.query.bookingId;

    logger.info(`requesting installation appointment for user ${userId} bookingId: ${bookingId}`);
    technicianService.getAppointmentStatus(userId, bookingId)
        .then(appointments => {
            wrapper.response(res, 'success', wrapper.data(appointments));
        })
        .catch(err => {
            logger.error('Error in get installation appointments status.', err);
            wrapper.response(res, 'success', wrapper.data({
                userId,
                appointments: [],
                technician: {},
                err
            }), `Error in get installation appointments status : ${err.message} `, 200, wrapper.checkErrorCode(err));
        });
};

/**
 * @deprecated
 * @param req
 * @param res
 */
module.exports.getServiceAppointmentStatus = (req, res) => {
    let userId = req.userData.userId;
    let bookingId = req.query.bookingId;

    technicianService.getServiceAppointmentStatus(userId, bookingId)
        .then(appointments => {
            wrapper.response(res, 'success', wrapper.data(appointments));
        })
        .catch(err => {
            logger.error('Error in get service appointments status.', err);
            wrapper.response(res, 'success', wrapper.data({
                userId,
                appointments: [],
                technician: {},
                err
            }), `Error in get installation appointments status : ${err.message}`, 200, wrapper.checkErrorCode(err));
        });
};

/**
 * Get early installation time slots
 */
module.exports.getEarlyInstallationTimes = (req, res) => {
    const bookingId = req.params.bookingId;
    technicianService.getEarlyInstallationTimes(bookingId)
        .then(slot => {
            wrapper.response(res, 'success', wrapper.data(slot));
        })
        .catch(err => {
            logger.error('Error in get early installation appointments.', err);
            wrapper.response(res, 'fail', wrapper.error(err));
        });
};

/**
 * Reschedule feasibility for user
 */
module.exports.getRescheduleFeasibility = (req, res) => {
    const userId = req.userData.userId;
    technicianService.getRescheduleFeasibility(userId)
        .then(data => {
            return wrapper.response(res, 'success', wrapper.data(data));
        })
        .catch(err => {
            logger.error('Error in reschedule installation appointments.', err);
            return wrapper.response(res, 'success', wrapper.data({
                err
            }), err.message, 200, wrapper.checkErrorCode(err));
        });
};

/**
 * Reschedule installation request by user
 */
module.exports.rescheduleInstallationAppointment = (req, res) => {
    const bookingId = req.params.bookingId;
    const userId = req.userData.userId;
    technicianService.rescheduleInstallationBooking(bookingId, userId)
        .then(data => {
            wrapper.response(res, 'success', wrapper.data(data));
        })
        .catch(err => {
            logger.error('Error in reschedule installation appointments.', err);
            wrapper.response(res, 'fail', wrapper.error(err));
        });
};

/**
 * Send early installation request to user
 */
module.exports.sendEarlyInstallationRequest = (req, res) => {
    const transactionId = req.query.transactionId;
    const newDateTime = req.query.newDateTime;
    technicianService.sendEarlyInstallationRequest(transactionId, newDateTime)
        .then(data => {
            wrapper.response(res, 'success', wrapper.data(data));
        })
        .catch(err => {
            logger.error('Error sending early appointment request.', err);
            wrapper.response(res, 'fail', wrapper.error(err));
        });
};

/**
 * Reschedule early installation for a new booking time
 */
module.exports.rescheduleEarlyInstallationBooking = (req, res) => {
    const payload = {
        bookingId : req.query.bookingId,
        schedule: req.query.schedule
    };
    const userId = req.userData.userId;

    technicianService.rescheduleEarlyInstallationBooking(payload, userId)
        .then(data => {
            wrapper.response(res, 'success', wrapper.data(data));
        })
        .catch(err => {
            logger.error('Error in reschedule installation appointments.', err);
            wrapper.response(res, 'fail', wrapper.error(err));
        });
};

/**
 * Report issue for technician and installation
 */
module.exports.reportIssue = (req, res) => {
    const userData = req.userData;
    const ticketData = req.body;
    const bookingId = req.query.bookingId;
    const selectedIndiHomeNum = req.query.selectedIndiHomeNum;
    const language = req.get(LANGUAGE_HEADER) || LANGUAGE.EN;

    // validate required data
    if (_.isEmpty(bookingId) || _.isEmpty(selectedIndiHomeNum) || _.isEmpty(ticketData.issueId)) {
        return wrapper.response(res, 'fail',
            wrapper.error(new BadRequestError('BookingId, selected IndiHome Number or issueId cannot be empty.')),
            'BookingId, selected IndiHome Number or Issue cannot be empty.', 400);
    }
    ticketData.bookingId = bookingId;
    supportService.reportIssue(selectedIndiHomeNum, language, ticketData, userData, false)
        .then(resp => {
            logger.info(`Issue successfully created: Response: ${JSON.stringify(resp)}`);
            wrapper.response(res, 'success', wrapper.data(resp), 'Issue successfully created.', 201);
        })
        .catch(err => {
            logger.error('Error in reporting issues.', err);
            wrapper.response(res, 'success', wrapper.data(err), `Error in reporting issue. ${err.message}`, 200, wrapper.checkErrorCode(err));
        });
};

/**
 * Update ticket status by ticketId: for fisik Ticket and Installation Ticket
 * TODO: (If installation and fisik tickets have different status, create new one)
 */
module.exports.updateTechnicianTicketStatus = (req, res) => {
    let {ticket, ticketId, transactionId, status} = req.body;
    let id = ticket? ticketId: transactionId;
    supportService.updateTicketStatus(id, status.toUpperCase(), !ticket)
        .then(resp => {
            //@todo: move response type, status codes to constants
            wrapper.response(res, 'success', wrapper.data(resp), 'Updated ticket status successfully', 201);
        })
        .catch(err => {
            logger.error('Error in update technician ticket status.', err);
            wrapper.response(res, 'fail', wrapper.error(err));
        });
};

/**
 * Get Technician information by laboutId
 */
module.exports.getTechnicianInformation = async (req, res) => {
    let bookingId = req.params.bookingId;
    await technicianService.getTechnicianInformation(bookingId)
        .then(technicians => {
            wrapper.response(res, 'success', wrapper.data(technicians));
        })
        .catch(err => {
            logger.error('Error in get technician information.', err);
            wrapper.response(res, 'fail', wrapper.error(err));
        });
};

/**
 * Get booking information for the given time
 */
module.exports.getBookingsForTime = async (req, res) => {
    let time = req.query.time;
    await technicianService.getBookingsForTime(time)
        .then(bookingData => {
            wrapper.response(res, 'success', wrapper.data(bookingData));
        })
        .catch(err => {
            logger.error('Error in get booking information.', err);
            wrapper.response(res, 'fail', wrapper.error(err));
        });
};
