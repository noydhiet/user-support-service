/**
 * Created by nishanif on 13/11/19
 */

const supportService = require('../../services/supportService');
const responseHandler = require('../../helpers/utils/wrapper');
const {BadRequestError} = require('../../helpers/error');
const _ = require('lodash');
const {LANGUAGE_HEADER, LANGUAGE} = require('../../config/const/commonConstants');
const Logger = require('../../helpers/utils/logger');
const logger = new Logger('SupportController');


/**
 * Create a Support Ticket
 */
module.exports.reportIssue = (req, res) => {
    const userData = req.userData;
    const ticketData = req.body;
    const selectedIndiHomeNum = req.query.selectedIndiHomeNum;
    const language = req.get(LANGUAGE_HEADER) || LANGUAGE.EN;

    // validate required data
    if (_.isEmpty(ticketData.categoryId) || _.isEmpty(selectedIndiHomeNum) || _.isEmpty(ticketData.issueId)) {
        return responseHandler.response(res, 'fail',
            responseHandler.error(new BadRequestError('categoryId, selected IndiHome Number or  or issueId cannot be empty.')),
            'Category, selected IndiHome Number or  or Issue cannot be empty.', 400);
    }
    supportService.reportIssue(selectedIndiHomeNum, language, ticketData, userData)
        .then(resp => {
            //@todo: move response type, status codes to constants
            responseHandler.response(res, 'success', responseHandler.data(resp), 'Issue successfully created', 201);
        })
        .catch(err => {
            logger.error('Error in reporting issues', err);
            responseHandler.response(res, 'success', responseHandler.data(err), `Error in reporting issue. ${err.message}`, 200, responseHandler.checkErrorCode(err));
        });
};

/**
 * Get All tickets
 */
module.exports.getAllTickets = (req, res) => {
    let open = req.query.open;
    if(req.userData || req.query.userId){
        let userId =  req.query.userId || req.userData.userId;
        supportService.getAllTicketsByUserId(userId, open)
            .then(resp => {
            //@todo: move response type, status codes to constants
                responseHandler.response(res, 'success', responseHandler.data(resp), 'Retrieved all issues by user successfully', 200);
            })
            .catch(err => {
                logger.error('Error in getAllTicketsByUserId.', err);
                responseHandler.response(res, 'fail', responseHandler.error(err));
            });
    }
    else{
        supportService.getAllTickets(open)
            .then(resp => {
            //@todo: move response type, status codes to constants
                responseHandler.response(res, 'success', responseHandler.data(resp), 'Retrieved all issues successfully', 201);
            })
            .catch(err => {
                logger.error('Error in getAllTickets.', err);
                responseHandler.response(res, 'fail', responseHandler.error(err));
            });
    }
};

/**
 * Get ticket status by ticketId
 */
module.exports.getTicketStatus = (req, res) => {
    const ticketId = req.params.ticketId;
    supportService.getTicketStatus(ticketId)
        .then(resp => {
            //@todo: move response type, status codes to constants
            responseHandler.response(res, 'success', responseHandler.data(resp), 'Retrieved ticket data successfully', 200);
        })
        .catch(err => {
            logger.error('Error in getTicketStatus.', err);
            responseHandler.response(res, 'fail', responseHandler.error(err));
        });
};

/**
 * Get ticket status by transactionId
 */
module.exports.getTicketStatusByTransactionId = (req, res) => {
    const transactionId = req.params.transactionId;
    supportService.getTicketStatusByTransactionId(transactionId)
        .then(resp => {
            //@todo: move response type, status codes to constants
            responseHandler.response(res, 'success', responseHandler.data(resp), 'Retrieved ticket data successfully', 200);
        })
        .catch(err => {
            logger.error('Error in getTicketStatusByTransactionId.', err);
            responseHandler.response(res, 'fail', responseHandler.error(err));
        });
};

/**
 * Notify ticket status by ticketId
 */
module.exports.notifyTicketStatus = (req, res) => {
    const ticketId = req.body.ticketId;
    const userId = req.body.userId;
    supportService.notifyTicketStatus(ticketId, userId)
        .then(resp => {
            //@todo: move response type, status codes to constants
            responseHandler.response(res, 'success', responseHandler.data(resp), 'Notifies ticket status successfully', 200);
        })
        .catch(err => {
            logger.error('Error in notifyTicketStatus.', err);
            responseHandler.response(res, 'fail', responseHandler.error(err));
        });
};

/**
 * Update ticket status by ticketId
 */
module.exports.updateTicketStatus = (req, res) => {
    let {ticket, ticketId, transactionId, status} = req.body;
    let id = ticket? ticketId: transactionId;
    supportService.updateTicketStatus(id, status.toUpperCase(), !ticket)
        .then(resp => {
            //@todo: move response type, status codes to constants
            responseHandler.response(res, 'success', responseHandler.data(resp), 'Updated ticket status successfully', 200);
        })
        .catch(err => {
            logger.error('Error in updateTicketStatus.', err);
            responseHandler.response(res, 'fail', responseHandler.error(err));
        });
};

/**
 *  update the ticket ID by admin using transactionId
 */
module.exports.updateTicketId = (req, res) => {
    let transactionId = req.body.transactionId;
    let ticketId = req.body.ticketId;
    
    logger.info(`Request data. TransactionId: ${transactionId}\nticket Id: ${ticketId}`);
    supportService.updateTicketId(transactionId, ticketId)
        .then(resp => {
            logger.info(`Updated ticket Id: Response: ${JSON.stringify(resp)}`);
            responseHandler.response(res, 'success', responseHandler.data(resp), 'Updated ticket status successfully', 200);
        })
        .catch(err => {
            logger.error('Error in updateTicketId.', err);
            responseHandler.response(res, 'fail', responseHandler.error(err));
        });
};


/**
 * Close or Reopen/reschedule a Support Ticket
 */
module.exports.closeOrReopenOrRescheduleTicket = (req, res) => {
    const ticketId = req.params.ticketId;
    const userId = req.userData.userId;
    let action = req.params.action ? req.params.action.toUpperCase() : null;
    if (_.isEmpty(ticketId) || _.isEmpty(action)) {
        return responseHandler.response(res, 'fail',
            responseHandler.error(new BadRequestError('tickedId/action cannot be empty.')),
            'tickedId/action cannot be empty.', 400);
    }
    //validate bookingId if specified
    if(req.body && req.body.hasOwnProperty('bookingId') && _.isEmpty(req.body.bookingId)){
        return responseHandler.response(res, 'fail',
            responseHandler.error(new BadRequestError('bookingId cannot be empty for fisik reopen/reschedule')),
            'bookingId cannot be empty for fisik reopen/reschedule', 400);
    }
    const bookingId = req.body && req.body.bookingId? req.body.bookingId: null;
    supportService.closeOrReopenOrRescheduleTicket(ticketId, action, userId, bookingId)
        .then(resp => {
            //@todo: move response type, status codes to constants
            responseHandler.response(res, 'success', responseHandler.data(resp), `Ticket was updated to ${action} successfully`, 200);
        })
        .catch(err => {
            logger.error('Error in closeOrReopenOrRescheduleTicket.', err);
            responseHandler.response(res, 'success', responseHandler.data(err), `Error in reopening/rescheduling ticket. ${err.message}`, 200, responseHandler.checkErrorCode(err));
        });
};

/**
 * Close or Reopen a Support Ticket
 */
module.exports.resetModem = (req, res) => {
    const indiHomeNumber = req.query.indiHomeNumber;
    const userId = req.userData.userId;
    if (_.isEmpty(indiHomeNumber)) {
        return responseHandler.response(res, 'fail',
            responseHandler.error(new BadRequestError('indiHomeNumber cannot be empty.')),
            'indiHomeNumber cannot be empty.', 400);
    }
    supportService.resetModem(indiHomeNumber, userId)
        .then(resp => {
            responseHandler.response(res, 'success', responseHandler.data(resp), 'Reboot completed successfully', 200);
        })
        .catch(err => {
            logger.error('Error in resetModem.', err);
            responseHandler.response(res, 'fail', responseHandler.error(err));
        });
};

/**
 * send reminder notification to close a support ticket
 */
module.exports.sendSolveTicketNotification = (req, res) => {
    const ticketId = req.body.ticketId;
    const bookingId = req.body.bookingId;
    const userId = req.body.userId;

    if (_.isEmpty(ticketId) || _.isEmpty(bookingId)) {
        return responseHandler.response(res, 'fail',
            responseHandler.error(new BadRequestError('ticketId or bookingId cannot be empty.')),
            'ticketId or bookingId data cannot be empty.', 400);
    }
    supportService.sendCloseReminderNotification(ticketId, bookingId, userId)
        .then(resp => {
            responseHandler.response(res, 'success', responseHandler.data(resp), 'reminder notification was sent successfully', 200);
        })
        .catch(err => {
            logger.error('Error in sendSolveTicketNotification.', err);
            responseHandler.response(res, 'fail', responseHandler.error(err));
        });
};

/**
 * Get scheduled tickets for the given time
 */
module.exports.getSolvedTicketsForTime = async (req, res) => {
    let time = req.query.time;
    await supportService.getTicketsForTime(time)
        .then(ticketsData => {
            responseHandler.response(res, 'success', responseHandler.data(ticketsData));
        })
        .catch(err => {
            logger.error('Error in get ticket information.', err);
            responseHandler.response(res, 'fail', responseHandler.error(err));
        });
};

/**
 * Get submitted tickets for the given time
 */
module.exports.getSubmittedTicketsForTime = async (req, res) => {
    let time = req.query.time;
    await supportService.getSubmittedTicketsForTime(time)
        .then(ticketsData => {
            responseHandler.response(res, 'success', responseHandler.data(ticketsData));
        })
        .catch(err => {
            logger.error('Error in get ticket information.', err);
            responseHandler.response(res, 'fail', responseHandler.error(err));
        });
};


