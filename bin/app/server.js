/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 8/11/2019, 11:36 AM
 */

const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const wrapper = require('../helpers/utils/wrapper');
const bodyParser = require('body-parser');

const config = require('../config');
const reqLogType = config.get('/requestLogType');

const fs = require('fs');
const path = require('path');
const spec = fs.readFileSync(path.join(__dirname, '../app/swagger.yaml'), 'utf8');

const jsyaml = require('js-yaml');
const swaggerDocument = jsyaml.safeLoad(spec);

const userAuth = require('../routes/middlewares/userAuth');
const basicAuth = require('../routes/middlewares/basicAuth');

const technicianController = require('../routes/controllers/technicianController');
const supportController = require('../routes/controllers/supportController');
const questionHandler = require('../modules/questions/handlers/api_handler');
const issueHandler = require('../modules/issues/handlers/api_handler');
const answerHandler = require('../modules/answers/handlers/api_handler');
const feedbackHandler = require('../modules/feedback/handlers/api_handler');
const categoryHandler = require('../modules/category/handlers/api_handler');
const ticketCategoryHandler = require('../modules/ticket-categories/handlers/api_handler');

function AppServer() {
    this.server = express();
    this.server.use(morgan(reqLogType));

    this.server.use(bodyParser.json());

    this.server.use(cors());
    this.server.use(basicAuth.init());

    this.server.get('/', (req, res) => {
        wrapper.response(res, 'success', wrapper.data('User Support Services'), 'This services is running properly.');
    });

    this.server.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    //  Technician Installation
    this.server.get('/technician/appointment/installation/availability', userAuth, technicianController.getAvailableInstallationTimesForDays);
    this.server.get('/technician/appointment/installation/availability/:days', userAuth, technicianController.getAvailableInstallationTimesForDays);
    this.server.get('/technician/appointment/installation', userAuth, technicianController.getInstallationAppointmentStatus);
    this.server.post('/technician/appointment/installation', userAuth, technicianController.reserveInstallationAppointment);
    this.server.put('/technician/appointment/installation/status', basicAuth.isAuthenticated,
        technicianController.updateInstallationAppointmentStatus);
    this.server.put('/technician/appointment/installation/order-number', basicAuth.isAuthenticated, technicianController.updateBookingsOrderNumber);
    this.server.put('/technician/appointment/installation/close', userAuth, technicianController.closeInstallationAppointment);

    // scheduler close ticket
    this.server.put('/technician/appointment/installation/scheduler/close', basicAuth.isAuthenticated, technicianController.closeInstallationAppointment);

    //  Technician Installation Reschedule
    this.server.get('/technician/appointment/installation/reschedule/feasibility', userAuth, technicianController.getRescheduleFeasibility);
    this.server.get('/technician/appointment/installation/reschedule/slot/:bookingId', basicAuth.isAuthenticated,
        technicianController.getEarlyInstallationTimes);
    this.server.get('/technician/appointment/installation/reschedule/slot/:bookingId', basicAuth.isAuthenticated,
        technicianController.getEarlyInstallationTimes);
    this.server.post('/technician/appointment/installation/reschedule/:bookingId', userAuth,
        technicianController.rescheduleInstallationAppointment);

    this.server.post('/technician/appointment/installation/early-visit/notification',
        basicAuth.isAuthenticated, technicianController.sendEarlyInstallationRequest);
    this.server.post('/technician/appointment/installation/early-visit/reschedule', userAuth,
        technicianController.rescheduleEarlyInstallationBooking);

    this.server.get('/technician/appointment/service/availability', userAuth, technicianController.getAvailableServiceTimesForDays);
    //deprecated
    this.server.get('/technician/appointment/service/availability/:days', userAuth, technicianController.getAvailableServiceTimesForDays);
    this.server.get('/technician/appointment/service', userAuth, technicianController.getServiceAppointmentStatus);
    this.server.post('/technician/appointment/service', userAuth, technicianController.reserveServiceAppointment);
    this.server.put('/technician/appointment/service/status', basicAuth.isAuthenticated, technicianController.updateServiceAppointmentStatus);

    this.server.get('/technician/ticket/issues', userAuth, ticketCategoryHandler.getTechnicianIssues);
    this.server.post('/technician/ticket', userAuth, technicianController.reportIssue);
    this.server.put('/technician/ticket/status', basicAuth.isAuthenticated,
        technicianController.updateTechnicianTicketStatus);

    this.server.post('/user-support/index', basicAuth.isAuthenticated, questionHandler.createIndexES);
    this.server.get('/technician/:bookingId', userAuth, technicianController.getTechnicianInformation);

    // Category
    this.server.get('/user-support/categories', basicAuth.isAuthenticated, categoryHandler.getAllCategory);

    // Question
    this.server.get('/user-support/questions', basicAuth.isAuthenticated, questionHandler.getAllQuestion);
    this.server.post('/user-support/questions', basicAuth.isAuthenticated, questionHandler.createQuestion);
    this.server.put('/user-support/questions/:questionId', basicAuth.isAuthenticated, questionHandler.updateQuestion);
    this.server.delete('/user-support/questions/:questionId', basicAuth.isAuthenticated, questionHandler.deleteQuestion);
    this.server.get('/user-support/questions/faq', basicAuth.isAuthenticated, questionHandler.getFrequentlyAskQuestion);

    // Issue
    this.server.get('/user-support/issues', basicAuth.isAuthenticated, issueHandler.getAllIssue);
    this.server.post('/user-support/issues', basicAuth.isAuthenticated, issueHandler.createIssue);
    this.server.put('/user-support/issues/:issueId', basicAuth.isAuthenticated, issueHandler.updateIssue);
    this.server.delete('/user-support/issues/:issueId', basicAuth.isAuthenticated, issueHandler.deleteIssue);

    // Answer
    this.server.get('/user-support/answers/:answerNumber/question/:questionId', basicAuth.isAuthenticated, answerHandler.getAnswerByQuestionId);
    this.server.get('/user-support/answers/:answerNumber/issue/:issueId', basicAuth.isAuthenticated, answerHandler.getAnswerByIssueId);
    this.server.put('/user-support/answers/:answerId', basicAuth.isAuthenticated, answerHandler.updateAnswer);
    this.server.delete('/user-support/answers/:answerId', basicAuth.isAuthenticated, answerHandler.deleteAnswer);
    this.server.post('/user-support/answers', basicAuth.isAuthenticated, answerHandler.createAnswer);

    // Support ticket
    this.server.get('/dashboard/support/tickets', basicAuth.isAuthenticated, supportController.getAllTickets);
    this.server.get('/support/ticket/categories/', userAuth, ticketCategoryHandler.getAllTicketCategories);
    this.server.get('/support/ticket/issues/:categoryId', userAuth, ticketCategoryHandler.getIssuesByCategoryId);
    this.server.post('/support/ticket/solutions', userAuth, ticketCategoryHandler.getSolutions);
    this.server.get('/support/ticket', userAuth, supportController.getAllTickets);
    this.server.get('/support/ticket/ticketId/:ticketId/status', userAuth, supportController.getTicketStatus);
    this.server.get('/support/ticket/:ticketId/status', userAuth, supportController.getTicketStatus);
    this.server.get('/support/ticket/transactionId/:transactionId/status', userAuth, supportController.getTicketStatusByTransactionId);
    this.server.put('/support/ticket/status', basicAuth.isAuthenticated, supportController.updateTicketStatus);
    this.server.put('/ticket/status', basicAuth.isAuthenticated, supportController.updateTicketStatus);
    this.server.post('/support/ticket', userAuth, supportController.reportIssue);
    this.server.post('/support/ticket/:ticketId/:action', userAuth, supportController.closeOrReopenOrRescheduleTicket);
    this.server.post('/support/ticket/notification', basicAuth.isAuthenticated, supportController.notifyTicketStatus);
    this.server.put('/ticket/ticketId', basicAuth.isAuthenticated, supportController.updateTicketId);

    //support self troubleshoot
    this.server.post('/support/modem/reset', userAuth, supportController.resetModem);

    // Feedback
    this.server.get('/user-support/feedback/topics', userAuth, feedbackHandler.getAllTopic);
    this.server.post('/user-support/feedback/topics', basicAuth.isAuthenticated, feedbackHandler.createTopic);
    this.server.get('/user-support/feedback', basicAuth.isAuthenticated, feedbackHandler.getAllFeedback);
    this.server.post('/user-support/feedback', userAuth, feedbackHandler.sendFeedback);

    //Notification
    this.server.get('/notification/installation/bookingTimes', basicAuth.isAuthenticated, technicianController.getBookingsForTime);
    this.server.get('/notification/ticket/solvedTickets', basicAuth.isAuthenticated, supportController.getSolvedTicketsForTime);
    this.server.get('/notification/ticket/submittedTickets', basicAuth.isAuthenticated, supportController.getSubmittedTicketsForTime);
    this.server.post('/notification/ticket/closeTicket', basicAuth.isAuthenticated, supportController.sendSolveTicketNotification);
}

module.exports = AppServer;

