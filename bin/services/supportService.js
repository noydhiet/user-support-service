/**
 * Created by nishanif on 13/11/19
 */

const rp = require('request-promise');
const _ = require('lodash');
const Logger = require('../helpers/utils/logger');
const logger = new Logger('SupportService');
const moment = require('moment');
const config = require('../config');
const notificationService = require('./notificationService');
const AuthenticationService = require('./clientAuthorizationService');
const technicianService = require('./technicianService');
const userProfileService = require('./userProfileService');
const authenticationService = new AuthenticationService();
const {BadRequestError, ForbiddenError, InternalServerError, NotFoundError,
    LogicTicketAlreadyExistsError, FisikTicketAlreadyExistsError, AdminTicketAlreadyExistsError, AcsHasNotResetError,
    FisikTicketFoundError, MaxAttemptsReachedError, PendingTicketRequestFoundError
} = require('../helpers/error');
const ticketHandler = require('../modules/ticket/handlers/api_handler');
const technicianHandler = require('../modules/technician/handlers/api_handler');
const hardwareHandler = require('../modules/hardware/handlers/api_handler');
const ticketCategoriesHandler = require('../modules/ticket-categories/handlers/api_handler');

const {
    RECEIVED: LOGIK_TICKET_RECEIVED,
    IN_PROGRESS: LOGIK_TICKET_IN_PROGRESS,
    SOLVED: LOGIK_TICKET_SOLVED
} = require('../config/const/logikTicketStatus');

const {
    SUBMITTED: FISIK_SUBMITTED,
    IN_PROGRESS: FISIK_IN_PROGRESS,
    COMPLETED: FISIK_COMPLETED,
    RESOLVED: FISIK_RESOLVED
} = require('../config/const/fisikTicketStatus');

const {
    SUBMITTED: INSTALLATION_SUBMITTED,
    IN_PROGRESS: INSTALLATION_IN_PROGRESS
} = require('../config/const/installationTicketStatus');
const {
    CATEGORIES,
    CATEGORY_TYPES,
    TICKET_TYPES,
    TICKET_ACTIONS,
    IBOOSTER_RANGE, TELKOM_REALM,
    SUPPORT_TICKET_CHANNEL,
    INDONESIAN_LOCALE,
    SUPPORT_TICKET_CLASS,
    ACS_RESET_CUTOFF,
    PRECISION_TWO_DECIMALS, FISIK_TICKET_MAX_RESCHEDULE_ATTEMPTS,
    TICKET_REQ_TYPES
} = require('../config/const/ticketConstants');
const {ISO_DATE_FORMAT, LANGUAGE, NOTIFICATION_CATEGORIES, NOTIFICATION_TYPES} = require('../config/const/commonConstants');

const reportIssueEndpoint = config.get('/supportService').reportIssueEndpoint;
const reopenFisikTicketEndpoint = config.get('/supportService').reopenTicketEndpoint;
const updateIncidentTicketEndpoint = config.get('/supportService').updateIncidentTicketEndpoint;
const checkCableTypeEndpoint = config.get('/supportService').checkCableTypeEndpoint;
const checkIboosterRangeEndpoint = config.get('/supportService').checkIboosterRangeEndpoint;
const resetModemEndpoint = config.get('/supportService').resetModemEndpoint;
const reportIssueParams = config.get('/supportService').reportIssueParams;

const TICKET_PREFIX = 'MYINX-';

/**
 * Create/report issue. This method is used by both technician & help issue reporting flow
 *
 * @param {String} selectedIndiHomeNo - selected IndiHomeNo
 * @param {String} language - selected language
 * @param {Object} ticketData - ticket data
 * @param {Object} userData - user details
 * @param {Boolean} isSupport - isSupport issue: true, technician issue: false
 * @returns {Promise<{data: *, ticketId: string, transactionId: string, status: string}>}
 */
module.exports.reportIssue = async (selectedIndiHomeNum, language = LANGUAGE.EN , ticketData, userData, isSupport = true) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;

    if (_.isEmpty(ticketData) || _.isEmpty(userData)) {
        throw new BadRequestError();
    }

    const fisikCategoryIds = [CATEGORIES.EN.INTERNET.ID, CATEGORIES.EN.TELEPHONE.ID, CATEGORIES.EN.TV.ID,
        CATEGORIES.EN.INSTALLATION.ID, CATEGORIES.ID.INTERNET.ID, CATEGORIES.ID.TELEPHONE.ID, CATEGORIES.ID.TV.ID,
        CATEGORIES.ID.INSTALLATION.ID];

    // if no category/categoryId it's an technician issue
    let categoryName;
    if(_.isEmpty(ticketData.categoryId)) {
        categoryName = CATEGORIES.EN.INSTALLATION.NAME;
        ticketData.categoryId = (language === LANGUAGE.EN)? CATEGORIES.EN.INSTALLATION.ID: CATEGORIES.ID.INSTALLATION.ID;
    } else {
        const element = [CATEGORIES.EN.ADMIN_AND_BILLING, CATEGORIES.EN.INTERNET, CATEGORIES.EN.TELEPHONE, CATEGORIES.EN.TV,
            CATEGORIES.EN.INSTALLATION, CATEGORIES.ID.ADMIN_AND_BILLING, CATEGORIES.ID.INTERNET, CATEGORIES.ID.TELEPHONE,
            CATEGORIES.ID.TV, CATEGORIES.ID.INSTALLATION].find((el, key, item) => {
            return el.ID ===ticketData.categoryId.replace('_id', '_en'); // save categoryId in english language suffix
        });
        categoryName = element ? element.NAME: '';
    }

    // Get user profile data token
    const profile = await userProfileService.getUserProfile(userData);
    if (profile.err) {
        throw new ForbiddenError('Error in getting profile data', profile.err);
    }
    const account = profile.accounts.find(account => account.indiHomeNum);
    if (_.isEmpty(account)) {
        throw new ForbiddenError('No matching account found in profile');
    }
    const accountData = account;
    accountData.name = profile.name;
    accountData.mobile = profile.primaryPhone;
    accountData.type = accountData.type || 'INTERNET'; // @todo revert to verion: ticket-assurance-real-flow
    if (_.isEmpty(accountData.type)) {
        logger.error('Error in user account data. type is missing.');
        throw new BadRequestError('ERROR in checking cable type. type is missing in account data');
    }

    const cableType = await this.checkCableType(selectedIndiHomeNum, accountData.type).catch(e => logger.error(e.message, e)) || 'Fiber'; // @todo revert to verion: ticket-assurance-real-flow
    const isFiber = cableType === 'Fiber';
    const iBoosterData = await this.getIboosterInfo(selectedIndiHomeNum).catch(e => logger.error(e.message, e)) || {}; // @todo revert to verion: ticket-assurance-real-flow
    const iBoosterInSpec = checkIboosterRange(iBoosterData);
    const isAcsReset = await this.checkACSResetStatus(selectedIndiHomeNum).catch(e => logger.error(e.message, e)) || true; // @todo revert to verion: ticket-assurance-real-flow

    const hardwareData = {
        iBoosterInSpec,
        isFiber
    };

    logger.info('User data: ' + JSON.stringify(userData));
    logger.info('Ticket data: ' + JSON.stringify(ticketData));
    logger.info(`Hardware Data: ${JSON.stringify(hardwareData)}`);

    // check the category type Help flow
    // if fisik, logic check for existing tickets of same category, allow max one ticket return error
    // todo: what is forbidden scenario for the installation issues
    let ticketCategoryType;
    let ticketType;
    if ([CATEGORIES.EN.ADMIN_AND_BILLING.ID, CATEGORIES.ID.ADMIN_AND_BILLING.ID].includes(ticketData.categoryId)) {
        ticketCategoryType = CATEGORY_TYPES.ADMIN;
        // SCENARIO 1 - ADMIN
        ticketType = TICKET_TYPES.ADMINISTRASI;
        const pendingBillingOrAdminTickets = await ticketHandler.getPendingTicketsByType(userData.userId, ticketCategoryType);
        if (!_.isEmpty(pendingBillingOrAdminTickets.data)) {
            logger.error(`User already has existing ticket of this category ${ticketCategoryType}`);
            throw new AdminTicketAlreadyExistsError(`User already has existing ticket of this category ${ticketCategoryType}`);
        }
    } else if (fisikCategoryIds.includes(ticketData.categoryId)) {
        ticketType = TICKET_TYPES.TEKNIKAL;
        if(!iBoosterInSpec) {
            // SCENARIO 2: FISIK - need booking Id
            ticketCategoryType = CATEGORY_TYPES.FISIK;
            // check for pending tickets of same types fisik/logic
            const pendingLogicTickets = await ticketHandler.getPendingTicketsByType(userData.userId, CATEGORY_TYPES.LOGIC);
            if (!_.isEmpty(pendingLogicTickets.data)) {
                logger.warn('User already has existing ticket of this ' + CATEGORY_TYPES.LOGIC
                    + '| Pending tickets: ' + JSON.stringify(pendingLogicTickets.data));
                throw new LogicTicketAlreadyExistsError('User already has existing ticket of this Logic');
            }
            const pendingFisikTickets = await ticketHandler.getPendingTicketsByType(userData.userId, ticketCategoryType);
            if (!_.isEmpty(pendingFisikTickets.data)) {
                logger.warn('User already has existing ticket of this ' + ticketCategoryType
                    + '| Pending tickets: ' + JSON.stringify(pendingFisikTickets.data));
                throw new FisikTicketAlreadyExistsError('User already has existing ticket of this Fisik');
            }
        } else {
            if(!isAcsReset) {
                logger.warn('ACS has not reset in past hour');
                throw new AcsHasNotResetError('ACS has not reset in past hour. Please ask user to reset the modem');
            } else {
                // SCENARIO 1: LOGIK
                ticketCategoryType = CATEGORY_TYPES.LOGIC;
                // check for pending tickets of same types fisik/logic
                const pendingLogicTickets = await ticketHandler.getPendingTicketsByType(userData.userId, ticketCategoryType);
                if (!_.isEmpty(pendingLogicTickets.data)) {
                    logger.warn('User already has existing ticket of this ' + CATEGORY_TYPES.LOGIC
                        + '| Pending tickets: ' + JSON.stringify(pendingLogicTickets.data));
                    throw new LogicTicketAlreadyExistsError('User already has existing ticket of this Logic');
                }
                const pendingFisikTickets = await ticketHandler.getPendingTicketsByType(userData.userId, CATEGORY_TYPES.FISIK);
                if (!_.isEmpty(pendingFisikTickets.data)) {
                    logger.warn('User already has existing ticket of this ' + CATEGORY_TYPES.FISIK
                        + '| Pending tickets: ' + JSON.stringify(pendingFisikTickets.data));
                    throw new FisikTicketAlreadyExistsError('User already has existing ticket of this Fisik');
                }
            }
        }
    }

    logger.info(`Category Type: ${ticketCategoryType}`);

    const issueObj = await this.getSupportIssueByIssueId(ticketData.issueId, language);
    if(_.isEmpty(issueObj)) {
        logger.error(`Error in finding matching issue for issueId: ${ticketData.issueId}`);
        throw new BadRequestError(`Error in finding matching issue for issueId: ${ticketData.issueId}`)
    }
    const symptomObj = await this.getSupportTicketSymptom(issueObj, iBoosterInSpec, isFiber);
    logger.info(`Symptom: ${JSON.stringify(symptomObj)}`);
    ticketCategoryType = !_.isEmpty(symptomObj.type)? symptomObj.type : ticketCategoryType;
    logger.info(`Category Type: ${ticketCategoryType}`);

    // return to FE to use technician flow if FISIK ticket found in support flow
    if(isSupport && ticketCategoryType === CATEGORY_TYPES.FISIK) {
        logger.warn(`${ticketCategoryType} ticket type found in Support flow. Use technician issue reporting flow with a booking Id`);
        throw new FisikTicketFoundError(`${ticketCategoryType} ticket type found in Support flow. Use technician issue reporting flow with a booking Id`);
    }
    ticketData.issue = issueObj.issue;
    const reqBody = await generateRequestBody(ticketData, userData, symptomObj, accountData, ticketType, language.toLowerCase());

    const options = {
        method: 'POST',
        timeout: 300000,
        uri: reportIssueEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorizationHeader
        },
        json: reqBody,
        strictSSL: false
    };

    // check pending requests before
    logger.info('Checking pending creation requests..');
    const reqType = [CATEGORY_TYPES.FISIK, CATEGORY_TYPES.LOGIC].includes(ticketCategoryType) ? TICKET_REQ_TYPES.FISIKLOGIC : TICKET_REQ_TYPES.ADMIN;

    const pendingReq = await ticketHandler.getTicketPendingTicketReq(userData.userId, reqType);
    if(pendingReq.err) {
        logger.error(`Error in getting pending ${reqType} ticket creation requests.`);
        throw new InternalServerError('Failed getting pending ticket creation request in DB');
    }
    logger.info(`Received pending creation requests. data: ${JSON.stringify(pendingReq.data)}`);
    if(pendingReq.data.length > 0) {
        logger.error(`Already submitted Ticket creation request. Data:  ${reqBody.EXTTransactionID}. Response: ${JSON.stringify(pendingReq.data)}. Please wait..`);
        throw new PendingTicketRequestFoundError(`Already submitted Ticket creation request. Data:  ${reqBody.EXTTransactionID}. Response: ${JSON.stringify(pendingReq.data)}.  Please wait..`);
    }

    // if no pending requests add new request
    const pendingReqData = {
        userId: userData.userId,
        ticketType: reqType,
        pending: true
    };
    const addReq = await ticketHandler.addTicketRequest(pendingReqData);
    if(addReq.err) {
        logger.error(`Error in adding pending ${reqType} ticket creation requests. ${JSON.stringify(addReq.err)}`);
        throw new InternalServerError('Failed adding pending ticket creation request in DB');
    }
    logger.info(`successfully entered ticket creation request. Data: ${JSON.stringify(addReq.data)}`);

    // good to submit single ticket
    logger.info(`ISSUE REPORTING SVC: Date: ${new Date()}| sending request to create Ticket: ${reqBody.EXTTransactionID}. Req: ${JSON.stringify(options)}`);

    const response = await rp.post(options);

    if (response.statusCode === '0') {
        //update pending ticket status
        const updateReq = await ticketHandler.updateTicketRequestStatus(pendingReqData.userId, pendingReqData.ticketType, false);
        if(updateReq.err) {
            logger.error(`Error in updating pending ${reqType} ticket creation requests.`);
        }
        logger.info(`successfully updated ticket creation request status. Data: ${JSON.stringify(updateReq.data)}`);

        logger.info(`ISSUE REPORTING SVC: Date: ${new Date()}| received response data from Ticket creation: ${reqBody.EXTTransactionID}. Response: ${JSON.stringify(response)}`);

        //execute elastic search indexing by support ticket/issue
        const data = Object.assign(reqBody, {});
        data.userId = userData.userId;
        data.ticketStatus = LOGIK_TICKET_IN_PROGRESS;
        //save category if present, HELP flow
        data.category = categoryName;
        data.categoryType = ticketCategoryType;
        // add default ticket status IN_PROGRESS
        if (ticketCategoryType === CATEGORY_TYPES.ADMIN) {
            data.ticketStatus = LOGIK_TICKET_IN_PROGRESS;
        } else if (ticketCategoryType === CATEGORY_TYPES.LOGIC) {
            data.ticketStatus = LOGIK_TICKET_IN_PROGRESS;
        } else if (ticketCategoryType === CATEGORY_TYPES.FISIK) {
            data.ticketStatus = FISIK_SUBMITTED;
        }

        let technician;

        if (!isSupport) {
            //get technician info & save in Db
            try {
                technician = await technicianService.getServiceTechnicianInformation(ticketData.bookingId);
                data.technician = technician;
            } catch (err) {
                logger.error('requesting technician failed: ', err); //@todo revert to verion: ticket-assurance-real-flow
            }
            data.technicianAssigned = false;
            //technician assigned flag init-false

            //get schedule time information and save
            try {
                const bookingSlot = await getBookingSlotFromBookingId(ticketData.bookingId);
                if (!bookingSlot.err) {
                    data.bookingSlot = bookingSlot.data;
                    data.scheduledTime = moment(bookingSlot.data['dateTime'], 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
                }
            } catch (err) {
                logger.error('requesting booking metadata failed: ', err); //@todo revert to verion: ticket-assurance-real-flow
            }

            // set max reschedule attempts = 2
            data.rescheduleAttempts = FISIK_TICKET_MAX_RESCHEDULE_ATTEMPTS;
        }

        logger.info(`${ticketCategoryType} issue will be reported`);

        logger.info(`${data.categoryType} Ticket/Issue creation data: ${JSON.stringify(data)}`);
        const created = await ticketHandler.createTicket(data);
        if (created.err) {
            logger.info(`Failed to save Ticket/Issue in DB. Error: ${JSON.stringify(created.err)}`);
            throw new InternalServerError('Failed to save Ticket/Issue in DB');
        }
        // send ticket submission notification
        const notification = {
            notifications: [
                {
                    userId: userData.userId,
                    header: 'Ticket Received',
                    isChecked: false,
                    referenceId:  data.ticketId,
                    notificationType: NOTIFICATION_TYPES.ACTIVITY,
                    category: NOTIFICATION_CATEGORIES.TICKET_ASSURANCE,
                    imageUrl: '',
                    body: {
                        topic: `Ticket ID #${data.ticketId}`,
                        description: 'Ticket has been submitted. Our team is processing your request',
                        data: {
                            ticket: data,
                        }
                    }
                }
            ]
        };

        notificationService.sendUserNotification(notification, userData.userId);

        return {
            status: 'success',
            data: response.data,
            ticketId: reqBody.ticketId,
            transactionId: reqBody.EXTTransactionID
        };
    }

    if(response.statusCode === '-2'){
        //update pending ticket status
        const updateReq = await ticketHandler.updateTicketRequestStatus(pendingReqData.userId, pendingReqData.ticketType, false);
        if(updateReq.err) {
            logger.error(`Error in updating pending ${reqType} ticket creation requests.`);
        }
        logger.info(`successfully updated ticket creation request status. Data: ${JSON.stringify(updateReq.data)}`);
    }

    logger.error(`ISSUE REPORTING SVC: Date: ${new Date()}| error in Ticket creation: ${reqBody.EXTTransactionID}. Response: ${JSON.stringify(response)}`);
    throw new BadRequestError(`Error in Ticket creation: ${reqBody.EXTTransactionID}. Response: ${JSON.stringify(response)}`);
};

/**
 * Get all tickets
 * @param {Boolean} openStatus - open status
 * @returns {Promise<void>}
 */
module.exports.getAllTickets = async (openStatus) => {
    if (_.isNull(openStatus)) {
        logger.error('openStatus is empty');
        throw new BadRequestError('openStatus cannot be empty');
    }
    const received = await ticketHandler.getAllTickets(openStatus);
    if (received.err) {
        logger.error('Error in get all tickets.', received.err);
        throw new ForbiddenError(`Error in get all tickets. Response: ${JSON.stringify(received)}`);
    }
    return received;
};

/**
 * Get all tickets by User Id
 * @param {String} userId - User Id
 * @param openStatus
 * @returns {Promise<void>}
 */
module.exports.getAllTicketsByUserId = async (userId, openStatus) => {
    if (_.isEmpty(userId)) {
        logger.error('Unauthorized action Missing userId.');
        throw new BadRequestError('Unauthorized action. Missing userId.');
    }
    const received = await ticketHandler.getAllTicketsByUserId(userId, openStatus);
    if (received.err) {
        logger.error('Error in get all tickets by UserId.', received.err);
        throw new ForbiddenError(`Error in get all tickets by UserId. Response: ${JSON.stringify(received)}`);
    }
    return received;
};

/**
 * Get ticket status by ticketId
 * @param {String} ticketId - ticket Id
 * @returns {Promise<void>}
 */
module.exports.getTicketStatus = async (ticketId) => {
    if (_.isEmpty(ticketId)) {
        logger.error('ticketId  is empty');
        throw new BadRequestError('ticketId cannot be empty');
    }

    const received = await ticketHandler.getTicketStatus(ticketId);
    if (received.err) {
        logger.error('Error in get tickets status.', received.err);
        throw new ForbiddenError(`Error in get tickets status. Response: ${JSON.stringify(received)}`);
    }

    return received;
};

/**
 * Get ticket status by TransactionId
 * @param {String} transactionId - transaction Id
 * @returns {Promise<void>}
 */
module.exports.getTicketStatusByTransactionId = async (transactionId) => {
    if (_.isEmpty(transactionId)) {
        logger.error('transactionId  is empty');
        throw new BadRequestError('transactionId cannot be empty');
    }

    const received = await ticketHandler.getTicketStatusByTransactionId(transactionId);
    if (received.err) {
        logger.error('Error in get tickets status by transactionId.', received.err);
        throw new ForbiddenError(`Error in get tickets status by transactionId. Response: ${JSON.stringify(received)}`);
    }

    return received;
};

/**
 * Notify ticket status by ticketId
 * @param {String} ticketId - ticket Id
 * @param {String} userId - user Id
 * @returns {Promise<void>}
 */
module.exports.notifyTicketStatus = async (ticketId, userId) => {
    if (_.isEmpty(ticketId) || _.isEmpty(userId)) {
        logger.error('ticketId or userId cannot be empty');
        throw new BadRequestError('ticketId or userId cannot be empty');
    }

    const received = await ticketHandler.getTicketStatus(ticketId);
    if (received.err) {
        logger.error('Error in get tickets status.', received.err);
        throw new ForbiddenError(`Error in get tickets status. Response: ${JSON.stringify(received)}`);
    }
    const ticket = received.data;
    if(!_.isEmpty(ticket.ticketStatus)){
        //send notification to user
        const notification = {
            notifications: [
                {
                    userId: userId,
                    header: 'Support ticket status changed to: ' + ticket.ticketStatus.status,
                    isChecked: false,
                    referenceId: ticketId,
                    notificationType: NOTIFICATION_TYPES.ACTIVITY,
                    category: NOTIFICATION_CATEGORIES.TICKET_ASSURANCE,
                    imageUrl: '',
                    body: {
                        topic: `Ticket ID #${ticketId}`,
                        description: 'Ticket status has been changed. Our team is processing your request', // VD not given
                        data: {
                            ticket: ticket,
                        }
                    }
                }
            ]
        };
        return notificationService.sendUserNotification(notification, userId);
    }

    logger.error('Error in sending ticket status notification. ticketStatus could be empty');
    throw new ForbiddenError('Error in sending ticket status notification');
};

/**
 * Update ticket status by ticketId/transactionId
 * @param ticketOrTransactionId
 * @param {String} ticketStatus - ticket Status
 * @param {Boolean} isDraft - true: is draft ticket, false: if real ticket with ticketId
 * either installation or Fisik)
 * @returns {Promise<void>}
 */
module.exports.updateTicketStatus = async (ticketOrTransactionId, ticketStatus, isDraft) => {
    if (_.isEmpty(ticketOrTransactionId) || _.isEmpty(ticketStatus)) {
        logger.error('ticketId/transactionId or ticketStatus is/are empty');
        throw new BadRequestError('ticketId/transactionId or ticketStatus cannot be empty');
    }

    let ticketData;
    if(isDraft){
        ticketData = await ticketHandler.getTicketByTransactionId(ticketOrTransactionId);
    } else {
        ticketData = await ticketHandler.getTicketByTicketId(ticketOrTransactionId);
    }
    //get ticket by ticketOrTransactionId
    if(_.isEmpty(ticketData.data)) {
        logger.error(`Ticket data not found for ${isDraft? 'transactionId': 'ticketId'} : ${ticketOrTransactionId}`);
        throw new NotFoundError('Ticket data not found');
    }

    let headerPrefix = 'Support ticket';
    let ticketTypesArr;
    if (ticketData.data.categoryType === CATEGORY_TYPES.FISIK) {
        headerPrefix = 'Technician Support ticket';
        ticketTypesArr = [FISIK_SUBMITTED.status, FISIK_IN_PROGRESS.status, FISIK_COMPLETED.status, FISIK_RESOLVED.status];
    } else {
        // LOGIC & ADMIN types
        ticketTypesArr = [LOGIK_TICKET_RECEIVED.status, LOGIK_TICKET_IN_PROGRESS.status, LOGIK_TICKET_SOLVED.status];
    }

    if (!ticketTypesArr.includes(ticketStatus.toUpperCase())) {
        logger.error('Invalid Ticket Status.');
        throw new BadRequestError('Invalid Ticket Status.');
    }

    let updated = await ticketHandler.updateTicketStatus(ticketOrTransactionId, ticketStatus.toUpperCase(), isDraft);
    if (updated.err) {
        logger.error('Error in ticket status update.', updated.err);
        throw new ForbiddenError(`Error in ticket status update. Response ${JSON.stringify(updated)}`);
    }
    // todo: manual change status because DB update delay
    updated.data.ticketStatus.status = ticketStatus;
    // send status ticket update notification
    const notification = {
        notifications: [
            {
                userId: updated.data.userId,
                header: `${headerPrefix} status changed to  ${ticketStatus}`,
                isChecked: false,
                referenceId: ticketOrTransactionId,
                notificationType: NOTIFICATION_TYPES.ACTIVITY,
                category: NOTIFICATION_CATEGORIES.TICKET_ASSURANCE,
                imageUrl: '',
                body: {
                    topic: updated.data.ticketStatus.title,
                    description: updated.data.ticketStatus.text,
                    data: {
                        ticket: updated.data,
                    }
                }
            }
        ]
    };

    notificationService.sendUserNotification(notification, updated.data.userId)
        .catch(e => logger.error('send notification failed', e));

    return updated;
};

/**
 * update the ticket ID by admin
 * @param transactionId
 * @param ticketId
 */
module.exports.updateTicketId = (transactionId, ticketId) => {
    if (_.isEmpty(transactionId) || _.isEmpty(ticketId)) {
        logger.error('transactionId or ticketId is empty');
        throw new BadRequestError(`transaction id or ticket id cannot be empty`);
    }

    const updated = ticketHandler.updateTicketId(transactionId, ticketId);
    if (updated.err) {
        logger.error('Error in update ticketId.', updated.err);
        throw new ForbiddenError(updated.err.message || '');
    }
    logger.info(`Successfully updated ticketId. Response: ${JSON.stringify(updated)}`);
    return updated;
};

/**
 * Generate request body for new ticket
 *
 * @param {Object} ticketData - ticket data
 * @param {Object} userData - user details
 * @param {Object} symptomObj - symptomObj details
 * @param {Object} accountData - user account data
 * @param {String} ticketType - Ticket type
 * @param {String} language - user selected language
 * @returns {Object}
 */
async function generateRequestBody(ticketData, userData, symptomObj, accountData, ticketType, language) {
    //@todo generate object properties
    //https://pccwsolutions.atlassian.net/browse/MYIN-2605:
    const txnId = TICKET_PREFIX + new Date().getTime();
    const now = new Date().toISOString();
    return {
        EXTTransactionID: txnId,
        locale: reportIssueParams.locale,
        open: true,
        incidentList: {
            incident: [
                {
                    assetNum: await getAssetNum(accountData, ticketData.categoryId),
                    class: reportIssueParams.class,
                    description: ticketData.issue,
                    longDescription: ticketData.message,
                    externalSystem: reportIssueParams.externalSystem,
                    hierarchy: await getIssueHierarchy(symptomObj),
                    internalPriority: reportIssueParams.internalPriority,
                    reportedBy: accountData.name,
                    reportDate: now,
                    statusDate: now,
                    channel: reportIssueParams.channel,
                    contactEmail: accountData.email,
                    contactName: accountData.name,
                    contactPhone: accountData.mobile,
                    gaul: reportIssueParams.gaul,
                    hardComplaint: reportIssueParams.hardComplaint,
                    lapul: reportIssueParams.lapul,
                    urgensi: reportIssueParams.urgensi,
                    ticketType: ticketType,
                    idReference: txnId,
                    symptomID: reportIssueParams.symptomID,
                    bookingID: ticketData.bookingId, //no bookingId for Help submit issue
                    symptomName: await getSymptomName(ticketData, language),
                    segment: reportIssueParams.segment
                }
            ]
        }
    };
}

/**
 * Determine issue hierarchy by ticket data
 *
 * @param {Object} symptomObj - symptomObj data
 * @returns {Promise<*>}
 */
async function getIssueHierarchy(symptomObj) {
    // returns sample 'C_INTERNET \\\\ C_INTERNET_001 \\\\ C_INTERNET_001_001 \\\\ C_INTERNET_001_001_001'
    return  symptomObj.symptom ? symptomObj.symptom : '';
}

/**
 * Get asset Number
 *
 * @param {Object} accountData - account data
 * @param categoryId
 * @returns {Promise<string>}
 */
async function getAssetNum(accountData, categoryId) {
    const {ncli, fixedPhoneNum, internetNum} = accountData;
    let assetNum;
    //TODO: this logic confirmation is required
    let indiHomeNum = internetNum;
    let type = CATEGORIES.EN.INTERNET.TYPE;
    if (categoryId === CATEGORIES.EN.TELEPHONE.ID || categoryId === CATEGORIES.ID.TELEPHONE.ID) {
        indiHomeNum = fixedPhoneNum;
        type = CATEGORIES.EN.TELEPHONE.TYPE;
    } else if (categoryId === CATEGORIES.EN.TV.ID || categoryId === CATEGORIES.ID.TV.ID) {
        type = CATEGORIES.EN.TV.TYPE;
    } else if (categoryId === CATEGORIES.EN.INSTALLATION.ID || categoryId === CATEGORIES.ID.INSTALLATION.ID) {
        type = CATEGORIES.EN.INSTALLATION.TYPE;
    } else if (categoryId === CATEGORIES.EN.ADMIN_AND_BILLING.ID || categoryId === CATEGORIES.ID.ADMIN_AND_BILLING.ID) {
        type = CATEGORIES.EN.ADMIN_AND_BILLING.TYPE;
    }
    assetNum = `${ncli}_${indiHomeNum}_${type}`;
    logger.debug(`AssetNum: ${assetNum}`);
    return assetNum;
}

/**
 * Get symptomName
 * @param {Object} ticketData - ticket data
 * @param {String} language - language
 * @returns {Promise<*>}
 */
async function getSymptomName(ticketData, language) {
    if(language === LANGUAGE.ID) {
        return ticketData.issue;
    }
    const issueId = ticketData.issueId.replace(`_${LANGUAGE.EN}`, `_${LANGUAGE.ID}`);
    const issueObj = await module.exports.getSupportIssueByIssueId(issueId, LANGUAGE.ID);

    if(_.isEmpty(issueObj) || issueObj.err || !issueObj.issue) {
        logger.error('Error in getting symptomName', issueObj.err);
        throw new InternalServerError(`Error in getting symptomName. Error: ${issueObj.err}`);
    }
    const symptomName = issueObj.issue;
    logger.info(`symptomName: ${symptomName}`);
    return symptomName;
}

/**
 * Close or reopen ticket
 *
 * @param {Object} ticketId - ticket data
 * @param {String} action - action: REOPEN/CLOSED
 * @param {Object} bookingData - booking Data
 * @returns {Promise<*|{err, data}>}
 */
const closeOrReopenTicket = async (ticketData, action, bookingData) => {
    const categoryType = ticketData.categoryType;

    if(action === TICKET_ACTIONS.CLOSED) {
        ticketData.open = false;
    } else if( action === TICKET_ACTIONS.REOPEN || action === TICKET_ACTIONS.RESCHEDULE) {
        ticketData.open = true;
        // set ticketStatus to default IN_PROGRESS
        if(categoryType === CATEGORY_TYPES.LOGIC || categoryType === CATEGORY_TYPES.ADMIN) {
            ticketData.ticketStatus = LOGIK_TICKET_IN_PROGRESS;
        } else if(categoryType === CATEGORY_TYPES.FISIK) {
            ticketData.ticketStatus = FISIK_SUBMITTED;
            //update technician & assigned status data
            ticketData.technicianAssigned = true;
            ticketData.incidentList.incident[0].bookingID = bookingData.bookingId;
            ticketData.technician = bookingData && bookingData.technician ? bookingData.technician: null;
            const bookingSlot = await getBookingSlotFromBookingId(bookingData.bookingId);
            if (!bookingSlot.err) {
                logger.info(`Fetched booking Slot data ${JSON.stringify(bookingSlot)}`);
                ticketData.bookingSlot = bookingSlot.data;
                ticketData.scheduledTime = moment(bookingSlot.data['dateTime'], 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
            }
        }
    }
    logger.info(`Update Ticket data ${JSON.stringify(ticketData)}`);
    const updated = await ticketHandler.updateTicket(ticketData);

    if (updated.err) {
        logger.error('Error in ticket update.', updated.err);
        throw new ForbiddenError(`Error in ticket update. Error ${JSON.stringify(updated.err)}`);
    }
    logger.info(`Successfully updated ${action} ticket`);
    return ticketData;
};

/**
 * Reopen / reschedule / close the Fisik ticket
 *
 * @param {String} TicketId - ticketId of the ticket
 * @param {String} action - action REOPEN/RESCHEDULE/ CLOSED
 * @param {String} userId - user Id
 * @param {String} bookingId - booking Id
 * @returns {Promise<{ticketData: (*|{err, data}), status: string}>}
 */
module.exports.closeOrReopenOrRescheduleTicket = async (ticketId, action, userId, bookingId) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;

    if (_.isEmpty(userId)) {
        logger.error('Unauthorised action. userId is missing');
        throw new ForbiddenError('User is not authorised');
    }
    if (_.isEmpty(ticketId)) {
        logger.error('ticketId is missing');
        throw new ForbiddenError('ticketId cannot be empty');
    }

    if(![TICKET_ACTIONS.REOPEN, TICKET_ACTIONS.RESCHEDULE, TICKET_ACTIONS.CLOSED].includes(action)) {
        logger.debug(`Invalid Ticket action: ${action}`);
        throw new BadRequestError('Invalid action');
    }

    logger.info(`Ticket Id: ${ticketId}`);
    logger.info(`action: ${action}`);
    logger.info(`User Id: ${userId}`);


    //get ticket by ticketId
    let ticketData = await ticketHandler.getTicketByTicketId(ticketId);
    if(ticketData.err){
        logger.info(`Error in getting ticket data. Error ${JSON.stringify(ticketData.err)}`);
        throw new ForbiddenError('Error in getting ticket data');
    }

    ticketData = ticketData.data;
    if(_.isEmpty(ticketData)) {
        logger.error(`Ticket data not found for ticket Id: ${ticketId}`);
        throw new NotFoundError('Ticket data not found');
    }

    //flow validations
    if(action === TICKET_ACTIONS.CLOSED && !_.isEmpty(bookingId)) {
        logger.error(`Invalid param bookingId is given for action ${action}.`);
        throw new BadRequestError(`Invalid param bookingId is given for action ${action}.`);
    }
    if(ticketData.categoryType === CATEGORY_TYPES.FISIK) {
        if([TICKET_ACTIONS.REOPEN, TICKET_ACTIONS.RESCHEDULE].includes(action) && _.isEmpty(bookingId)) {
            logger.error(`BookingId is mandatory for Fisik ${action}.`);
            throw new BadRequestError(`BookingId is mandatory for Fisik ${action}.`);
        }
    }
    if(ticketData.categoryType === CATEGORY_TYPES.ADMIN || ticketData.categoryType === CATEGORY_TYPES.LOGIC) {
        if(action === TICKET_ACTIONS.RESCHEDULE || !_.isEmpty(bookingId)) {
            logger.error(`Invalid params given for ticket of type ${ticketData.categoryType}.`);
            throw new BadRequestError(`Invalid params given for ticket of type ${ticketData.categoryType}.`);
        }
    }

    // check remaining reschedule attempts
    if(action === TICKET_ACTIONS.RESCHEDULE && !_.isEmpty(bookingId) && ticketData.categoryType === CATEGORY_TYPES.FISIK) {
        let attempts = ticketData.rescheduleAttempts;
        logger.info(`Reschedule request received. Reschedule attempts count: ${attempts}`);
        if (attempts <= 0) {
            logger.error('REOPEN: Max reschedule attempts reached.');
            throw new MaxAttemptsReachedError('Max reschedule attempts reached.');
        }
    }

    //todo: check if this is a draft ticket, yes: block reopening, else continue

    if([TICKET_ACTIONS.REOPEN, TICKET_ACTIONS.CLOSED].includes(action)) {
        const reqBody = {
            ticketId,
            status: action === TICKET_ACTIONS.CLOSED ? action : 'BACKEND',
            channel: SUPPORT_TICKET_CHANNEL
        };

        const options = {
            method: 'POST',
            uri: reopenFisikTicketEndpoint,
            headers: {
                'Content-Type': 'application/json',
                Authorization: authorizationHeader
            },
            json: reqBody,
            strictSSL: false
        };
        logger.info(`ISSUE REPORTING SVC: Date: ${new Date()}| sending request to ${action} Ticket. Request: ${JSON.stringify(options)}`);
        const response = await rp.post(options);
        logger.info(`ISSUE REPORTING SVC: Date: ${new Date()}| received response data from Ticket ${action}. Response: ${JSON.stringify(response)}`);
        if (response.statusCode === '0') {
            return await updateScheduleData(action, bookingId, ticketData, userId);
        }
        logger.error(`ISSUE REPORTING SVC: Date: ${new Date()}| received ERROR response from Ticket ${action}`, response);
        throw new BadRequestError(`ERROR in ticket ${action}. Response ${JSON.stringify(response)} `);

    } else if(action === TICKET_ACTIONS.RESCHEDULE) {
        return await updateScheduleData(action, bookingId, ticketData, userId);
    }

    logger.error(`ISSUE REPORTING SVC: Date: ${new Date()}| ERROR in Ticket ${action}`);
    throw new BadRequestError(`ERROR in ticket. action ${action}`);
};

/**
 * Update schedule data
 * @param {String} action - action REOPEN/RESCHEDULE/ CLOSED
 * @param {String} bookingId - booking Id
 * @param {Object} ticketData - ticket Data
 * @param {String} userId - user Id
 * @returns {Promise<{ticketData: (*|{err, data}), message: string, status: string}>}
 */
async function updateScheduleData(action, bookingId, ticketData, userId) {
    logger.info(`Updating schedule after ${action}`);
    let updateIncidentBooking;
    if (action === TICKET_ACTIONS.REOPEN || action === TICKET_ACTIONS.RESCHEDULE) {
        // if booking Id is passed it's a Fisik reopen/reschedule
        if (!_.isEmpty(bookingId)) {
            // decrement attempts
            if(action === TICKET_ACTIONS.RESCHEDULE) {
                ticketData.rescheduleAttempts -= 1;
                logger.info(`Decremented Reschedule attempts count: ${ticketData.rescheduleAttempts}`);
            } else if(action === TICKET_ACTIONS.REOPEN) {
                // reset max reschedule attempts = 2
                ticketData.rescheduleAttempts = FISIK_TICKET_MAX_RESCHEDULE_ATTEMPTS;
                logger.info(`Reset Reschedule attempts count: ${ticketData.rescheduleAttempts}`);
            }
            logger.info(`Booking Id: ${bookingId}`);
            // update ticket data on db
            updateIncidentBooking = await updateIncidentTicket(ticketData, bookingId, userId);
        }
    }

    const bookingData = updateIncidentBooking && updateIncidentBooking.bookingData? updateIncidentBooking.bookingData: null;
    // update ticket data on db
    const updatedTicket = await closeOrReopenTicket(ticketData, action, bookingData);

    let updateResponse =  {
        status: 'success',
        ticketData: updatedTicket,
        message: `action: ${action} support ticket success`
    };

    if(updateIncidentBooking && updateIncidentBooking.status) {
        updateResponse.message =  `action: ${action} fisik support ticket success`;
    } 
    logger.info(`Successfully ${action} ticket. Response: ${JSON.stringify(updateResponse)}`);
    return updateResponse;   
}
/**
 * Update incident Ticket
 * @param {Object} ticketData - ticket data
 * @param {String} bookingId - booking Id of the ticket
 * @param {String} userId - user Id
 * @returns {Promise<{data: *, status: string}>}
 */
async function updateIncidentTicket(ticketData, bookingId, userId) {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;

    const reqBody = {
        EXTTransactionID: ticketData.EXTTransactionID,
        locale: INDONESIAN_LOCALE,
        incident: {
            ticketID: ticketData.ticketId,
            bookingID: bookingId
        }
    };

    const options = {
        method: 'POST',
        uri: updateIncidentTicketEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorizationHeader
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`ISSUE REPORTING SVC: Date: ${new Date()}| sending request to update incident ticket. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    
    logger.info(`ISSUE REPORTING SVC: Date: ${new Date()}| got response incident ticket. for \n Req : ${JSON.stringify(options)} as  Res: ${JSON.stringify(response)}`);

    if (response.statusCode === '0') {
        // save new booking in db
        const booked = await technicianService.generateBookingData(userId, bookingId);
        if(booked.err) {
            logger.error('Error in save booking to DB.', booked.err);
            throw new ForbiddenError('Error in save booking to DB');
        }

        return {
            status: 'success',
            transactionId: response.data ? response.data.EXTTransactionID : ticketData.EXTTransactionID,
            bookingData: booked.data,
        };

    }
    logger.error(`ERROR in updating incident ticket ${JSON.stringify(response)}`);
    throw new BadRequestError('ERROR in updating incident ticket');
}

/**
 * load all support issues
 * @param {String}  language default: en
 * @returns {Promise<*>}
 */
module.exports.getAllSupportTicketCategories = async (language = LANGUAGE.EN) => {
    if (language === LANGUAGE.EN || language === LANGUAGE.ID) {
        return ticketCategoriesHandler.getAllIssuesByLanguage(language);
    }
    logger.error(`Invalid language: ${language}`);
    throw new BadRequestError('Invalid language');
};

/**
 * todo: category validation
 * get All SupportTickets In Category
 * @param {String} category
 * @param {String}  language default: en
 * @returns {Promise<*[]>}
 */
module.exports.getAllSupportTicketsInCategory =  async (category, language = LANGUAGE.EN) => {
    if (!category || !language) {
        logger.error(`Invalid category: ${category}/language: ${language}.`);
        throw new BadRequestError(`category is invalid : ${category}`);
    }
    return (await this.getAllSupportTicketCategories(language))
        .filter(issue => issue['category'].toUpperCase() === category.toUpperCase());
};

/**
 *  null if not found
 * @param id of the issue
 * @param {String}  language default: en
 * @returns {Promise<null|*>}
 */
module.exports.getSupportIssueByIssueId = async (issueId, language = LANGUAGE.EN) => {
    const filtered = (await this.getAllSupportTicketCategories(language)).filter(i => i['issueId'] === issueId);
    if (filtered && filtered.length === 1) {
        return filtered[0];
    }
    return null;
};

/**
 *  null if not found
 * @param id of the issue
 * @param {Boolean} isSpec
 * @param {Boolean} isFiber
 * @returns {Promise<null|*>}
 */
module.exports.getSupportTicketSymptom = async (issueObj, isSpec, isFiber = true) => {
    const key = `${isFiber ? 'fiber' : 'copper'}${isSpec ? 'Spec' : 'UnderSpec'}`;
    logger.info(`Symptom key ${key}`);
    return issueObj[key];
};

/**
 *  null if not found
 * @param id of the issue
 * @param {Boolean} isSpec
 * @param {Boolean} isFiber
 * @param {String}  language default: en
 * @returns {Promise<null|*>}
 */
module.exports.getSupportTicketSymptomById = async (issueId, isSpec, isFiber = true, language = LANGUAGE.EN) => {
    const key = `${isFiber ? 'fiber' : 'copper'}${isSpec ? 'Spec' : 'UnderSpec'}`;
    logger.info(`Symptom key ${key}`);
    const filtered = (await this.getAllSupportTicketCategories(language)).filter(i => i['issueId'] === issueId);
    if (filtered && filtered.length === 1) {
        return filtered[0][key];
    }
    return null;
};

/**
 * null if not found
 * @param {String} issue name of the issue
 * @param {Boolean} isSpec
 * @param {Boolean} isFiber
 * @param {String}  language default: en
 * @returns {Promise<null|*>}
 */
module.exports.getSupportTicketSymptomByName = async (issue, isSpec, isFiber = true, language = LANGUAGE.EN) => {
    const key = `${isFiber ? 'fiber' : 'copper'}${isSpec ? 'Spec' : 'UnderSpec'}`;
    const filtered = (await this.getAllSupportTicketCategories(language)).filter(i => i['issue'].toUpperCase() === issue.toUpperCase());
    if (filtered && filtered.length === 1) {
        return filtered[0][key];
    }
    return null;
};

/**
 * Get category by issue
 * @param {String} issue name of the issue
 * @param {String}  language default: en
 * @returns {Promise<null|*>}
 */
module.exports.getCategoryByIssueName = async (issue, language = LANGUAGE.EN) => {
    const filtered = (await this.getAllSupportTicketCategories(language)).filter(i => i['issue'].toUpperCase() === issue.toUpperCase());
    if (filtered && filtered.length === 1) {
        return filtered[0];
    }
    return null;
};

/**
 * Get cable type information
 * @param {String} number - indihome number or fixed phone number
 * @param {String} serviceName - service Name
 * @returns {Promise<*>}
 */
module.exports.checkCableType =  async (number, serviceName) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;
    serviceName = serviceName.toUpperCase();
    if (_.isEmpty(number) || _.isEmpty(serviceName)) {
        logger.error('number or serviceName is empty');
        throw new ForbiddenError('Indihome number(fixed phone number) or service Name cannot be empty');
    }
    const reqBody = {
        ND: number,
        serviceName: serviceName.toUpperCase()
    };

    const options = {
        method: 'POST',
        uri: checkCableTypeEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorizationHeader
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`ISSUE REPORTING SVC: Date: ${new Date()}| sending request to check cable type. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    if (response.statusCode === '0') {
        logger.info(`ISSUE REPORTING SVC: Date: ${new Date()}| received data for check cable type. Res: ${JSON.stringify(response.data)}`);
        logger.info(`Cable Type ${response.data.cableType}`);
        return response.data.cableType? response.data.cableType : null;
    }
    logger.error(`ISSUE REPORTING SVC: Date: ${new Date()}| ERROR in checking cable type`);
    throw new BadRequestError('ERROR in checking cable type');
};

/**
 * Check if Ibooster in range in given data
 * @param {Object} data - Ibooster check API data
 * @returns {boolean}
 */
function checkIboosterRange(data) {
    const range = data.onu_rx_pwr;
    if (IBOOSTER_RANGE.MIN < range && range < IBOOSTER_RANGE.MAX) {
        logger.info('IBooster in range.');
        return true;
    }
    logger.info('Ibooster not in range');
    return false;
}

/**
 * Get Ibooster information
 * @param {String} number - indihome number or fixed phone number
 * @returns {Promise<boolean>}
 */
module.exports.getIboosterInfo = async (number) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;

    if (_.isEmpty(number)) {
        logger.error('number is empty.');
        throw new ForbiddenError('Indihome number(fixed phone number) cannot be empty');
    }
    const reqBody = {
        nd: number,
        realm: TELKOM_REALM
    };

    const options = {
        method: 'POST',
        uri: checkIboosterRangeEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorizationHeader
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`ISSUE REPORTING SVC: Date: ${new Date()}| sending request to check iBooster spec range. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);

    if (response.statusCode === '0') {
        logger.info(`ISSUE REPORTING SVC: Date: ${new Date()}| Ibooster data Successfully received. Data: ${JSON.stringify(response.data)}`);
        return response.data;
    }
    logger.info(`ISSUE REPORTING SVC: Date: ${new Date()}| Error in getting Ibooster Data.`);
    throw new BadRequestError('ERROR in checking iBooster spec range');
};

/**
 * Check if ACS has reset in past hour
 * @param {String} indiHomeNum - indiHome number
 * @returns {Promise<boolean>}
 */
module.exports.checkACSResetStatus = async (indiHomeNum) => {
    if (_.isEmpty(indiHomeNum)) {
        logger.error('indiHomeNum is empty');
        throw new ForbiddenError('indiHomeNum cannot be empty');
    }

    const status = await hardwareHandler.getModemStatus(indiHomeNum);
    if (status.err) {
        logger.info(`Failed to get modem reset status in DB. Error: ${JSON.stringify(status.err)}`);
        throw new InternalServerError('Failed to get modem reset status in DB');
    }

    if(_.isEmpty(status.data)) {
        logger.info(`No ACS reset data found for this indiHomeNum: ${indiHomeNum}. So ACS has NOT reset in past hour`);
        return false;
    }

    const lastRebootTime =  status.data.rebootTime;
    const now = new Date().toISOString();
    const duration = moment.duration(moment(now, ISO_DATE_FORMAT).diff(moment(lastRebootTime, ISO_DATE_FORMAT)));
    const minutes = duration.asMinutes().toFixed(PRECISION_TWO_DECIMALS);

    if(minutes <= ACS_RESET_CUTOFF) {
        logger.info(`ACS has reset in past hour. ${minutes} minutes Ago`);
        return true;
    }
    logger.info('ACS has NOT reset in past hour');
    return false;
};

/**
 *
 * @param {String} number - indihome number or fixed phone number
 * @returns {Promise<{data: {nd: *, message: *}, status: string}>}
 */
/**
 * Reset Modem
 *
 * @param {String} number - indihome number or fixed phone number
 * @param {String} userId - user Id
 * @returns {Promise<{data: {rebootStatus: string, indiHomeNum: *, userId: *, rebootTime: string}, status: string}>}
 */
module.exports.resetModem = async(number, userId) => {
    const token = await authenticationService.getJWTFForUser();
    const authorizationHeader = 'Bearer ' + token;

    if (_.isEmpty(number)) {
        logger.error('number is empty');
        throw new ForbiddenError('Indihome number(fixed phone number) cannot be empty');
    }

    const reqBody = {
        nd: number,
        realm: TELKOM_REALM
    };

    const options = {
        method: 'POST',
        uri: resetModemEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorizationHeader
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`ISSUE REPORTING SVC: Date: ${new Date()}| sending request to reset MODEM. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);

    if (response.statusCode === '0') {
        //save in db
        const now = new Date().toISOString();
        const resetData =  {
            indiHomeNum: number,
            userId,
            rebootTime: now,
            updatedTime: now,
            rebootStatus: 'SUCCESS'
        };
        const updated = await hardwareHandler.resetModemStatus(resetData);
        if (updated.err) {
            logger.info(`Failed to update modem reset status in DB. Error: ${JSON.stringify(updated.err)}`);
            throw new InternalServerError('Failed to modem reset status in DB');
        }

        return {
            status: 'success',
            data: {
                indiHomeNum: number,
                userId,
                rebootTime: now,
                rebootStatus: 'SUCCESS'
            },
        };

    }
    logger.error(`ERROR in resetting MODEM. Response: ${JSON.stringify(response)}`);
    throw new BadRequestError('ERROR in resetting MODEM');
};

async function getBookingSlotFromBookingId(bookingId) {
    let bookingSlot = await technicianHandler.findServiceSlotByBookingId(bookingId);
    if (bookingSlot) {
        return bookingSlot;
    }
    logger.error('booking slot not found');
    return new ForbiddenError('booking slot not found');
}

module.exports.sendCloseReminderNotification = async(ticketId, bookingId, userId) => {
    // send status ticket update notification
    const notification = {
        notifications: [
            {
                userId: userId,
                header: 'test Notification',
                isChecked: false,
                referenceId: ticketId,
                notificationType: NOTIFICATION_TYPES.ACTIVITY,
                category: NOTIFICATION_CATEGORIES.TICKET_ASSURANCE_REMINDER,
                imageUrl: '',
                body: {
                    topic: 'updated.data.ticketStatus.title',
                    description: 'updated.data.ticketStatus.text',
                    data: {
                        ticket: ticketId,
                    }
                }
            }
        ]
    };

    return notificationService.sendUserNotification(notification, userId);
};

/**
 * Get scheduled tickets within the given time
 * @param {String} time - time
 * @returns {Promise<*>}
 */
module.exports.getTicketsForTime = async (time) => {
    return await technicianHandler.getTicketsForTime(time);
};

module.exports.getSubmittedTicketsForTime = async (time) => {
    return await technicianHandler.getSubmittedTicketsForTime(time);
};
