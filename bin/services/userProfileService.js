/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 26/11/19
*/
const rp = require('request-promise');
const Logger = require('../helpers/utils/logger');
const logger = new Logger('NotificationService');
const wrapper = require('../helpers/utils/wrapper');
const {InternalServerError} = require('../helpers/error');
const config = require('../config');
const userProfileServiceUrl = config.get('/userProfileService');

/**
 * Get account by indiHomeNum
 * @param {String} indiHomeNum - indiHome Number
 * @returns {Promise<{err: null, data: *}>}
 */
module.exports.getUserAccount = async(indiHomeNum) => {
    const options = {
        method: 'GET',
        uri: userProfileServiceUrl.host + userProfileServiceUrl.getUserAccountEndpoint.replace(':indiHomeNum', indiHomeNum),
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: 'Basic ' + config.get('/authorization').token,
        },
        json: true,
        strictSSL: false
    };
    logger.info('Get user profile of the user');
    const result = await rp.get(options);
    if (result.err) {
        logger.error('Error in getting profile data.', result.err);
        throw new InternalServerError('Error in getting profile data');
    }

    logger.info(`Received profile data: ${JSON.stringify(result.data)}`);
    return wrapper.data(result.data);
};

/**
 * get all user accounts
 * @param user
 */
module.exports.getUserProfile = async (user) => {
    const options = {
        method: 'GET',
        uri: userProfileServiceUrl.host + userProfileServiceUrl.getUserProfileEndpoint,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            Authorization: `Bearer ${user.token}`,
        },
        json: true,
        strictSSL: false
    };
    logger.info('Get user profile of the user');
    const result = await rp.get(options);
    if (result.err) {
        logger.error('Error in getting profile data.', result.err);
        throw new InternalServerError('Error in getting profile data');
    }

    logger.info(`Received profile data: ${JSON.stringify(result.data)}`);
    return result.data;
};

/**
 * Get user's default lactation is not activated first
 * @param user
 * @returns {null|*}
 */
module.exports.getDefaultLocation = async (user) => {
    const account = await this.getInactiveAccount(user);
    if (account) {
        return account.Location;
    }
    return null;
};

/**
 * get Inactive Account
 * @param user
 * @returns {Promise<{Location}|*|null>}
 */
module.exports.getInactiveAccount = async (user) => {
    const profile = await this.getUserProfile(user);

    if (profile && profile.accounts && profile.accounts.length > 0 && profile.accounts.find(ac => !ac.active)) {
        const account = profile.accounts.find(ac => !ac.active);
        const accountInfo = await this.getUserAccount(account.indiHomeNum);

        if (accountInfo && accountInfo.data && accountInfo.data.Location) {
            return accountInfo.data;
        }
    }
    return null;
};

/**
 * fixme: mark the account as activate
 * @param user
 * @returns {Promise<boolean>}
 */
module.exports.activateAccount = async (user) => {
    const account = this.getInactiveAccount(user);
    if (account) {
        // fixme: activate the account
    }
    return false;
};
