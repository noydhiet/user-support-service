/*
* Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Created by Nishani Fernando on 26/11/19
*/
const rp = require('request-promise');
const Logger = require('../helpers/utils/logger');
const logger = new Logger('NotificationService');
const config = require('../config');
const notificationHost = config.get('/notificationService').host;
const notificationEndpoint = config.get('/notificationService').push.endpoint;

module.exports.sendUserNotification = async(reqBody, userId) => {
    const options = {
        method: 'POST',
        uri: notificationHost + notificationEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Basic ' + config.get('/authorization').token,
        },
        json: reqBody,
        strictSSL: false
    };
    //  send notifications
    logger.info(`sending notification to the user: ${JSON.stringify(options)}`);
    rp(options)
        .then(() => logger.info(`send notification to the user: ${userId}`))
        .catch(err => logger.trace(err));
};
