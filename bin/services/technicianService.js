/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 7/11/2019, 6:28 PM
 */

const rp = require('request-promise');
const Logger = require('../helpers/utils/logger');
const logger = new Logger('TechnicianService');
const config = require('../config');
const _ = require('lodash');
const moment = require('moment');
const AuthenticationService = require('./clientAuthorizationService');
const notificationService = require('./notificationService');
const authenticationService = new AuthenticationService();
const technicianHandler = require('../modules/technician/handlers/api_handler');
const {BadRequestError, ForbiddenError, NotFoundError, ConflictError, PreconditionFailedError} = require('../helpers/error');

const {
    TECHNICIAN, ISO_DATE_FORMAT, ISO_DATETIME_FORMAT, TECHNICIAN_APPOINTMENT_DATE_FORMAT, ISO_DATETIME_WITH_SECS_FORMAT
} = require('../config/const/commonConstants');
const {BOOKED, ON_THE_WAY, IN_PROGRESS, COMPLETED} = require('../config/const/technicianStatus');
const {
    SUBMITTED: SERVICE_SUBMITTED,
    IN_PROGRESS: SERVICE_IN_PROGRESS,
    COMPLETED: SERVICE_COMPLETED,
    RESOLVED: SERVICE_RESOLVED
} = require('../config/const/fisikTicketStatus');
const listInstallationAppointmentsHost = config.get('/technician').schedule.listAvailableInstallationTimesHost;
const maxAttempts = config.get('/technician').schedule.listAvailableInstallationAttempts;
const listAvailableServiceTimesHost = config.get('/technician').schedule.listAvailableServiceTimesHost;
const reserveAssignmentScheduleHost = config.get('/technician').schedule.reserveAppointmentHost;
const finalizeInstallationHost = config.get('/technician').schedule.finalizeInstallationHost;
const rescheduleInstallationEndpoint = config.get('/technician').schedule.rescheduleAppointmentHost;
const maxRescheduleAttempts = config.get('/technician').schedule.maxRescheduleAttempts;
const crewInformationEndpoint = config.get('/technician').schedule.technicianCrewInformationEndpoint;
const assuranceCrewInformationEndpoint = config.get('/technician').schedule.assuranceCrewInformationEndpoint;
const technicianWorkorderInformationEndpoint = config.get('/technician').schedule.technicianWorkorderInformationEndpoint;
const technicianTimeSlotsMaxRetry = config.get('/technician').schedule.technicianTimeSlotsMaxRetry;

const wrapper = require('../helpers/utils/wrapper');
const {NOTIFICATION_CATEGORIES, NOTIFICATION_TYPES} = require('../config/const/commonConstants');
const {OK, RESCHEDULE_ATTEMPTS_EXCEEDED, NO_OPEN_BOOKINGS, WORK_ORDER_NOT_UPDATED} = require('../config/const/rescheduleStatus-en');
const {SUBMITTED} = require('../config/const/fisikTicketStatus');

const userProfileService = require('./userProfileService');
const defaultSto = config.get('/technician').schedule.defaultSTO;

const INSTALLATION_PREFIX = 'MYIRX-';

async function getAvailableInstallationTimesFromAPI(dateFrom, dateTo, sto) {
    let token = await authenticationService.getJWTFForUser();
    const authorization = `Bearer ${token}`;

    const reqBody = {
        'SCHEDULEDATE': `${dateFrom} TO ${dateTo}`,
        'STO': sto
    };

    const options = {
        method: 'POST',
        uri: listInstallationAppointmentsHost,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorization,
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`requesting data from the technician availability API on: ${new Date()} as ${JSON.stringify(options)}`);
    return rp.post(options);
}


async function getAvailableServiceTimesFromAPI(date, sto) {
    let token = await authenticationService.getJWTFForUser();
    const authorization = `Bearer ${token}`;

    const reqBody = {
        'scheduleDate': date,
        'RK': '',
        'STO': sto,
    };

    const options = {
        method: 'POST',
        uri: listAvailableServiceTimesHost,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorization
        },
        json: reqBody,
        strictSSL: false,
        timeout: 15000
    };

    logger.info(`requesting data from the technician service availability API on: ${new Date()} as ${JSON.stringify(options)}`);
    return rp.post(options);
}


/**
 * get next available/not-availability time slots for Installation
 * @param days
 * @param sto, if not provided, user's default location's sto wil be added
 * @param user
 * @returns {Promise<*|Uint8Array|BigInt64Array|{dateTime: *, crewId: *, information: *, bookingId: *}[]|
 * Float64Array|Int8Array|Float32Array|Int32Array|Uint32Array|Uint8ClampedArray|BigUint64Array|Int16Array|Uint16Array>}
 */
module.exports.getAvailableInstallationTimes = async (days, sto, user) => {
    if (!sto && user) {
        const location = await userProfileService.getDefaultLocation(user).catch(e => logger.error(e.message, e));
        if (location) {
            sto = location.sto;
        } else {
            sto = defaultSto;
        }
    }

    let fromDate = new Date().toISOString();
    let nonemptyDays = 0;
    let filteredSlots = [];
    let attempts = 0;
    let completed = false;
    do {
        let times = await getAvailableInstallationTimes(fromDate, days * 3, sto);
        //  filter the all not fully available dates
        if (times) {
            let groupedTimes = times.reduce((rv, x) => {
                (rv[x['date']] = rv[x['date']] || []).push(x);
                return rv;
            }, {});

            //  filter the all not fully available dates
            for (const day of Object.values(groupedTimes)) {
                if (nonemptyDays >= days) {
                    completed = true;
                    break;
                }
                //for a at least one available date
                if (day.some(slot => slot['availability'] === 1)) {
                    let daysSlots = day.reduce((rv, x) => {
                        (rv[x['slotId']] = rv[x['slotId']] || []).push(x);
                        return rv;
                    }, {});
                    for (const slots of Object.values(daysSlots)) {
                        const [firstSlot] = slots.sort((a, b) => b.availability - a.availability);
                        filteredSlots.push(firstSlot);
                    }
                    nonemptyDays++;
                }
            }
        }
        fromDate = new Date(Date.parse(fromDate) + (1000 * 60 * 60 * 24 * days * 3)).toISOString();
        logger.info(`retrying : ${attempts} attempts`);
        attempts++;
    } while (!(attempts > maxAttempts || completed));
    logger.info(`got non empty days: ${nonemptyDays} in ${attempts} attempts`);

    // index available times
    if (filteredSlots.length > 0) {
        let addAvailableSlots = await technicianHandler.addAvailableSlots(filteredSlots);
        if (addAvailableSlots.err) {
            logger.error('Failed to save Available Slots.', addAvailableSlots.err);
        }
    }

    return filteredSlots;
};

async function getAvailableInstallationTimes(date, days, sto) {
    let dateTo = new Date(Date.parse(date) + (1000 * 60 * 60 * 24 * days)).toISOString();
    const result = await getAvailableInstallationTimesFromAPI(date, dateTo, sto);

    logger.info(`got data from the API on: ${new Date()} for date: ${date} to: ${dateTo}`);
    if (result.statusCode === '-2') {
        logger.error(`Failed to receive data from the API on: ${new Date()} Error: ${result['returnMessage']}`);
        return [];
    }
    return result['data']['listAssignment']
        .filter(d => d['AVAILABILITY'])
        .map(d => {
            let timeSlot = getInstallationTimeSlot(d['JADWAL']);
            return {
                dateTime: d['JADWAL'],
                availability: d['AVAILABILITY'] === '1' ? 1 : 0,
                bookingId: d['BOOKINGID'],
                crewId: d['CREWID'],
                information: d['KETERANGAN'],
                timeSlot,
                date: timeSlot['date'],
                slotId: timeSlot['slotId']
            };
        })
        .filter(d => d !== {});
}

/**
 * get number of days response in parallel
 * @param {Integer} numberOfDays
 * @param {String} sto
 * @param {Date} startDate
 * @returns {Promise<[]>}
 */
async function getServiceTimesParallel(numberOfDays, sto, startDate) {
    const days = [...Array(numberOfDays).keys()]
        .map(i => {
            let date = new Date(startDate);
            date.setDate(date.getDate() + i);
            return date.toISOString();
        })
        .map(date => getAvailableServiceTimesFromAPI(date, sto).catch(e => logger.error(e)));

    let allResponses = await Promise.all(days);
    let filtered = [];
    if (allResponses) {
        filtered = allResponses.filter(result => result && result.statusCode === '0' && result.data && result.data.SCHEDULE)
            .filter(result => result.data.SCHEDULE.some(r => r['mx:AVAILABILITY'] === 'AVAILABLE'))
            .map(result => {
                //for a at least one available date
                let allSlots = result.data.SCHEDULE
                    .map(element => {
                        let timeSlot = getServiceTimeSlot(element['mx:JADWAL']);
                        return {
                            dateTime: element['mx:JADWAL'],
                            availability: element['mx:AVAILABILITY'] === 'AVAILABLE' ? 1 : 0,
                            crewId: element['mx:CREWID'],
                            information: element['mx:KETERANGAN'],
                            bookingId: element['mx:BOOKINGID'],
                            timeSlot,
                            date: timeSlot['date'],
                            slotId: timeSlot['slotId']
                        };
                    })
                    .filter(r => r.timeSlot)
                    .filter(r => r.slotId);

                let groupedSlots = allSlots.reduce((rv, x) => {
                    (rv[x['slotId']] = rv[x['slotId']] || []).push(x);
                    return rv;
                }, {});

                return Object.values(groupedSlots).map(slots => {
                    const [firstSlot] = slots.sort((a, b) => b.availability - a.availability);
                    return firstSlot;
                });
            });
    }
    return filtered;
}

/**
 * get next available/not-available time slots for Service
 * @param numberOfDays
 * @param sto
 * @param user
 * @returns {Promise<[]>}
 */
module.exports.getAvailableServiceTimes = async (numberOfDays, sto, user) => {

    if (!sto && user) {
        const location = await userProfileService.getDefaultLocation(user).catch(e => logger.error(e.message, e));
        if (location) {
            sto = location.sto;
        } else {
            sto = defaultSto;
        }
    }

    let serviceSlots = [];
    let nonEmptyDays = 0;

    for (let retries = 0; retries < technicianTimeSlotsMaxRetry && nonEmptyDays < numberOfDays; retries++) {
        let date = new Date();
        date.setDate(date.getDate() + retries * numberOfDays * 2);
        let filtered = await getServiceTimesParallel(numberOfDays * 2, sto, date);
        for (let i = 0; i < filtered.length; i++) {
            serviceSlots.push(...(filtered[i]));
            nonEmptyDays++;
            //  break to avoid adding additional dates
            if (nonEmptyDays >= numberOfDays) {
                logger.info(`found ${nonEmptyDays} nonempty days in ${retries + 1} attempts`);
                break;
            }
        }
    }

    // index available times
    if (serviceSlots.length > 0) {
        serviceSlots = serviceSlots
            .filter(s => s.date)
            .sort((a, b) => {
                const d1Splits = a.date.split('-');
                const d2Splits = b.date.split('-');
                const d1 = `${d1Splits[2]}-${d1Splits[1]}-${d1Splits[0]}`;
                const d2 = `${d2Splits[2]}-${d2Splits[1]}-${d2Splits[0]}`;
                if (d1 < d2) {
                    return -1;
                }
                if (d2 < d1) {
                    return 1;
                }
                return 0;
            });

        let addAvailableSlots = await technicianHandler.addAvailableServiceSlots(serviceSlots);
        if (addAvailableSlots.err) {
            logger.error('Failed to save Available Service Slots.', addAvailableSlots.err);
        }
    }
    return serviceSlots;
};

function getServiceTimeSlot(dateTime) {
    if (dateTime && dateTime.split(' ')[0] && dateTime.split(' ')[1]) {
        let date = dateTime.split(' ')[0];
        let time = dateTime.split(' ')[1];
        let hour = time.split(':')[0];
        if (hour >= 8 && hour < 10) {
            return {
                slotId: 'morning-1',
                slot: '8:00 - 10:00',
                time,
                date
            };
        } else if (hour >= 10 && hour < 12) {
            return {
                slotId: 'morning-2',
                slot: '10:00 - 12:00',
                time,
                date
            };
        } else if (hour >= 13 && hour < 15) {
            return {
                slotId: 'evening-1',
                slot: '13:00 - 15:00',
                time,
                date
            };
        } else if (hour >= 15 && hour <= 17) {
            return {
                slotId: 'evening-2',
                slot: '15:00 - 17:00',
                time,
                date
            };
        }
    }
    return {};
}

function getInstallationTimeSlot(dateTime) {
    if (dateTime && dateTime.split(' ')[0] && dateTime.split(' ')[1]) {
        let date = dateTime.split(' ')[0];
        let time = dateTime.split(' ')[1];
        let hour = time.split(':')[0];
        if (hour >= 8 && hour < 12) {
            return {
                slotId: 'Morning',
                slot: '8:00 - 12:00',
                time,
                date
            };
        } else if (hour >= 12 && hour <= 17) {
            return {
                slotId: 'Afternoon',
                slot: '12:00 - 17:00',
                time,
                date
            };
        }
    }
    return {};
}

async function getNewBooking(userId, bookingId) {
    let technician;
    try {
        technician = await getTechnicianFromBookingId(bookingId);
    } catch (err) {
        logger.error('requesting technician failed: ', err);
    }

    let bookingSlot;

    try {
        bookingSlot = await getBookingSlotFromBookingId(bookingId);
        if (bookingSlot.err) {
            bookingSlot.data = {};
        }
    } catch (err) {
        logger.error('requesting booking metadata failed: ', err);
        bookingSlot.data = {};
    }

    const now = new Date().toISOString();
    //as: https://pccwsolutions.atlassian.net/browse/MYIN-2605, installations with MYIRX
    const transactionId = INSTALLATION_PREFIX + new Date().getTime();

    const scheduledTime = moment(bookingSlot.data['dateTime'], 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');

    return {
        transactionId,
        orderNumber: null,
        userId,
        bookingId,
        bookedTime: now,
        updatedTime: now,
        technician: technician,
        status: BOOKED.status,
        closed: false,
        currentStatus: BOOKED,
        statusUpdates: [],
        timeSlot: bookingSlot.data['timeSlot'] || {},
        scheduledTime
    };
}

async function getNewBookingConfirmationRequest(bookingData, user) {

    let profile = await userProfileService.getUserProfile(user).catch(e => logger.error('error', e)) || {};
    let account = await userProfileService.getInactiveAccount(user).catch(e => logger.error('error', e)) || {};
    let location = await userProfileService.getDefaultLocation(user).catch(e => logger.error('error', e)) || {};

    // fixme: user this after testing
    // fixme: move the default values to ENV
    // const [profile, account, location] = await Promise
    //     .all([
    //         userProfileService.getUserProfile(user),
    //         userProfileService.getInactiveAccount(user),
    //         userProfileService.getDefaultLocation(user)
    //     ])
    //     .catch(e => logger.error('error', e));

    const primaryPhone = `${profile.primaryPhone || ''}`;
    const telpHpPrimary = primaryPhone.startsWith('0') ? primaryPhone.substring(1) : primaryPhone;

    let country = profile.country ? profile.country.toLowerCase().replace(/./, m => m.toUpperCase()) : '';
    let national = profile.nationality ? profile.nationality.toLowerCase().replace(/./, m => m.toUpperCase()) : '';

    return {
        guid: '24',
        code: '1',
        data: {
            external: {
                trackId: bookingData.transactionId,
                user_id: 'user_myindihome',
                source: 'MYINDIHOME',
                inbox: 'BE'
            },
            transaction: {
                //todo: get form produt info- package.SERVICE
                services: 'Test Service1',
                order_type: '1',
                transSystem: 'INDIHOME',
                regist108: 'YES',
                omzet: '0',
                ndref: '',
                battlezone: '',
                zone: '0',
                kcontact: `MI;${bookingData.transactionId};CUST;${profile.name || ''};${profile.primaryPhone || ''}`,
                //todo: get form produt info- package.SERVICE
                packageName: 'Bundling 3P 1',
                packageCodeInet: '',
                packageCodeVoice: '',
                sales_phone: '',
                cagent: '0',
                transType: '',
                transFlag: 'AO',
                helpdesk_caring: '',
                indihomeIndicator: 'Indihome',
                //todo: get from mapping for the STO
                witel: 'JAKARTA SELATAN',
                channel: 'MYINDIHOME',
                lokasiChannel: 'NATIONAL#0',
                tsi: ''
            },
            alpro: {
                'alproType': 'ODP',
                'technology': 'GPON',
                alproId: location.deviceId? location.deviceId.toString() : '',
                refId: '',
                alproName: location.locId,
                alproLat: `${location.latitude}` || '',
                alproLng: `${location.longitude}` || '',
                STO: location.sto,
                areaCode: location.system
            },
            // todo:  some in can get from user profile
            customer: {
                exist: '1',
                'statusCus': 'NEW',
                'custType': '1',
                ncli: account.ncli? account.ncli.toString(): '',
                nd: account.indiHomeNum,
                data: {
                    'title': '.',
                    name: profile.name || '',
                    telp: profile.primaryPhone || '',
                    email: profile.email || '',
                    birthDate: profile.dob || '20/11/1986',
                    birthPlace: profile.birthPlace || 'JAKARTA',
                    idenType: 'KTP',
                    idenNo: profile.idNum || '3203072011860001',
                    expiredDate: profile.idExpDate || '20/11/2024',
                    'npwp': '0',
                    motherName: 'Mother Name',
                    country,
                    national,
                    province: location.province || '',
                    cityName: location.city || '',
                    cityCode: location.cityCode || '',
                    districtName: location.district || '',
                    districtCode: location.districtCode || '',
                    streetName: location.street || '',
                    streetCode: location.streetCode || '',
                    number: '1',
                    postCode: (location.postalCode || '').trim(),
                    addressDesc: location.addressDesc || '',
                    block: location.block || '',
                    floor: location.floor || '',
                    room: location.room || '',
                    marketInfo: '',
                    category: 'Residensial',
                    socio: 'Lain-lain',
                    'provider': 'DCS - UCS II',
                    validNumber: telpHpPrimary,
                    telpHpPrimary: telpHpPrimary,
                    'codeAreaTelpHpPrimary': '+622',
                    'statusTelpPrimary': 'VERIFIED',
                    'telpHpAlt1': '',
                    'statusEmail': 'VALIDATE',
                    'codeAreaTelHppAlt1': '',
                    'ownerTelpHpAlt1': '',
                    'telpHpAlt2': '',
                    'codeAreaTelpHpAlt2': '',
                    'ownerTelpHpAlt2': '',
                    'telpRumah': '',
                    'codeAreaTelpRumah': '',
                    'idFacebook': '',
                    'idinstagram': '',
                    'idTwitter': '',
                    rt: location.rt || '',
                    rw: location.rw || '',
                    custType: 'Retail',
                    'religion': '',
                    'noKK': '',
                    gender: profile.gender || ''
                }
            },
            //  todo: get from subscription and user profile
            installation: {
                'title': '.',
                latitude: `${location.latitude}`,
                longitude: `${location.longitude}`,
                name: profile.name,
                contactNo: profile.primaryPhone,
                email: profile.email,
                'statusEmail': 'VALIDATE',
                cityName: location.city,
                cityCode: location.cityCode,
                districtName: location.district,
                districtCode: location.districtCode,
                streetName: location.street || '',
                streetCode: location.streetCode || '',
                number: '1',
                postCode: (location.postalCode || '').trim(),
                addressDesc: location.addressDesc || '',
                rw: location.rw || '',
                telpHpPrimary: telpHpPrimary,
                codeAreaTelpHpPrimary: '+62',
                'statusTelpPrimary': 'VERIFIED',
                'telpHpAlt1': '',
                'codeAreaTelHppAlt1': '',
                'ownerTelpHpAlt1': '',
                'telpHpAlt2': '',
                'codeAreaTelpHpAlt2': '',
                'ownerTelpHpAlt2': '',
                country,
                national
            },
            //  todo: get from subscription and user profile
            billing: {
                'title': '.',
                name: profile.name,
                cityName: location.city,
                cityCode: location.cityCode,
                districtName: location.district,
                districtCode: location.districtCode,
                streetName: location.street || '',
                streetCode: location.streetCode || '',
                number: '1',
                postCode: (location.postalCode || '').trim(),
                addressDesc: location.addressDesc || '',
                rt: location.rt || '',
                rw: location.rw || '',
                email: account.email,
                statusEmail: 'VALIDATE',
                telpHpPrimary: telpHpPrimary,
                codeAreaTelpHpPrimary: '+62',
                statusTelpPrimary: 'VERIFIED',
                'telpHpAlt1': '',
                'codeAreaTelHppAlt1': '',
                'ownerTelpHpAlt1': '',
                'telpHpAlt2': '',
                'codeAreaTelpHpAlt2': '',
                'ownerTelpHpAlt2': '',
                'npwp': '0',
                latitude: location.latitude,
                longitude: location.longitude,
                country,
                national
            },
            reservation: {
                nd: account.indiHomeNum,
                nd_inet: account.internetNum,
                deviceId: location.deviceId? location.deviceId.toString(): '',
                telp: account.fixedPhoneNum,
                custId: account.ncli? account.ncli.toString(): '',
                reserveNumber: account.reservationId? account.reservationId.toString(): '',
                reservePort: account.reservationPort? account.reservationPort.toString(): '',
                reserveTn: account.transaction? account.transaction.toString(): '',
                reserveInet: account.internetNum? account.internetNum.toString(): ''
            },
            shiping: {
                shipingModem: 'BY TECHNICIAN',
                installationType: 'BY TECHNICIAN',
                nameShiping: '',
                modelShiping: '',
                serialNumber: ''
            },
            appointment: {
                startDate: bookingData.timeSlot.date,
                startTime: bookingData.timeSlot.time,
                type: 'FACE_TO_FACE',
                desc: 'PASANG BARU INDIHOME',
                reminder: 'SMS',
                daysReminder: '1',
                location: `latitude:${location.latitude}; longitude:${location.longitude}`,
                technology: 'GPON',
                stoCode: location.sto,
                date: ` ${bookingData.timeSlot.date} ${bookingData.timeSlot.time}`,
                'siteId': 'REG-2',//todo
                woId: '',
                bookId: bookingData.bookingId,
                descApt: `${bookingData.timeSlot.date} ${bookingData.timeSlot.time}|${location.addressDesc}`
            },
            //todo: comes from the subscription, after the service is completed
            package: {
                package_code: {
                    //todo: package.PACKAGE_ID
                    voice: '1',
                    internet: '1'
                },
                //todo: package.FLAG
                package_name: 'Bundling 3P 1',
                //todo: package.PACKAGE_ID
                offer: '1',
                offer_detail: '',
                packageId: '1'
            },
            'deposit': {
                'invoiceDate': '',
                'invoiceId': '',
                'paymentStatus': '1',
                'paymentAmount': '',
                'paymentTimeout': '',
                'paymentCode': '',
                'invoiceStatus': '2',
                'paymentDate': ''
            },
            'prepayment': {
                'invoiceDate': '',
                'invoiceId': '',
                'paymentStatus': '1',
                'paymentAmount': '',
                'paymentTimeout': '',
                'paymentCode': '',
                'invoiceStatus': '2',
                'paymentDate': ''
            }
        }
    };
}

async function confirmBooking(confirmation) {
    let token = await authenticationService.getJWTFForUser();
    const authorization = 'Bearer ' + token;

    const options = {
        method: 'POST',
        uri: finalizeInstallationHost,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorization,
        },
        json: confirmation,
        strictSSL: false
    };

    try {
        logger.info(`sending data to booking confirmation API on: ${new Date()} for ${JSON.stringify(options)}`);
        const result = await rp.post(options);
        logger.info(`got response from booking confirmation API on: ${new Date()} as : ${JSON.stringify(result)}`);

        return {
            success: result['statusCode'] === '0',
            response: result
        };
    } catch (e) {
        logger.error('error in booking finalization API', e);
        return {
            success: false,
            response: e
        };
    }
}

async function getNewServiceBooking(userId, bookingId) {
    let technician;
    try {
        technician = await getServiceTechnicianFromBookingId(bookingId);
    } catch (err) {
        logger.error('requesting technician failed: ', err);
    }

    let bookingSlot;

    try {
        bookingSlot = await getServiceBookingSlotFromBookingId(bookingId);
        if (bookingSlot.err) {
            bookingSlot.data = {};
        }
    } catch (err) {
        logger.error('requesting booking metadata failed: ', err);
        bookingSlot.data = {};
    }

    const now = new Date().toISOString();
    return {
        userId,
        bookingId,
        bookedTime: now,
        updatedTime: now,
        technician: technician,
        status: SUBMITTED.status,
        closed: false,
        currentStatus: SUBMITTED,
        statusUpdates: [],
        timeSlot: bookingSlot.data['timeSlot'] || {}
    };
}

module.exports.reserveInstallationAppointment = async (user, bookingId) => {

    let bookingsWithSameBookingId = await technicianHandler.findBookingByBookingId(bookingId);
    if (bookingsWithSameBookingId.data && bookingsWithSameBookingId.data.length > 0) {
        logger.info(`booking id: ${bookingId}  already assigned for some user`);
        throw new ForbiddenError('booking id already assigned for some user');
    }

    let existingBookings = await technicianHandler.findBookingByUserId(user.userId, false);
    if (existingBookings.data) {
        if (existingBookings.data.filter(b => !b['closed']).length > 0) {
            throw new ForbiddenError(`user already has open installations with booking id
            : ${existingBookings.data.filter(b => !b['closed']).map(b => b['bookingId'])}`);
        }
    }
    let token = await authenticationService.getJWTFForUser();
    const authorization = `Bearer ${token}`;

    const reqBody = {
        'bookingID': bookingId,
        'status': TECHNICIAN.APPOINTMENT.STATUS.BOOKED
    };

    const options = {
        method: 'POST',
        uri: reserveAssignmentScheduleHost,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorization,
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info('sending booking data to the API on: ', new Date());
    const result = await rp.post(options);
    logger.info('got booking response data on: ', new Date());

    if (result['statusCode'] === '0') {
        // to update the status by client informing the technician status change
        //  save these records in a db with key:user_id and booking_id to provide the status and later
        let booking = await getNewBooking(user.userId, bookingId);
        let bookingUpdatedStatus = await technicianHandler.reserveBooking(booking);
        if (bookingUpdatedStatus.err) {
            logger.error('Error in reserve Booking.', bookingUpdatedStatus.err);
            throw new ForbiddenError('Error in reserve Booking');
        }

        //todo: check the flow and get the required values form the other services or move this call to another service
        let bookingConfirmation = await getNewBookingConfirmationRequest(booking, user);

        const [confirmedStatus] = await Promise.all([
            confirmBooking(bookingConfirmation),
            technicianHandler.confirmBooking(bookingConfirmation)
        ]).catch(e => logger.error(`error in booking confirmation: ${bookingConfirmation.data.external.trackId}`, e));

        let activatedStatus = false;
        if (!confirmedStatus.success) {
            logger.error(`could not confirm the booking with the Core System: ${JSON.stringify(confirmedStatus)}`);

            logger.info('closing the installation in system');
            let closed = await technicianHandler.closeBooking(bookingId, user.userId, true);
            logger.info(`closed the installation in system: ${closed}`);

            throw new ConflictError(`core api for order confirmation failed: ${JSON.stringify(confirmedStatus.response)}`);
        }
        return {
            status: 'booking confirmed',
            bookingId,
            transactionId: booking['transactionId'],
            confirmedStatus,
            activatedStatus
        };
    }
    logger.debug('booking id already being used');
    throw new ForbiddenError('booking id already being used');

};

/**
 * Save booking in DB
 * @param {String} userId - user Id
 * @param {String} bookingId - booking Id
 * @returns {Promise<void>}
 */
module.exports.generateBookingData = async (userId, bookingId) => {
    let booking = await getNewServiceBooking(userId, bookingId);

    return {
        status: 'success',
        data: booking
    };
};

module.exports.reserveServiceAppointment = async (userId, bookingId) => {


    let bookingsWithSameBookingId;
    try {
        bookingsWithSameBookingId = await technicianHandler.findServiceBookingByBookingId(bookingId);
    } catch (e) {
        logger.error('error', e);
    }
    if (bookingsWithSameBookingId && bookingsWithSameBookingId.data && bookingsWithSameBookingId.data.length > 0) {
        throw new ForbiddenError('booking id already assigned');
    }

    let existingBookings;
    try {

        existingBookings = await technicianHandler.findServiceBookingByUserId(userId);
    } catch (e) {
        logger.error('error', e);
    }
    if (existingBookings && existingBookings.data) {
        if (existingBookings.data.filter(b => !b['closed']).length > 0) {
            throw new ForbiddenError('user already has open installations with booking ' +
                `id : ${existingBookings.data.filter(b => !b['closed']).map(b => b['bookingId'])}`);
        }
    }

    //fixme: call the respective API for service booking
    let token = await authenticationService.getJWTFForUser();
    const authorization = 'Bearer ' + token;

    const reqBody = {
        'bookingID': bookingId,
        'status': TECHNICIAN.APPOINTMENT.STATUS.BOOKED
    };

    const options = {
        method: 'POST',
        uri: reserveAssignmentScheduleHost,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorization,
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info('requesting data from the API on: ', new Date());
    const result = await rp.post(options);
    logger.info('got data from the API on: ', new Date());

    if (result['statusCode'] === '0') {
        // to update the status by client informing the technician status change
        //  save these records in a db with key:user_id and booking_id to provide the status and later
        let booking = await getNewServiceBooking(userId, bookingId);
        let bookingUpdatedStatus = await technicianHandler.reserveServiceBooking(booking);
        if (bookingUpdatedStatus.err) {
            logger.error('Error in reserve service booking.', bookingUpdatedStatus.err);
            throw new ForbiddenError('Error in reserve service booking');
        }
        return {
            status: 'booking confirmed',
            bookingId
        };
    }
    logger.error('Error in reserve assignment schedule.', result);
    throw new ForbiddenError('Error in reserve assignment schedule.');

};

async function findAppointmentInformationByTransactionId(transactionId) {
    let booking = await technicianHandler.findBookingByTransactionId(transactionId);
    if (booking.data && booking.data.length === 1) {
        return wrapper.data(booking.data[0]);
    }
    logger.debug('multiple bookings for same transaction ID');
    throw new ConflictError('multiple bookings for same transaction ID');
}

async function findServiceAppointmentInformation(bookingId, userId) {
    if (userId) {
        return technicianHandler.findServiceBookingExact(bookingId, userId);
    }
    let booking = await technicianHandler.findServiceBookingByBookingId(bookingId);
    if (booking.data && booking.data.filter(b => !b['closed']).length === 1) {
        return wrapper.data(booking.data[0]);
    }
    logger.debug('multiple bookings for same booking ID');
    throw new ConflictError('multiple bookings for same booking ID');
}

async function updateBookingStatusInDB(transactionId, status, technician) {
    return technicianHandler.updateInstallationBookingStatus(transactionId, status, technician);
}

async function updateServiceBookingStatusInDB(bookingId, userId, status) {
    return technicianHandler.updateServiceBookingStatus(bookingId, userId, status);
}

module.exports.updateAppointmentStatus = async (transactionId, status) => {

    if (!status || [ON_THE_WAY.status, IN_PROGRESS.status, COMPLETED.status].indexOf(status) < 0) {
        throw new BadRequestError('invalid status');
    }

    let technician = {};
    if (status === ON_THE_WAY.status || IN_PROGRESS.status) {
        logger.info('updating technician in system');
        const workOrder = await getWoInformationFromAPI(transactionId).catch(reason => {
            logger.error('error in getting work order info', reason);
            throw new ConflictError('error in getting work order info from API');
        });

        if (
            workOrder &&
            workOrder.data &&
            workOrder.data['workorder'] &&
            workOrder.data['workorder'][0] &&
            workOrder.data['workorder'][0]['woActivity'] &&
            workOrder.data['workorder'][0]['woActivity'][0] &&
            workOrder.data['workorder'][0]['woActivity'][0] &&
            workOrder.data['workorder'][0]['woActivity'][0]['assignment'] &&
            workOrder.data['workorder'][0]['woActivity'][0]['assignment'][0] &&
            workOrder.data['workorder'][0]['woActivity'][0]['assignment'][0].amCrew
        ) {
            const crewId = workOrder.data['workorder'][0]['woActivity'][0]['assignment'][0].amCrew;
            technician = await getTechniciansByCrewId(crewId);
        }
    }
    let updated = await updateBookingStatusInDB(transactionId, status, technician);
    if (!updated.err) {
        let appointment = await findAppointmentInformationByTransactionId(transactionId);
        delete appointment.data.statusUpdates;
        // todo: manual change status because DB update delay
        appointment.data.status = status;
        const notification = {
            notifications: [
                {
                    userId: appointment.data['userId'],
                    header: `Technician Status changed to : ${status}`,
                    isChecked: false,
                    referenceId: appointment.data['bookingId'],
                    notificationType: NOTIFICATION_TYPES.ACTIVITY,
                    category: NOTIFICATION_CATEGORIES.INSTALLATION_FULFILLMENT,
                    imageUrl: '',
                    body: {
                        topic: '',
                        description: '',
                        data: {
                            appointment: appointment.data,
                        }
                    }
                }
            ]
        };
        notificationService.sendUserNotification(notification, appointment.data['userId'])
            .catch(e => logger.error('send notification failed', e));

        return {
            userId: appointment.data['userId'],
            bookingId: appointment.data['bookingId'],
            transactionId,
            status: status
        };
    }
    throw new ForbiddenError('updated the appointment status failed');

};


module.exports.updateBookingsOrderNumber = async (transactionId, orderNumber) => {

    if (_.isEmpty(transactionId) || _.isEmpty(orderNumber)) {
        logger.error('bookingId or dateTime is empty');
        throw new BadRequestError('bookingId or dateTime cannot be empty');
    }
    let appointment = await technicianHandler.updateBookingsOrderNumber(transactionId, orderNumber);
    if (appointment.data) {
        return {
            userId: appointment.data['userId'],
            bookingId: appointment.data['bookingId'],
            transactionId: appointment.data['transactionId'],
            orderNumber: appointment.data['orderNumber']
        };
    }

    throw new Error(appointment.err);
};

/**
 * @deprecated
 * @param bookingId
 * @param userId
 * @param status
 * @returns {Promise<{userId: *, bookingId: *, status: *}>}
 */
module.exports.updateServiceAppointmentStatus = async (bookingId, userId, status) => {

    if (!status || [SERVICE_SUBMITTED.status, SERVICE_IN_PROGRESS.status, SERVICE_COMPLETED.status,
        SERVICE_RESOLVED.status].indexOf(status) < 0) {
        throw new BadRequestError('invalid status');
    }
    let updated = await updateServiceBookingStatusInDB(bookingId, userId, status);
    if (!updated.err) {
        let appointment = await findServiceAppointmentInformation(bookingId, userId);
        // todo: not used, if used change to new notification format
        const notification = {
            notifications: [
                {
                    'userId': appointment.data['userId'],
                    'header': 'Service Appointment Technician Status changed to :' + appointment.data['status'],
                    'isChecked': false,
                    'referenceId': appointment.data['bookingId'],
                    'messageType': 'notification',
                    'imageUrl': ''
                }
            ]
        };
        notificationService.sendUserNotification(notification, appointment.data['userId']);

        return {
            userId: appointment.data['userId'],
            bookingId,
            status: status
        };
    }
    throw new ForbiddenError('updated the appointment status failed');

};

module.exports.closeBooking = async (bookingId, userId, closed) => {
    technicianHandler.upsertRescheduleAttempts({
        userId,
        attempts: maxRescheduleAttempts
    }).then(() => {
        logger.info(`attempts reset user: ${userId}, booking: ${bookingId}`);
    }).catch(error => logger.error('reset max attempts failed', error));

    let closedStatus = await technicianHandler.closeBooking(bookingId, userId, closed);

    if (closedStatus.data) {
        return {
            bookingId,
            userId,
            result: closedStatus.data['result']
        };
    }
    throw new ForbiddenError('booking close failed');

};

module.exports.getAppointmentStatus = async (userId, bookingId) => {
    let bookings = await technicianHandler.getAppointmentStatus(userId);

    if (bookings.data) {
        logger.info(`found appointments for the user ${userId} : ${JSON.stringify(bookings.data)}`);
        return {
            userId,
            appointments: bookings.data,
            technician: bookings.data.technician
        };
    }
    return {
        userId,
        appointments: [],
        technician: {}
    };
};

/**
 * @deprecated
 * @param userId
 * @param bookingId
 * @returns {Promise<*>}>}
 */
module.exports.getServiceAppointmentStatus = async (userId, bookingId) => {
    let bookings = await technicianHandler.getServiceAppointmentStatus(userId, bookingId);

    if (bookings.data) {
        return {
            userId,
            appointments: bookings.data,
            technician: bookings.data.technician
        };
    }
    return {
        userId,
        appointments: [],
        technician: {}
    };
};

async function findInstallationSlot(bookingId) {
    return technicianHandler.findSlotByBookingId(bookingId);
}

async function findServiceSlot(bookingId) {
    return technicianHandler.findServiceSlotByBookingId(bookingId);
}

async function getCrewInformationFromAPI(crewId) {
    let token = await authenticationService.getJWTFForUser();
    const authorization = 'Bearer ' + token;

    const reqBody = {
        'amCrew': crewId
    };

    const options = {
        method: 'POST',
        uri: crewInformationEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorization,
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info('requesting data from the crew Infromation API on: ', new Date());
    return rp.post(options);
}

async function getAssuranceCrewInformationFromAPI(crewId) {
    let token = await authenticationService.getJWTFForUser();
    const authorization = 'Bearer ' + token;

    const reqBody = {
        'crewId': crewId
    };

    const options = {
        method: 'POST',
        uri: assuranceCrewInformationEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorization,
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info('requesting data from the crew Infromation API on: ', new Date());
    return rp.post(options);
}

async function getTechniciansByCrewId(crewId) {

    let crew = await getCrewInformationFromAPI(crewId);
    if(crew.data.AMCREWLABOR && crew.data.AMCREWLABOR.length > 0){
        let labor = crew.data.AMCREWLABOR[0];
        if(labor.LABOR && labor.LABOR.length > 0)
        {
            let person = labor.LABOR[0].PERSON[0];
            if(person)
            {
                return {
                    personId: person.PERSONID,
                    displayname: person.DISPLAYNAME,
                    email: person.PRIMARYEMAIL || '',
                    phone: person.PRIMARYPHONE || ''
                };
            }
            logger.debug('Invalid Person Object');
            throw new ConflictError('Invalid Person Object');
        }
        logger.debug('Invalid Labor Object');
        throw new ConflictError('Invalid Labor Object');

    }
    else{
        logger.error('Error in crew data.', crew);
        throw new ConflictError(crew.message);
    }
}

async function getAssuranceTechniciansByCrewId(crewId) {

    let crew = await getAssuranceCrewInformationFromAPI(crewId);
    if(crew.data.AMCREWLABOR && crew.data.AMCREWLABOR.length > 0){
        let labor = crew.data.AMCREWLABOR[0];
        if(labor.LABOR && labor.LABOR.length > 0)
        {
            let person = labor.LABOR[0].PERSON[0];
            if(person)
            {
                return {
                    personId: person.PERSONID,
                    displayname: person.DISPLAYNAME,
                    email: person.PRIMARYEMAIL || '',
                    phone: person.PRIMARYPHONE || ''
                };
            }
            logger.debug('Invalid Person Object');
            throw new ConflictError('Invalid Person Object');
        }
        logger.debug('Invalid Labor Object');
        throw new ConflictError('Invalid Labor Object');

    }
    else{
        logger.error('Error in crew data.', crew);
        throw new ConflictError(crew.message);
    }
}

async function getTechnicianFromBookingId(bookingId){
    let bookingSlot = await findInstallationSlot(bookingId);
    if (bookingSlot && bookingSlot.data && bookingSlot.data.crewId) {
        return await getTechniciansByCrewId(bookingSlot.data.crewId);
    }
    throw new ForbiddenError('booking slot not found');
}
async function getServiceTechnicianFromBookingId(bookingId){
    let bookingSlot = await findServiceSlot(bookingId);
    if (bookingSlot && bookingSlot.data && bookingSlot.data.crewId) {
        return await getAssuranceTechniciansByCrewId(bookingSlot.data.crewId);
    }
    logger.debug('booking slot not found');
    throw new ForbiddenError('booking slot not found');
}

async function getBookingSlotFromBookingId(bookingId) {
    let bookingSlot = await findInstallationSlot(bookingId);
    if (bookingSlot) {
        return bookingSlot;
    }
    logger.debug('booking slot not found');
    return new ForbiddenError('booking slot not found');
}

async function getServiceBookingSlotFromBookingId(bookingId) {
    let bookingSlot = await findServiceSlot(bookingId);
    if (bookingSlot) {
        return bookingSlot;
    }
    logger.debug('booking slot not found');
    return new ForbiddenError('booking slot not found');
}

module.exports.getTechnicianInformation = async (bookingId) => {
    return getTechnicianFromBookingId(bookingId);
};

module.exports.getServiceTechnicianInformation = async (bookingId) => {
    return getServiceTechnicianFromBookingId(bookingId);
};

/**
 * Send early installation appointment notification to user
 * @param transactionId
 * @param dateTime
 */
module.exports.sendEarlyInstallationRequest = async (transactionId, dateTime) => {
    //get userId from transactionId
    if (_.isEmpty(transactionId) || _.isEmpty(dateTime)) {
        logger.error('transactionId or dateTime is empty');
        throw new BadRequestError('transactionId or dateTime cannot be empty');
    }

    let existingBooking = await technicianHandler.findBookingByTransactionId(transactionId);
    if (_.isEmpty(existingBooking.data)) {
        logger.debug('Couldn\'t find matching booking slot for transactionId');
        throw new PreconditionFailedError(`Couldn't find matching booking slot for transactionId: ${transactionId}`);
    }
    if(existingBooking.data.length !== 1) {
        logger.debug('Invalid number of bookings found for the user');
        throw new PreconditionFailedError('Invalid number of bookings found for the user');
    }

    existingBooking = existingBooking.data[0];

    //validate dateTime on the same date early time
    const isValid = await validateNewBookingTime(dateTime, existingBooking);
    if (!isValid) {
        logger.error('Invalid date time');
        throw new BadRequestError('Invalid date time.');
    }

    //to send notification to user
    const userId = existingBooking.userId;
    const notification = {
        notifications: [
            {
                userId: userId,
                header: 'Earlier Schedule Available',
                isChecked: false,
                referenceId: transactionId,
                notificationType: NOTIFICATION_TYPES.ACTIVITY,
                category: NOTIFICATION_CATEGORIES.APPOINTMENT_EARLY_RESCHEDULE,
                imageUrl: '',
                body: {
                    topic: `Order ID ${transactionId}`,
                    description: 'Looks like your technician can come earlier! Are you available?',
                    data: {
                        previousBooking: existingBooking,
                        newSchedule: dateTime
                    }
                }
            }
        ]
    };
    notificationService.sendUserNotification(notification, userId);

    return {
        userId: userId,
        transactionId,
        message: 'Early Installation notification sent.'
    };
};

/**
 * check if use can reschedule or not
 * @param userId
 * @param language
 * @returns {Promise<{orderNumber: null, existingBooking: *, attemptsRemaining, rescheduleAvailable: boolean}>}
 */
module.exports.getRescheduleFeasibility = async (userId, language) => {
    let rescheduleAvailable = true;
    let attemptsResponse = await technicianHandler.getRescheduleAttempts(userId);
    if (attemptsResponse.err) {
        attemptsResponse.data = {attempts: maxRescheduleAttempts};
    }

    let bookings = await technicianHandler.findBookingByUserId(userId, false);

    let oldBooking = bookings.data[0];
    let reason = OK;
    if (attemptsResponse.data.attempts <= 0) {
        rescheduleAvailable = false;
        reason = RESCHEDULE_ATTEMPTS_EXCEEDED;
    } else if (!oldBooking) {
        rescheduleAvailable = false;
        reason = NO_OPEN_BOOKINGS;
    } else if (!oldBooking.orderNumber) {
        rescheduleAvailable = false;
        reason = WORK_ORDER_NOT_UPDATED;
    }

    return {
        rescheduleAvailable,
        ...reason,
        attemptsRemaining: attemptsResponse.data.attempts,
        orderNumber: oldBooking ? oldBooking.orderNumber : '',
        existingBooking: oldBooking
    };

};

/**
 * Reschedule early installation booking
 * @param {Object} payload - reschedule data
 * @param {String} userId - user Id
 * @returns {Promise<{bookingId: *, status: string}>}
 */
module.exports.rescheduleEarlyInstallationBooking = async (payload, userId) => {
    const token = await authenticationService.getJWTFForUser();
    const authorization = 'Bearer ' + token;
    const {bookingId, schedule} = payload;

    if (_.isEmpty(bookingId) || _.isEmpty(schedule)) {
        logger.error('bookingId or schedule is empty');
        throw new BadRequestError('bookingId or schedule is empty');
    }

    let bookings = await technicianHandler.findBookingByUserId(userId, false);
    if (bookings.data.length !== 1) {
        logger.debug('Invalid number of bookings found for the user');
        throw new PreconditionFailedError('Invalid number of bookings found for the user');
    }

    let oldBooking = bookings.data[0];
    if (!oldBooking.orderNumber) {
        logger.debug(`Existing booking oder number has not updated: current booking : ${oldBooking}`);
        throw new PreconditionFailedError(`Existing booking oder number has not updated: current booking : ${oldBooking}`);
    }

    //validate dateTime on the same date early time
    const isValid = await validateNewBookingTime(schedule, oldBooking);
    if (!isValid) {
        logger.error('Invalid date time');
        throw new BadRequestError('Invalid schedule for date time.');
    }

    const reqBody = await getWOData(bookingId, oldBooking.transactionId, schedule);
    const options = {
        method: 'POST',
        uri: rescheduleInstallationEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorization,
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`Technician Service: Date: ${new Date()}| sending request to reschedule early installation. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    logger.info(`Technician Service: Date: ${new Date()}| received response from early rescheduled installation. Res: ${JSON.stringify(response)}`);

    if (response.statusCode === '0') {
        // get new timeSlot by new schedule
        const newTime = moment(schedule, ISO_DATE_FORMAT).format(TECHNICIAN_APPOINTMENT_DATE_FORMAT);
        const newSlot = getInstallationTimeSlot(newTime);

        // update the same booking with new time on DB
        const updated = await technicianHandler.rescheduleBooking(bookingId, userId, TECHNICIAN.APPOINTMENT.STATUS.BOOKED, newSlot,schedule);
        if (updated.err) {
            logger.info(`Technician Service: Date: ${new Date()}| Reschedule early installation booking update failed. Error:
            ${JSON.stringify(updated.err)}`);
            throw new ForbiddenError('Reschedule early installation booking update failed');
        }

        return {
            bookingId,
            userId: userId,
            message: 'Booking rescheduled',
            schedule: schedule
        };
    }

    throw new BadRequestError('Error in rescheduling request');
};

/**
 * Reschedule installation booking
 * @param {String} bookingId - booking Id
 * @param {String} userId - user Id
 * @returns {Promise<{bookingId: *, status: string}>}
 */
module.exports.rescheduleInstallationBooking = async (bookingId, userId) => {
    const token = await authenticationService.getJWTFForUser();
    const authorization = 'Bearer ' + token;

    if (_.isEmpty(bookingId) || _.isEmpty(userId)) {
        logger.error('bookingId or userId is empty');
        throw new BadRequestError('bookingId or userId is empty');
    }

    // check remaining reschedule attempts
    let attempts = await technicianHandler.getRescheduleAttempts(userId);
    //fixme: fix this after get attempts fixed or add default value
    if (attempts.err) {
        attempts.data = {attempts: maxRescheduleAttempts};
    }
    if (attempts.data.attempts <= 0) {
        logger.debug('Max reschedule attempts reached.');
        throw new BadRequestError('Max reschedule attempts reached.');
    }

    //get the previous bookings for the user
    let bookings = await technicianHandler.findBookingByUserId(userId, false);
    if (bookings.data.length !== 1) {
        logger.debug('Invalid number of bookings found for the user');
        throw new PreconditionFailedError('Invalid number of bookings found for the user');
    }

    let oldBooking = bookings.data[0];
    if (!oldBooking.orderNumber) {
        logger.debug(`Existing booking oder number has not updated: current booking : ${JSON.stringify(oldBooking)}`);
        throw new PreconditionFailedError(`Existing booking oder number has not updated: current booking : ${JSON.stringify(oldBooking)}`);
    }

    const reqBody = await getWOData(bookingId, oldBooking.transactionId);
    const options = {
        method: 'POST',
        uri: rescheduleInstallationEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorization,
        },
        json: reqBody,
        strictSSL: false
    };

    logger.info(`Technician Service: Date: ${new Date()}| sending request to reschedule installation. Req: ${JSON.stringify(options)}`);
    const response = await rp.post(options);
    logger.info(`Technician Service: Date: ${new Date()}| received response data from rescheduled installation. Res: ${JSON.stringify(response)}`);

    if (response.statusCode === '0') {

        //execute elastic search indexing
        let booking = await getNewBooking(userId, bookingId);
        booking.transactionId = oldBooking.transactionId;
        booking.orderNumber = oldBooking.orderNumber;
        booking.statusUpdates = oldBooking.statusUpdates;
        booking.statusUpdates.push({
            status: booking.status,
            action: TECHNICIAN.APPOINTMENT.STATUS.RESCHEDULED,
            updated: booking.updatedTime
        });
        logger.info(`Booking data update: ${JSON.stringify(booking)}`);
        //update the same booking with new BookingId data
        let updateBooking = await technicianHandler.updateBooking(booking);
        if (updateBooking.err) {
            logger.info(`Technician Service: Date: ${new Date()}| Reschedule installation booking update failed. Error:
            ${JSON.stringify(updateBooking.err)}`);
            throw new ForbiddenError('Reschedule installation booking update failed');
        }

        //update user installation attempts
        const attemptObj = {
            userId: userId,
            attempts: maxRescheduleAttempts
        };
        if (_.isEmpty(attempts.data)) {
            //insert new one by deducting max attempt
            attemptObj.attempts -= 1;
        } else {
            //update by deducting current attempt
            attemptObj.attempts = attempts.data.attempts - 1;
        }

        let upsertRescheduleAttempts = await technicianHandler.upsertRescheduleAttempts(attemptObj);
        if (upsertRescheduleAttempts.err) {
            logger.info(`Technician Service: Date: ${new Date()}| Update reschedule attempts failed. Error: 
            ${JSON.stringify(upsertRescheduleAttempts.err)}`);
            throw new ConflictError('Update reschedule attempts failed');
        }

        return {
            status: 'booking rescheduled',
            bookingId
        };
    }
    logger.error(`Error in reschedule booking. Response: ${JSON.stringify(response)}`);
    throw new BadRequestError(`Error in reschedule booking. Response: ${JSON.stringify(response)}`);
};


/**
 * Validate booking Time
 * @param {Date} dateTime - date Time
 * @param {Object} currentBooking - current booking data
 * @param {Boolean} validateTime - validateTime boolean, True: validate both date & time, False: validate only date
 */
async function validateNewBookingTime(dateTime, currentBooking, validateTime = true) {
    const currentBookingDateTime = currentBooking.timeSlot.date.concat(' ').concat(currentBooking.timeSlot.time);
    const currentBookingDate = moment(currentBookingDateTime, TECHNICIAN_APPOINTMENT_DATE_FORMAT).format(ISO_DATETIME_FORMAT);
    const newBookingDate = moment(dateTime, ISO_DATE_FORMAT).format(ISO_DATETIME_FORMAT);

    const isSameDate = moment(newBookingDate).isSame(moment(currentBookingDate), 'day');
    if (!validateTime) {
        return  isSameDate;//check date wise
    }
    return isSameDate && moment(newBookingDate).isBefore(moment(currentBookingDate), 'minute'); //check hourly

}

/**
 * Get work order information
 * @param {String} orderNumber - order number assigned: scorderno
 * @returns {Promise<request.Request>}
 */
async function getWoInformationFromAPI(orderNumber){
    const token = await authenticationService.getJWTFForUser();
    const authorization = 'Bearer ' + token;

    const options = {
        method: 'POST',
        uri: technicianWorkorderInformationEndpoint,
        headers: {
            'Content-Type': 'application/json',
            Authorization: authorization,
        },
        json: {
            scorderno: orderNumber
        },
        strictSSL: false
    };

    logger.info(`Technician Service: Date: ${new Date()}| retrieving workorder information. Req: ${JSON.stringify(options)}`);
    return rp.post(options);
}

/**
 * Get WO data
 * @param {String} bookingId - Booking Id
 * @param {String} orderNumber - Order Number
 * @param {Date} schedule
 * @returns {Promise<{propschedStart: *, siteId: string, woNum: string}>}
 */
async function getWOData(bookingId, orderNumber, schedule = null) {
    let schedStart = null;

    if (_.isEmpty(schedule)) {
        //get booking data by Id
        const newBooking = await technicianHandler.findSlotByBookingId(bookingId);
        if (newBooking.err) {
            logger.debug(`Couldn't find booking data for bookingId: ${bookingId}`);
            throw new NotFoundError(`Couldn't find booking data for bookingId: ${bookingId}`);
        }
        schedStart = moment(newBooking.data.dateTime, TECHNICIAN_APPOINTMENT_DATE_FORMAT).format(ISO_DATETIME_WITH_SECS_FORMAT);
    } else {
        schedStart = moment(schedule, ISO_DATE_FORMAT).format(ISO_DATETIME_WITH_SECS_FORMAT);
    }

    let workorderInformation = await getWoInformationFromAPI(orderNumber);
    if (!(workorderInformation.data && workorderInformation.data.workorder && workorderInformation.data.workorder[0])) {
        logger.debug('Workorder information not found');
        throw new PreconditionFailedError('Workorder information not found');
    }

    return {
        propschedStart: schedStart,
        siteId: 'REG-2', //@todo get from telkom API
        woNum: workorderInformation.data.workorder[0].woNum
    };
}

/**
 * Get available bookings within the given time
 * @param {String} time - time
 * @returns {Promise<*>}
 */
module.exports.getBookingsForTime = async (time) => {
    return await technicianHandler.getBookings(time);
};

/**
 * Get available Early installation bookings slots
 * @deprecated
 * @param {String} bookingId - Booking Id
 * @returns {Promise<*>}
 */
module.exports.getEarlyInstallationTimes = async (bookingId) => {
    // find booking data
    let slot = await technicianHandler.findSlotByBookingId(bookingId);

    if (_.isEmpty(slot.data)) {
        logger.debug(`Not found such booking for bookingId: ${bookingId}`);
        throw new NotFoundError(`Not found such booking for bookingId: ${bookingId}`);
    }
    // find available slots in the same date
    const earlySlots = await technicianHandler.findEarlySlots(slot.data.timeSlot.date);
    if (_.isEmpty(earlySlots.data)) {
        logger.debug('Not found any available slots in the same date');
        throw new NotFoundError('Not found any available slots in the same date');
    }
    // filter early slots by original booking time
    const end = moment(slot.data.timeSlot.time, 'HH:mm');
    const possibleSlots = earlySlots.data.filter(s => {
        const start = moment(s.timeSlot.time, 'HH:mm');
        return start.isBefore(end);
    });

    if (_.isEmpty(possibleSlots)) {
        logger.debug('Not found any early slots in the same date');
        throw new NotFoundError('Not found any early slots in the same date');
    }
    return possibleSlots;
};
