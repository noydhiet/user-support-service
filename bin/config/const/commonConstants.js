/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 12/18/2019
 */

module.exports = {
    TECHNICIAN: {
        APPOINTMENT: {
            STATUS: {
                BOOKED: 'BOOKED',
                RESCHEDULED: 'RESCHEDULED',
                REOPENED: 'REOPENED'
            }
        }
    },
    ISO_DATE_FORMAT: 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]',
    TECHNICIAN_APPOINTMENT_DATE_FORMAT: 'DD-MM-YYYY HH:mm',
    ISO_DATETIME_FORMAT: 'YYYY-MM-DD[T]HH:mm',
    ISO_DATETIME_WITH_SECS_FORMAT: 'YYYY-MM-DD[T]HH:mm:ss',
    LANGUAGE: {
        EN: 'en',
        ID: 'id'
    },
    LANGUAGE_HEADER: 'Accept-Language',
    NOTIFICATION_TYPES: {
        ACTIVITY: 'activity',
        ACTION: 'action',
        NOTIFICATION: 'notification'
    },
    NOTIFICATION_CATEGORIES: {
        INSTALLATION_FULFILLMENT: 'INSTALLATION_FULFILLMENT',
        TICKET_ASSURANCE: 'TICKET_ASSURANCE',
        TICKET_ASSURANCE_REMINDER: 'TICKET_ASSURANCE_REMINDER',
        APPOINTMENT_EARLY_RESCHEDULE: 'APPOINTMENT_EARLY_RESCHEDULE'
    }
};
