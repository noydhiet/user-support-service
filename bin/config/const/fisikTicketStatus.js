/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 3/12/2019, 4:00 PM
 */

const submitted = {
    status: 'SUBMITTED',
    title: 'Ticket Submitted',
    text: 'Ticket has been submitted. Our team is currently precessing your request',
};

const inProgress = {
    status: 'IN_PROGRESS',
    title: 'In progress',
    text: 'Technician on the way to you'
};


const completed = {
    status: 'COMPLETED',
    title: 'Repair Complete',
    text: 'Our technician has finished repairing the issue'
};

const resolved = {
    status: 'RESOLVED',
    title: 'Issue solved',
    text: 'Issue has been resolved'
};

module.exports.SUBMITTED = {
    ...submitted,
    steps: [
        {
            ...submitted,
            active: true
        },
        {
            ...inProgress,
            active: false
        }, {
            ...completed,
            active: false
        }, {
            ...resolved,
            active: false
        }
    ]
};

module.exports.IN_PROGRESS = {
    ...inProgress,
    steps: [
        {
            ...submitted,
            active: true
        },
        {
            ...inProgress,
            active: true
        }, {
            ...completed,
            active: false
        }, {
            ...resolved,
            active: false
        }
    ]
};

module.exports.COMPLETED = {
    ...completed,
    steps: [
        {
            ...submitted,
            active: true
        },
        {
            ...inProgress,
            active: true
        }, {
            ...completed,
            active: true
        }, {
            ...resolved,
            active: false
        }
    ]
};

module.exports.RESOLVED = {
    ...resolved,
    steps: [
        {
            ...submitted,
            active: true
        },
        {
            ...inProgress,
            active: true
        }, {
            ...completed,
            active: true
        }, {
            ...resolved,
            active: true
        }
    ]
};
