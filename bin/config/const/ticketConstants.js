/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 11/29/2019
 */
module.exports = {
    CATEGORIES: {
        EN: {
            INTERNET: {
                ID: 'cat_01_en',
                NAME: 'Internet',
                TYPE: 'INTERNET'
            },
            TV: {
                ID: 'cat_02_en',
                NAME: 'TV',
                TYPE: 'IPTV'
            },
            TELEPHONE: {
                ID: 'cat_03_en',
                NAME: 'Telephone',
                TYPE: 'VOICE'
            },
            ADMIN_AND_BILLING: {
                ID: 'cat_04_en',
                NAME: 'Administration',
                TYPE: 'INTERNET'
            },
            INSTALLATION: {
                ID: 'cat_05_en',
                NAME: 'Installation',
                TYPE: 'INTERNET'
            }
        },
        ID: {
            INTERNET: {
                ID: 'cat_01_id',
                NAME: 'Internet',
                TYPE: 'INTERNET'
            },
            TV: {
                ID: 'cat_02_id',
                NAME: 'TV',
                TYPE: 'IPTV'
            },
            TELEPHONE: {
                ID: 'cat_03_id',
                NAME: 'Telepon',
                TYPE: 'VOICE'
            },
            ADMIN_AND_BILLING: {
                ID: 'cat_04_id',
                NAME: 'Administrasi',
                TYPE: 'INTERNET'
            },
            INSTALLATION: {
                ID: 'cat_05_id',
                NAME: 'Instalasi',
                TYPE: 'INTERNET'
            }
        }

    },
    CATEGORY_TYPES: {
        ADMIN: 'ADMIN', // Admin - Non Technical
        FISIK: 'FISIK', // Technical - technician
        LOGIC: 'LOGIC' // Technical
    },
    TICKET_TYPES: {
        TEKNIKAL: 'TEKNIKAL',
        ADMINISTRASI: 'ADMINISTRASI',
    },
    TICKET_ACTIONS: {
        REOPEN: 'REOPEN',
        RESCHEDULE: 'RESCHEDULE',
        CLOSED: 'CLOSED'
    },
    TICKET_REQ_TYPES: {
        ADMIN: 'ADMIN',
        FISIKLOGIC: 'FISIKLOGIC'
    },
    CABLE_TYPES: {
        FIBER: 'Fiber',
        COPPER: 'Copper'
    },
    INDONESIAN_LOCALE: 'id_id',
    TELKOM_REALM: 'telkom.net',
    IBOOSTER_RANGE: {
        MIN: -25,
        MAX: -13
    },
    ACS_RESET_CUTOFF: 60.00,
    PRECISION_TWO_DECIMALS: 2,
    FISIK_TICKET_MAX_RESCHEDULE_ATTEMPTS: 2

};
