/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 22/12/2019, 12:47 PM
 */

//  todo: remove
module.exports.ISSUES = {
    EN: [
        {
            'id': 1,
            'category': 'Internet',
            'issue': 'Telepon, Internet dan USeeTV tidak dapat berfungsi',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_001 //// A_INTERNET_001_001_002',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 2,
            'category': 'Internet',
            'issue': 'Tidak bisa terkoneksi internet',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_001 //// A_INTERNET_001_001_001',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 3,
            'category': 'Internet',
            'issue': 'Kecepatan internet sangat lambat',
            'fiberSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_002 //// A_INTERNET_001_002_003',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_005 //// A_INTERNET_001_005_001',
                'ownerGroup': '',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 4,
            'category': 'Internet',
            'issue': 'Koneksi internet tidak stabil (putus-putus)',
            'fiberSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_002 //// A_INTERNET_001_002_004',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_005 //// A_INTERNET_001_006_001',
                'ownerGroup': '',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 5,
            'category': 'Internet',
            'issue': 'ONT/ Modem tidak dapat berfungsi normal',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_003 //// A_INTERNET_001_003_003',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 6,
            'category': 'TV',
            'issue': 'Layanan UseeTV tidak dapat berfungsi',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_001 //// A_IPTV_001_001_002',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 7,
            'category': 'TV',
            'issue': 'Telepon, Internet dan USeeTV tidak dapat berfungsi',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_001 //// A_INTERNET_001_001_002',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 8,
            'category': 'TV',
            'issue': 'USeeTV normal namun layar TV tidak memunculkan gambar',
            'fiberSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_002 //// A_IPTV_001_002_014',
                'ownerGroup': 'DTVV SA',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 9,
            'category': 'TV',
            'issue': 'TV on demand tidak stabil (putus-putus)',
            'fiberSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_002 //// A_IPTV_001_002_010',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 10,
            'category': 'TV',
            'issue': 'Remote UseeTV tidak berfungsi',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_003 //// A_IPTV_001_003_002',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 11,
            'category': 'TV',
            'issue': 'Set-Top-Box (STB) tidak berfungsi',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_003 //// A_IPTV_001_003_001',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 12,
            'category': 'TV',
            'issue': 'Konten di UseeTV tidak tampil dilayar',
            'fiberSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_002 //// A_IPTV_001_002_016',
                'ownerGroup': 'DTVV SA',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 13,
            'category': 'TELEPHONE',
            'issue': 'Tidak dapat melakukan panggilan telepon',
            'fiberSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_001 //// A_VOICE_001_001_004',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_002 //// A_VOICE_001_002_004',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_003 //// A_VOICE_001_003_004',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'copperUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_004 //// A_VOICE_001_004_004',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            }
        },
        {
            'id': 14,
            'category': 'TELEPHONE',
            'issue': 'Telepon tidak dapat menerima panggilan',
            'fiberSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_001 //// A_VOICE_001_001_005',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_002 //// A_VOICE_001_002_005',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'copperSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_003 //// A_VOICE_001_003_005',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'copperUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_004 //// A_VOICE_001_004_005',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            }
        },
        {
            'id': 15,
            'category': 'TELEPHONE',
            'issue': 'Telepon tidak ada nada',
            'fiberSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_001 //// A_VOICE_001_001_009',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_002 //// A_VOICE_001_002_009',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_003 //// A_VOICE_001_003_009',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'copperUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_004 //// A_VOICE_001_004_009',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISK'
            }
        },
        {
            'id': 16,
            'category': 'TELEPHONE',
            'issue': 'Suara telepon tidak jernih/ kemerosok',
            'fiberSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_001 //// A_VOICE_001_001_012',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_002 //// A_VOICE_001_002_012',
                'ownerGroup': 'ROC-2',
                'type': 'FISIK'
            },
            'copperSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_003 //// A_VOICE_001_003_013',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'copperUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_004 //// A_VOICE_001_004_013',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            }
        },
        {
            'id': 17,
            'category': 'Adminstation',
            'issue': 'Permintaan Cabut Layanan',
            'fiberSpec': {'symptom': 'Z_PERMINTAAN_003', 'ownerGroup': '', 'type': 'ADMIN'},
            'fiberUnderSpec': {'symptom': 'Z_PERMINTAAN_003', 'ownerGroup': '', 'type': 'ADMIN'},
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 18,
            'category': 'Adminstation',
            'issue': 'Permintaan Migrasi/ merubah Paket',
            'fiberSpec': {'symptom': 'Z_PERMINTAAN_001', 'ownerGroup': '', 'type': 'ADMIN'},
            'fiberUnderSpec': {'symptom': 'Z_PERMINTAAN_001', 'ownerGroup': '', 'type': 'ADMIN'},
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 19,
            'category': 'Adminstation',
            'issue': 'Permintaan Rincian Tagihan',
            'fiberSpec': {'symptom': 'Z_PERMINTAAN_011', 'ownerGroup': '', 'type': 'ADMIN'},
            'fiberUnderSpec': {'symptom': 'Z_PERMINTAAN_011', 'ownerGroup': '', 'type': 'ADMIN'},
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 20,
            'category': 'Installation',
            'issue': 'My IndiHome Internet service is activated, but it is not working properly after installation',
            'fiberSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_004',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_004',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 21,
            'category': 'Installation',
            'issue': 'My IndiHome TV service is not activated',
            'fiberSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_004',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'fiberUnderSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_004',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 22,
            'category': 'Installation',
            'issue': 'My IndiHome Voice service is not activated',
            'fiberSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_007',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'fiberUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_007',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 23,
            'category': 'Installation',
            'issue': 'My IndiHome Internet service is not activated',
            'fiberSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_002 //// A_INTERNET_002_002',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_004',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 24,
            'category': 'Installation',
            'issue': 'My IndiHome TV service is activated, but it is not working properly after installation',
            'fiberSpec': {
                'symptom': 'A_IPTV //// A_IPTV_002 //// A_IPTV_002_002',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'fiberUnderSpec': {
                'symptom': 'A_IPTV //// A_IPTV_002 //// A_IPTV_002_002',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 25,
            'category': 'Installation',
            'issue': 'My IndiHome Voice service is activated, but it is not working properly after installation',
            'fiberSpec': {
                'symptom': 'A_VOICE //// A_VOICE_002 //// A_VOICE_002_002',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'fiberUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_002 //// A_VOICE_002_002',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        }]
    ,
    ID: [
        {
            'id': 1,
            'category': 'Internet',
            'issue': 'Telepon, Internet dan USeeTV tidak dapat berfungsi',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_001 //// A_INTERNET_001_001_002',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 2,
            'category': 'Internet',
            'issue': 'Tidak bisa terkoneksi internet',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_001 //// A_INTERNET_001_001_001',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 3,
            'category': 'Internet',
            'issue': 'Kecepatan internet sangat lambat',
            'fiberSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_002 //// A_INTERNET_001_002_003',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_005 //// A_INTERNET_001_005_001',
                'ownerGroup': '',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 4,
            'category': 'Internet',
            'issue': 'Koneksi internet tidak stabil (putus-putus)',
            'fiberSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_002 //// A_INTERNET_001_002_004',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_005 //// A_INTERNET_001_006_001',
                'ownerGroup': '',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 5,
            'category': 'Internet',
            'issue': 'ONT/ Modem tidak dapat berfungsi normal',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_003 //// A_INTERNET_001_003_003',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 6,
            'category': 'TV',
            'issue': 'Layanan UseeTV tidak dapat berfungsi',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_001 //// A_IPTV_001_001_002',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 7,
            'category': 'TV',
            'issue': 'Telepon, Internet dan USeeTV tidak dapat berfungsi',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_001 //// A_INTERNET_001_001_002',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 8,
            'category': 'TV',
            'issue': 'USeeTV normal namun layar TV tidak memunculkan gambar',
            'fiberSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_002 //// A_IPTV_001_002_014',
                'ownerGroup': 'DTVV SA',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 9,
            'category': 'TV',
            'issue': 'TV on demand tidak stabil (putus-putus)',
            'fiberSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_002 //// A_IPTV_001_002_010',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 10,
            'category': 'TV',
            'issue': 'Remote UseeTV tidak berfungsi',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_003 //// A_IPTV_001_003_002',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 11,
            'category': 'TV',
            'issue': 'Set-Top-Box (STB) tidak berfungsi',
            'fiberSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'fiberUnderSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_003 //// A_IPTV_001_003_001',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 12,
            'category': 'TV',
            'issue': 'Konten di UseeTV tidak tampil dilayar',
            'fiberSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_002 //// A_IPTV_001_002_016',
                'ownerGroup': 'DTVV SA',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 13,
            'category': 'TELEPHONE',
            'issue': 'Tidak dapat melakukan panggilan telepon',
            'fiberSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_001 //// A_VOICE_001_001_004',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_002 //// A_VOICE_001_002_004',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_003 //// A_VOICE_001_003_004',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'copperUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_004 //// A_VOICE_001_004_004',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            }
        },
        {
            'id': 14,
            'category': 'TELEPHONE',
            'issue': 'Telepon tidak dapat menerima panggilan',
            'fiberSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_001 //// A_VOICE_001_001_005',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_002 //// A_VOICE_001_002_005',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'copperSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_003 //// A_VOICE_001_003_005',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'copperUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_004 //// A_VOICE_001_004_005',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            }
        },
        {
            'id': 15,
            'category': 'TELEPHONE',
            'issue': 'Telepon tidak ada nada',
            'fiberSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_001 //// A_VOICE_001_001_009',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_002 //// A_VOICE_001_002_009',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            },
            'copperSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_003 //// A_VOICE_001_003_009',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'copperUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_004 //// A_VOICE_001_004_009',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISK'
            }
        },
        {
            'id': 16,
            'category': 'TELEPHONE',
            'issue': 'Suara telepon tidak jernih/ kemerosok',
            'fiberSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_001 //// A_VOICE_001_001_012',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'fiberUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_002 //// A_VOICE_001_002_012',
                'ownerGroup': 'ROC-2',
                'type': 'FISIK'
            },
            'copperSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_003 //// A_VOICE_001_003_013',
                'ownerGroup': 'ROC-2',
                'type': 'LOGIC'
            },
            'copperUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_004 //// A_VOICE_001_004_013',
                'ownerGroup': 'TAW HD JAKPUS',
                'type': 'FISIK'
            }
        },
        {
            'id': 17,
            'category': 'Adminstation',
            'issue': 'Permintaan Cabut Layanan',
            'fiberSpec': {'symptom': 'Z_PERMINTAAN_003', 'ownerGroup': '', 'type': 'ADMIN'},
            'fiberUnderSpec': {'symptom': 'Z_PERMINTAAN_003', 'ownerGroup': '', 'type': 'ADMIN'},
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 18,
            'category': 'Adminstation',
            'issue': 'Permintaan Migrasi/ merubah Paket',
            'fiberSpec': {'symptom': 'Z_PERMINTAAN_001', 'ownerGroup': '', 'type': 'ADMIN'},
            'fiberUnderSpec': {'symptom': 'Z_PERMINTAAN_001', 'ownerGroup': '', 'type': 'ADMIN'},
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 19,
            'category': 'Adminstation',
            'issue': 'Permintaan Rincian Tagihan',
            'fiberSpec': {'symptom': 'Z_PERMINTAAN_011', 'ownerGroup': '', 'type': 'ADMIN'},
            'fiberUnderSpec': {'symptom': 'Z_PERMINTAAN_011', 'ownerGroup': '', 'type': 'ADMIN'},
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 20,
            'category': 'Installation',
            'issue': 'My IndiHome Internet service is activated, but it is not working properly after installation',
            'fiberSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_004',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_004',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 21,
            'category': 'Installation',
            'issue': 'My IndiHome TV service is not activated',
            'fiberSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_004',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'fiberUnderSpec': {
                'symptom': 'A_IPTV //// A_IPTV_001 //// A_IPTV_001_004',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 22,
            'category': 'Installation',
            'issue': 'My IndiHome Voice service is not activated',
            'fiberSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_007',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'fiberUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_001 //// A_VOICE_001_007',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 23,
            'category': 'Installation',
            'issue': 'My IndiHome Internet service is not activated',
            'fiberSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_002 //// A_INTERNET_002_002',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'fiberUnderSpec': {
                'symptom': 'A_INTERNET //// A_INTERNET_001 //// A_INTERNET_001_004',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 24,
            'category': 'Installation',
            'issue': 'My IndiHome TV service is activated, but it is not working properly after installation',
            'fiberSpec': {
                'symptom': 'A_IPTV //// A_IPTV_002 //// A_IPTV_002_002',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'fiberUnderSpec': {
                'symptom': 'A_IPTV //// A_IPTV_002 //// A_IPTV_002_002',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        },
        {
            'id': 25,
            'category': 'Installation',
            'issue': 'My IndiHome Voice service is activated, but it is not working properly after installation',
            'fiberSpec': {
                'symptom': 'A_VOICE //// A_VOICE_002 //// A_VOICE_002_002',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'fiberUnderSpec': {
                'symptom': 'A_VOICE //// A_VOICE_002 //// A_VOICE_002_002',
                'ownerGroup': 'WOC FULFILLMENT JAKARTA PUSAT',
                'type': 'FISIK'
            },
            'copperSpec': {'symptom': '', 'ownerGroup': '', 'type': ''},
            'copperUnderSpec': {'symptom': '', 'ownerGroup': '', 'type': ''}
        }]
};
