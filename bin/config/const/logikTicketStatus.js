

const received = {
    status: 'RECEIVED',
    title: 'Report Received',
    text: 'We are trying to solve this issue'
};

const inProgress = {
    status: 'IN_PROGRESS',
    title: 'Repair in progress',
    text: 'We have assigned a special team to resolve this issue'
};

const solved = {
    status: 'SOLVED',
    title: 'Issue solved',
    text: 'Issue has been solved'
};

module.exports.RECEIVED ={
    ...received,
    steps: [
        {
            ...received,
            active: true
        },
        {
            ...inProgress,
            active: false
        },
        {
            ...solved,
            active: false
        }
    ]
};

module.exports.IN_PROGRESS ={
    ...inProgress,
    steps: [
        {
            ...received,
            active: true
        },
        {
            ...inProgress,
            active: true
        },
        {
            ...solved,
            active: false
        }
    ]
};

module.exports.SOLVED ={
    ...solved,
    steps: [
        {
            ...received,
            active: true
        },
        {
            ...inProgress,
            active: true
        },
        {
            ...solved,
            active: true
        }
    ]
};
