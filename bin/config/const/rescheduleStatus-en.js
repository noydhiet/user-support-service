/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 13/12/2019, 3:34 PM
 */
module.exports.OK = {
    rescheduleAvailable: true,
    reason: '',
    message: '',
    code: 200
};

module.exports.RESCHEDULE_ATTEMPTS_EXCEEDED = {
    rescheduleAvailable: false,
    reason: 'Sorry, you can\'t reschedule anymore',
    message: 'You have exceeded reschedule installation limit, Please contact our customer service for further information',
    code: 410
};

module.exports.NO_OPEN_BOOKINGS = {
    rescheduleAvailable: false,
    reason: 'Sorry, you can\'t reschedule',
    message: 'You dont have active installation, Please contact our customer service for further information',
    code: 411
};

module.exports.WORK_ORDER_NOT_UPDATED = {
    rescheduleAvailable: false,
    reason: 'Sorry, you can\'t reschedule',
    message: 'Your order number has not updated yet, Please contact our customer service for further information',
    code: 412
};
