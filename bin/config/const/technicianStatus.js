/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 8/11/2019, 3:44 PM
 */

const booked = {
    status: 'BOOKED',
    title: '',
    text: '',
};

const onTheWay = {
    status: 'ON_THE_WAY',
    title: 'Technician is on the way',
    text: 'Technician is on the way and will be there soon',
};

const inProgress = {
    status: 'IN_PROGRESS',
    title: 'Installation in progress',
    text: 'Technician is installing your new package'
};


const completed = {
    status: 'COMPLETED',
    title: 'Installation complete',
    text: 'Installation complete! Enjoy your Indihome package!',
};

module.exports.BOOKED = {
    ...booked,
    steps: [
        {
            ...onTheWay,
            active: false
        },
        {
            ...inProgress,
            active: false
        }, {
            ...completed,
            active: false
        }
    ]
};

module.exports.ON_THE_WAY = {
    ...onTheWay,
    steps: [
        {
            ...onTheWay,
            active: true
        },
        {
            ...inProgress,
            active: false
        }, {
            ...completed,
            active: false
        }
    ]
};

module.exports.IN_PROGRESS = {
    ...inProgress,
    steps: [
        {
            ...onTheWay,
            active: true
        },
        {
            ...inProgress,
            active: true
        }, {
            ...completed,
            active: false
        }
    ]
};

module.exports.COMPLETED = {
    ...completed,
    steps: [
        {
            ...onTheWay,
            active: true
        },
        {
            ...inProgress,
            active: true
        }, {
            ...completed,
            active: true
        }
    ]
};
