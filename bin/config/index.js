/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 7/11/2019, 6:36 PM
 */
require('dotenv').config();
const confidence = require('confidence');

const config = {
    port: process.env.PORT,
    authorization: {
        host: process.env.AUTHORIZATION_API_HOST,
        endpoint: process.env.AUTHORIZATION_API_ENDPOINT,
        token: process.env.AUTHORIZATION_TOKEN,
        timeOut: process.env.AUTHORIZATION_TIMEOUT
    },
    authentication: {
        token: process.env.SHARED_SECRET
    },
    basicAuthApi: [
        {
            username: process.env.BASIC_AUTH_USERNAME,
            password: process.env.BASIC_AUTH_PASSWORD
        }
    ],
    technician: {
        schedule: {
            listAvailableInstallationTimesHost: process.env.TECHNITION_LIST_INSTALLATION_AVAILABILITY_HOST,
            listAvailableInstallationAttempts: process.env.TECHNITION_LIST_INSTALLATION_ATTEMPTS,
            listAvailableInstallationDefaultDays: process.env.TECHNITION_LIST_INSTALLATION_DAYS,
            listAvailableServiceTimesHost: process.env.TECHNITION_LIST_SERVICE_AVAILABILITY_HOST,
            numberOfAvailabilityDays: process.env.TECHNITION_LIST_NUMBER_OF_DAYS,
            defaultSTO: process.env.TECHNITION_LIST_DEFAULT_STO,
            reserveAppointmentHost: process.env.TECHNITION_RESERVE_APPOINTMENT_HOST,
            finalizeInstallationHost: process.env.TECHNITION_FINALYZE_APPOINTMENT_HOST,
            rescheduleAppointmentHost: process.env.TECHNICIAN_RESCHEDULE_APPOINTMENT_ENDPOINT,
            maxRescheduleAttempts: process.env.TECHNICIAN_MAX_RESCHEDULE_ATTEMPTS,
            getTechnicianInformation: process.env.TECHNITION_CREW_LABOUR_DETAILS_ENDPOINT,
            technicianCrewInformationEndpoint: process.env.TECHNICIAN_CREW_INFORMATION_ENDPOINT,
            assuranceCrewInformationEndpoint: process.env.TECHNICIAN_ASSURANCE_CREW_INFORMATION_ENDPOINT,
            technicianCrewTechnicianDetailsEndpoint: process.env.TECHNITION_CREW_TECHNICIAN_DETAILS_ENDPOINT,
            technicianWorkorderInformationEndpoint: process.env.TECHNICIAN_WORKORDER_INFORMATION_ENDPOINT,
            technicianTimeSlotsMaxRetry: process.env.TECHNICIAN_GET_TIME_SLOTS_MAX_RETRY
        }
    },
    notificationService: {
        host: process.env.NOTIFICATION_SERVICE_HOST,
        push: {
            endpoint: process.env.NOTIFICATION_SERVICE_PUSH_NOTIFICATION_ENDPOINT
        }
    },
    userProfileService: {
        host: process.env.USER_PROFILE_SERVICE_HOST,
        getUserAccountEndpoint: process.env.USER_PROFILE_GET_USER_ACCOUNT_ENDPOINT,
        getUserProfileEndpoint: process.env.USER_PROFILE_GET_USER_PROFILE_ENDPOINT
    },
    identityBaseUrl: process.env.IDENTITY_BASE_URL,
    authenticationPort: process.env.AUTHENTICATION_PORT,
    indihomeBaseUrl: process.env.INDIHOME_BASE_URL,
    indihomeBasicAuth: process.env.INDIHOME_BASIC_AUTH,
    jwtAuthKey: process.env.JWT_AUTH_KEY,
    dsnSentryUrl: process.env.DSN_SENTRY_URL,
    elasticsearch: {
        connectionClass: process.env.ELASTICSEARCH_CONNECTION_CLASS,
        apiVersion: process.env.ELASTICSEARCH_API_VERSION,
        host: [
            process.env.ELASTICSEARCH_HOST
        ],
        maxRetries: process.env.ELASTICSEARCH_MAX_RETRIES,
        requestTimeout: process.env.ELASTICSEARCH_REQUEST_TIMEOUT,
        size: process.env.ELASTICSEARCH_MAX_SIZE
    },
    elasticIndexes: {
        question: process.env.ELASTICSEARCH_QUESTIONS_INDEX,
        issue: process.env.ELASTICSEARCH_ISSUES_INDEX,
        answer: process.env.ELASTICSEARCH_ANSWERS_INDEX,
        ticket: process.env.ELASTICSEARCH_TICKETS_INDEX,
        topic: process.env.ELASTICSEARCH_TOPICS_INDEX,
        categories: process.env.ELASTICSEARCH_CATEGORIES_INDEX,
        hardware: process.env.ELASTICSEARCH_HARDWARE_INDEX,
        feedback: process.env.ELASTICSEARCH_FEEDBACKS_INDEX,
        installationBooking: process.env.ELASTICSEARCH_INSTALLATOIN_BOOKING_INDEX,
        ticketCategory: process.env.ELASTICSEARCH_TICKET_CATEGORY_INDEX,
        supportIssues: process.env.ELASTICSEARCH_SUPPORT_ISSUES_INDEX,
        supportSolutions: process.env.ELASTICSEARCH_SUPPORT_SOLUTIONS_INDEX,
        serviceBooking: process.env.ELASTICSEARCH_SERVICE_BOOKING_INDEX,
        installationSlot: process.env.ELASTICSEARCH_INSTALLATION_SLOT_INDEX,
        serviceSlot: process.env.ELASTICSEARCH_SERVICE_SLOT_INDEX,
        installationRescheduleAttempts: process.env.ELASTICSEARCH_INSTALLATION_ATTEMPTS_INDEX,
        bookingConfirmation: process.env.ELASTICSEARCH_BOOKING_CONFIRMATION_INDEX,
        ticketRequests: process.env.ELASTICSEARCH_TICKET_REQUEST_INDEX
    },
    supportService: {
        reportIssueEndpoint: process.env.SUPPORT_REPORT_ISSUE_ENDPOINT,
        reopenTicketEndpoint: process.env.SUPPORT_REOPEN_ISSUE_ENDPOINT,
        updateIncidentTicketEndpoint: process.env.SUPPORT_UPDATE_INCIDENT_ENDPOINT,
        checkCableTypeEndpoint: process.env.SUPPORT_CABLE_TYPE_CHECK_ENDPOINT,
        checkIboosterRangeEndpoint: process.env.SUPPORT_IBOOSTER_RANGE_CHECK_ENDPOINT,
        resetModemEndpoint: process.env.SUPPORT_RESET_MODEM_ENDPOINT,
        reportIssueParams: {
            locale: 'id_id',
            class: 'INCIDENT',
            externalSystem: 'RIGHTNOW',
            internalPriority: '2',
            channel: '40',
            gaul: '0',
            hardComplaint: '2',
            lapul: '0',
            urgensi: '3',
            symptomID: '',
            segment: 'DCS'
        }
    },
    logLevel: process.env.LOG_LEVEL,
    requestLogType: process.env.REQEST_LOG_TYPE
};

const store = new confidence.Store(config);

exports.get = (key) => store.get(key);
