/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 12/27/2019
 */

const SupportTypesCommand = require('../repositories/commands/domain');
const {BadRequestError} = require('../../../helpers/error');
const supportTypesCommand = new SupportTypesCommand();
const wrapper = require('../../../helpers/utils/wrapper');
const _ = require('lodash');
const Logger = require('../../../helpers/utils/logger');
const logger = new Logger('TickerCategoriesHandler');
const {LANGUAGE_HEADER,LANGUAGE} = require('../../../config/const/commonConstants');

/**
 * Add new category
 * @param {Object} category - ticket category
 * @returns {Promise<*>}
 */
module.exports.addTicketCategory = async (category) => {
    return supportTypesCommand.addTicketCategory(category);
};


/**
 * Update category
 * @param {Object} category - ticket category
 * @returns {Promise<*>}
 */
module.exports.updateTicketCategory = async (category) => {
    return supportTypesCommand.UpdateTicketCategory(category);
};

/**
 * Get all ticket categories by language
 */
module.exports.getAllTicketCategories = async (req, res) => {
    const language = req.get(LANGUAGE_HEADER) || LANGUAGE.EN;
    if(_.isEmpty(language)){
        throw new BadRequestError('language cannot be empty');
    }
    const result = await supportTypesCommand.getAllTicketCategories(language);
    if(result.err) {
        logger.error('Error in get all ticket categories.', result.err);
        wrapper.response(res, 'fail', result)
    } else {
        logger.info(`Successfully received all ticket categories. Response: ${JSON.stringify(result)}`);
        wrapper.response(res, 'success', result, 'Received all ticket categories successfully')
    }
};

/**
 * Get all issues by category
 */
module.exports.getAllIssuesByLanguage = async (language) => {
    if(_.isEmpty(language)){
        throw new BadRequestError('language cannot be empty');
    }
    const result = await supportTypesCommand.getAllIssuesByLanguage(language);
    if(result.err) {
        logger.error('Error in getting Issues.', result.err);
        throw new BadRequestError('Error in getting Issues');
    }
    logger.info(`Successfully received all Issues by language. Response length: ${result.data.length}`);
    return result.data;

};

/**
 * Get all issues by category
 */
module.exports.getIssuesByCategoryId = async (req, res) => {
    const categoryId = req.params.categoryId;
    if(_.isEmpty(categoryId)){
        throw new BadRequestError('categoryId cannot be empty');
    }
    const result = await supportTypesCommand.getIssuesByCategoryId(categoryId);
    if(result.err) {
        logger.error('Error in get issues by categoryId.', result.err);
        wrapper.response(res, 'fail', result)
    } else {
        logger.info(`Successfully received all issues by categoryId. Response length: ${result.data.length}`);
        wrapper.response(res, 'success', result, 'Received all issues by category successfully')
    }
};

/**
 * Get all technician issues
 */
module.exports.getTechnicianIssues = async (req, res) => {
    const language = req.get(LANGUAGE_HEADER) || LANGUAGE.EN;
    if(_.isEmpty(language)){
        throw new BadRequestError('language cannot be empty');
    }
    const result = await supportTypesCommand.getTechnicianIssues(language);
    if(result.err) {
        logger.error('Error in get technician issues.', result.err);
        wrapper.response(res, 'fail', result)
    } else {
        logger.info(`Successfully received all technician issues. Response length: ${result.data.length}`);
        wrapper.response(res, 'success', result, 'Received all technician issues successfully')
    }
};

/**
 * Get solutions by solution ids
 */
module.exports.getSolutions = async (req, res) => {
    const solutionIds = req.body.solutionIds;
    if(_.isEmpty(solutionIds) || !_.isArray(solutionIds)){
        throw new BadRequestError('Invalid solutionIds given');
    }
    const result = await supportTypesCommand.getSolutions(solutionIds);
    if(result.err) {
        logger.error('Error in get solutions.', result.err);
        wrapper.response(res, 'fail', result)
    } else {
        logger.info(`Successfully received all solutions. Response length: ${result.data.length}`);
        wrapper.response(res, 'success', result, 'Received all solutions successfully')
    }
};
