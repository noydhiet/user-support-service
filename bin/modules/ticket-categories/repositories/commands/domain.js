/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 12/27/2019
 */
const wrapper = require('../../../../helpers/utils/wrapper');
const Logger = require('../../../../helpers/utils/logger');
const logger = new Logger('TickerCategoriesDomain');
const {ForbiddenError} = require('../../../../helpers/error');
const Command = require('./command');
const {CATEGORIES} = require('../../../../config/const/ticketConstants');
/**
 * ES Support ticket
 */
class SupportTypes {

    constructor() {
        this.command = new Command();
    }

    async addTicketCategory(category) {
        let result = await this.command.addCategory(category);
        if (result.err) {
            throw new ForbiddenError('Add new category Failed');
        }
        return wrapper.data(result.data);
    }

    async UpdateTicketCategory(category) {
        let result = await this.command.updateCategory(category);
        if (result.err) {
            throw new ForbiddenError('Update category Failed');
        }
        return wrapper.data(result.data);
    }

    async getAllTicketCategories(language) {
        const query = {
            bool: {
                must: [
                    {
                        match: {
                            'language': language
                        }
                    },
                    {
                        match: {
                            'show': true
                        }
                    }
                ],
            }
        };
        logger.info(`Query: ${JSON.stringify(query)}`);
        let result = await this.command.getAllCategories(query);
        if (result.err) {
            return wrapper.error(new ForbiddenError('Get all category Failed'));
        }
        return wrapper.data(result.data);
    }

    async getAllIssuesByLanguage(language) {
        let result = await this.command.getAllIssuesByLanguage({language});
        if (result.err) {
            return wrapper.error(new ForbiddenError('Get all issues by language Failed'));
        }

        return wrapper.data(result.data);
    }

    async getIssuesByCategoryId(categoryId) {
        let result = await this.command.getIssuesByCategoryId({categoryId});
        if (result.err) {
            return wrapper.error(new ForbiddenError('Get all issues by category Failed'));
        }
        return wrapper.data(result.data);
    }

    async getTechnicianIssues(language) {
        const query = {
            bool: {
                must: [
                    {
                        match: {
                            'language': language
                        }
                    },
                    {
                        match: {
                            'categoryId': CATEGORIES.EN.INSTALLATION.ID.replace('_en', `_${language}`)
                        }
                    }
                ],
            }
        };
        logger.info(`Query: ${JSON.stringify(query)}`);
        let result = await this.command.getTechnicianIssues(query);
        if (result.err) {
            return wrapper.error(new ForbiddenError('Get all technician issues by language Failed'));
        }
        return wrapper.data(result.data);
    }

    /**
     * Get solutions by theirIds
     * @param {String} solutionIds - solutionIds array
     */
    async getSolutions(solutionIds) {
        let result = await this.command.getSolutions(solutionIds);
        if (result.err) {
            return wrapper.error(new ForbiddenError('Get all solutions Failed'));
        }
        return wrapper.data(result.data);
    }
}

module.exports = SupportTypes;
