/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 12/27/2019
 */
const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

/**
 * ES functions for ticket category
 */
class SupportTypes {

    constructor() {
        this.elastic = elastic;
    }

    async addCategory(category) {
        const data = {
            index: config.get('/elasticIndexes/ticketCategory'),
            type: '_doc',
            id: category.categoryId,
            body: category
        };
        return this.elastic.insertData(config.get('/elasticsearch'), data);
    }

    async updateCategory(category) {
        const data = {
            index: config.get('/elasticIndexes/ticketCategory'),
            type: '_doc',
            id: category.categoryId,
            body: {
                doc: category
            }
        };
        return this.elastic.updateData(config.get('/elasticsearch'), data);
    }

    async getAllCategories(param) {
        const query = {
            index: config.get('/elasticIndexes/ticketCategory'),
            type: '_doc',
            body: {
                from: 0,
                size: config.get('/elasticsearch/size'),
                query: param
            }
        };
        return this.elastic.findAll(config.get('/elasticsearch'), query);
    }

    async getIssuesByCategoryId(match) {
        const query = {
            index: config.get('/elasticIndexes/supportIssues'),
            type: '_doc',
            body: {
                from: 0,
                size: config.get('/elasticsearch/size'),
                query: {
                    match
                }
            }
        };
        return this.elastic.findAll(config.get('/elasticsearch'), query);
    }

    async getTechnicianIssues(param) {
        const query = {
            index: config.get('/elasticIndexes/supportIssues'),
            type: '_doc',
            body: {
                from: 0,
                size: config.get('/elasticsearch/size'),
                query: param
            }
        };
        return this.elastic.findAll(config.get('/elasticsearch'), query);
    }

    async getAllIssuesByLanguage(match) {
        const query = {
            index: config.get('/elasticIndexes/supportIssues'),
            type: '_doc',
            body: {
                from: 0,
                size: config.get('/elasticsearch/size'),
                query: {
                    match
                }
            }
        };
        return this.elastic.findAll(config.get('/elasticsearch'), query);
    }

    async getSolutions(solutionIds) {
        const query = {
            index: config.get('/elasticIndexes/supportSolutions'),
            type: '_doc',
            body: {
                from: 0,
                size: config.get('/elasticsearch/size'),
                query: {
                    terms: {
                        solutionId : solutionIds
                    }
                }
            }
        };
        return this.elastic.findAll(config.get('/elasticsearch'), query);
    }
}

module.exports = SupportTypes;
