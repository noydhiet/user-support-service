/**
 * Created by nishanif on 15/11/19
 */
const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');
const _ = require('lodash');

/**
 * ES Query for Ticket
 */
class Query {

    constructor() {
        this.elastic = elastic;
    }

    /**
     * Get all tickets
     * @returns {Promise<*>}
     */
    async getAllTickets(param) {
        let query;
        if(_.isEmpty(param)){
            query = {
                index: config.get('/elasticIndexes/ticket'),
                type: '_doc',
                body: {
                    from: 0,
                    size: config.get('/elasticsearch/size'),
                    query: {
                        match_all: {}
                    }
                }
            };
        }
        else{
            query = {
                index: config.get('/elasticIndexes/ticket'),
                type: '_doc',
                body: {
                    from: 0,
                    size: config.get('/elasticsearch/size'),
                    query: param
                }
            };
        }

        const result = this.elastic.findAll(config.get('/elasticsearch'), query);
        return result;
    }

    /**
     * Get all tickets by param filter
     * @param {Object} param - filter by param
     * @returns {Promise<*>}
     */
    async getTicketsByParam(param) {
        const query = {
            index: config.get('/elasticIndexes/ticket'),
            type: '_doc',
            body: {
                from: 0,
                size: config.get('/elasticsearch/size'),
                query: param
            }
        };
        const result = this.elastic.findAll(config.get('/elasticsearch'), query);
        return result;
    }

    /**
     * Get a ticket by param filter
     * @param {Object} param - filter by param
     * @returns {Promise<*>}
     */
    async getTicketByParam(param) {
        const query = {
            index: config.get('/elasticIndexes/ticket'),
            type: '_doc',
            body: {
                query: param
            }
        };
        const result = this.elastic.findOne(config.get('/elasticsearch'), query);
        return result;
    }

     /**
     * Get pending ticket reqs by user and ticket type
     * @param {Object} param - filter by param
     * @returns {Promise<*>}
     */
    async getTicketPendingTicketReq(param) {
        const query = {
            index: config.get('/elasticIndexes/ticketRequests'),
            type: '_doc',
            body: {
                query: param
            }
        };
        const result = this.elastic.findAll(config.get('/elasticsearch'), query);
        return result;
    }

}

module.exports = Query;

