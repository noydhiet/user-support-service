/**
 * Created by nishanif on 15/11/19
 */
const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const {NotFoundError} = require('../../../../helpers/error');
const _ = require('lodash');

/**
 * ES Query request for Ticket
 */
class Ticket {

    constructor() {
        this.query = new Query();
    }

    async getAllTickets(openStatus) {
        let query;
        if(!_.isEmpty(openStatus)){
            query = {
                bool: {
                    must: [
                        {
                            match: {
                                'open': openStatus
                            }
                        }
                    ],
                }
            };
        }

        const result = await this.query.getAllTickets(query);
        if (result.err) {
            return wrapper.error(new NotFoundError('Ticket data not found'));
        }

        return wrapper.data(result.data);
    }

    async getTicketByTicketId(ticketId) {
        const query = {
            bool: {
                must: [
                    {
                        match: {
                            'ticketId.keyword': ticketId
                        }
                    }
                ]
            }
        };

        const result = await this.query.getTicketByParam(query);
        if(!result || result.err) {
            return wrapper.error(new NotFoundError('Ticket data not found'));
        }

        return wrapper.data(result.data);
    }

    async getTicketByTransactionId(transactionId) {
        const query = {
            bool: {
                must: [
                    {
                        match: {
                            'EXTTransactionID.keyword': transactionId
                        }
                    }
                ]
            }
        };

        const result = await this.query.getTicketByParam(query);
        if(!result || result.err) {
            return wrapper.error(new NotFoundError('Ticket data not found'));
        }

        return wrapper.data(result.data);
    }

    async getAllTicketsByUserId(userId, openStatus) {
        let query;
        if(!_.isEmpty(openStatus)){
            query = {
                bool: {
                    must: [
                        {
                            match: {
                                'userId.keyword': userId
                            }
                        },
                        {
                            match: {
                                'open': openStatus
                            }
                        },
                        {
                            exists: {
                                field: 'categoryType'
                            }
                        }
                    ],
                }
            };
        }
        else{
            query = {
                bool: {
                    must: [
                        {
                            match: {
                                'userId': userId
                            }
                        },
                        {
                            exists: {
                                field: 'categoryType'
                            }
                        }
                    ],
                }
            };
        }

        const result = await this.query.getTicketsByParam(query);

        if (result.err) {
            return wrapper.error(new NotFoundError('Ticket data not found.'));
        }

        return await this.generateTicketResults(result);
    }

    async getPendingTicketsByType(userId, categoryType) {
        const query = {
            bool: {
                must_not : [
                    { match : { open : false } }
                ],
                must : [
                    { match : { userId : userId } },
                    { match : { categoryType : categoryType } }
                ]
            }
        };

        const result = await this.query.getTicketsByParam(query);

        if(result.err) {
            return wrapper.error(new NotFoundError('Ticket data not found.'));
        }

        return await this.generateTicketResults(result);
    }

    async generateTicketResults(result){
        const data = result.data.map(v => {
            const { EXTTransactionID, category, categoryType, locale, incidentList, ticketId, userId,
                open, ticketStatus, technician, technicianAssigned, bookingSlot, scheduledTime, rescheduleAttempts } = v;
            return {
                ticketId,
                userId,
                category,
                categoryType,
                EXTTransactionID,
                locale,
                open,
                incidentList,
                ticketStatus,
                technician, technicianAssigned,
                bookingSlot, scheduledTime, rescheduleAttempts
            };
        });
        return wrapper.data(data);
    }

    async getTicketStatus(ticketID) {
        const result = await this.getTicketByTicketId(ticketID);
        if(!result || result.err) {
            throw new NotFoundError('Ticket data not found.');
        }

        /**
         *   ticketStatus is an Array of applied status on ticket so far
         *   Array will help track ticket control flor in Frontend
         *   Assumed last in the array to be latest ticket status
         */
        const { ticketId, userId,  category, categoryType, locale, EXTTransactionID, incidentList, open,
            ticketStatus, technician, technicianAssigned, bookingSlot, scheduledTime, rescheduleAttempts} = result.data;
        const data = {
            ticketId,
            userId,
            category,
            categoryType,
            EXTTransactionID,
            locale,
            open,
            incidentList,
            ticketStatus,
            technician, technicianAssigned,
            bookingSlot, scheduledTime, rescheduleAttempts
        };

        data.rescheduleFeasibility =  false;
        if(ticketId !== EXTTransactionID && rescheduleAttempts > 0) {
            data.rescheduleFeasibility =  true;
        }
        return wrapper.data(data);

    }

    async getTicketStatusByTransactionId(transactionId) {
        const result = await this.getTicketByTransactionId(transactionId);
        if(!result || result.err) {
            throw new NotFoundError('Ticket data not found.');
        }

        /**
         *   ticketStatus is an Array of applied status on ticket so far
         *   Array will help track ticket control flor in Frontend
         *   Assumed last in the array to be latest ticket status
         */
        const { ticketId, userId,  category, categoryType, locale, EXTTransactionID, incidentList, open,
            ticketStatus, technician, technicianAssigned, bookingSlot, scheduledTime, rescheduleAttempts} = result.data;
        const data = {
            ticketId,
            userId,
            category,
            categoryType,
            EXTTransactionID,
            locale,
            open,
            incidentList,
            ticketStatus,
            technician, technicianAssigned,
            bookingSlot, scheduledTime, rescheduleAttempts
        };

        data.rescheduleFeasibility =  false;
        if(ticketId !== EXTTransactionID && rescheduleAttempts > 0) {
            data.rescheduleFeasibility =  true;
        }
        return wrapper.data(data);

    }

     async getTicketPendingTicketReq(userId, ticketType) {
        const query = {
            bool: {
                must: [
                    {
                        match: {
                            'userId.keyword': userId
                        }
                    },
                    {
                        match: {
                            'ticketType.keyword': ticketType
                        }
                    },
                    {
                        match: {
                            'pending': true
                        }
                    }
                ]
            }
        };

        const result = await this.query.getTicketPendingTicketReq(query);
        if(!result || result.err) {
            return wrapper.error(new NotFoundError('Ticket req data not found'));
        }

        return wrapper.data(result.data);
    }
}

module.exports = Ticket;
