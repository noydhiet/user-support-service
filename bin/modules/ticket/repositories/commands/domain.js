/**
 * Created by nishanif on 15/11/19
 */
const wrapper = require('../../../../helpers/utils/wrapper');
const {ConflictError, NotFoundError, BadRequestError} = require('../../../../helpers/error');
const Command = require('./command');
const TicketQuery = require('../queries/query');
const TicketCommand = require('../commands/command');
const {CATEGORY_TYPES} = require('../../../../config/const/ticketConstants');
const {
    RECEIVED: LOGIK_RECEIVED, IN_PROGRESS: LOGIK_IN_PROGRESS, SOLVED: LOGIK_SOLVED
} = require('../../../../config/const/logikTicketStatus');
const {
    SUBMITTED: FISIK_SUBMITTED, IN_PROGRESS: FISIK_IN_PROGRESS, COMPLETED: FISIK_COMPLETED, RESOLVED: FISIK_RESOLVED
} = require('../../../../config/const/fisikTicketStatus');

function findLogikTicketStatus(status) {
    switch (status) {
        case LOGIK_RECEIVED.status: {
            return LOGIK_RECEIVED;
        }
        case LOGIK_IN_PROGRESS.status: {
            return LOGIK_IN_PROGRESS;
        }
        case LOGIK_SOLVED.status: {
            return LOGIK_SOLVED;
        }
        default: {
            return null;
        }
    }
}

function findFisikTicketStatus(status) {
    switch (status) {
        case FISIK_SUBMITTED.status: {
            return FISIK_SUBMITTED;
        }
        case FISIK_IN_PROGRESS.status: {
            return FISIK_IN_PROGRESS;
        }
        case FISIK_COMPLETED.status: {
            return FISIK_COMPLETED;
        }
        case FISIK_RESOLVED.status: {
            return FISIK_RESOLVED;
        }
        default: {
            return null;
        }
    }
}

/**
 * ES Support ticket
 */
class Ticket {

    constructor() {
        this.command = new Command();
        this.ticketQuery = new TicketQuery();
        this.ticketCommand = new TicketCommand();
    }

    async createTicket(payload) {
        const now = new Date().toISOString();
        payload.ticketId = payload.EXTTransactionID;
        payload.createdAt = now;
        payload.updatedAt = now;
        let result = await this.command.insertTicket(payload);
        if (result.err) {
            return wrapper.error(new ConflictError('Create Ticket Failed'));
        }
        return await wrapper.data(result.data);
    }

    async updateTicket(payload) {
        const now = new Date().toISOString();
        payload.updatedAt = now;
        let result = await this.command.updateTicket(payload);
        if (result.err) {
            return wrapper.error(new ConflictError('Update Full Ticket Failed'));
        }
        return await wrapper.data(result.data);
    }

    async updateTicketStatus(ticketId, ticketStatus, isDraft) {
        //todo check update flow
        let query;
        if(isDraft) {
            query = {
                match: {
                    'EXTTransactionID.keyword': ticketId
                }
            };
        } else {
            query = {
                match: {
                    'ticketId.keyword': ticketId
                }
            };
        }


        let ticket = await this.ticketQuery.getTicketByParam(query);
        if (ticket.err) {
            throw new NotFoundError('Ticket not found');
        }
        const updatedTicket = ticket.data;
        updatedTicket.updatedAt = new Date().toISOString();
        if (updatedTicket.ticketStatus.status === ticketStatus) {
            throw new BadRequestError(`Ticket already in ${ticketStatus}`);
        }

        let statusDescription;
        if (updatedTicket.categoryType === CATEGORY_TYPES.FISIK) {
            statusDescription = findFisikTicketStatus(ticketStatus);
        } else if (updatedTicket.categoryType === CATEGORY_TYPES.LOGIC || updatedTicket.categoryType === CATEGORY_TYPES.ADMIN) {
            statusDescription = findLogikTicketStatus(ticketStatus);
        }
        if (!statusDescription) {
            throw new BadRequestError(`invalid status: ${ticketStatus} for the ticket type: ${updatedTicket.categoryType}`);
        }
        updatedTicket.ticketStatus = statusDescription;
        const result = await this.ticketCommand.updateTicket(updatedTicket);
        if (result.err) {
            throw new ConflictError('Update Ticket status failed');
        }
        return wrapper.data(updatedTicket);

    }

    async updateTicketId(transactionId, ticketId) {
        //todo check update flow
        let query = {
            match: {
                'EXTTransactionID.keyword': transactionId
            }
        };

        let ticket = await this.ticketQuery.getTicketByParam(query);
        if (ticket.err) {
            // retry - no data found scenario
            const maxRetryCount = 5;
            for(let count = 0; count < maxRetryCount; count++) {
                ticket = await this.ticketQuery.getTicketByParam(query);
                if (ticket.err) {
                    continue;
                }
                break;
            }
            if(ticket.err) {
                throw new NotFoundError('Ticket not found');
            }
        }
        const updatedTicket = ticket.data;
        updatedTicket.updatedAt = new Date().toISOString();
        //set technician assigned status true
        //todo: add technician data? updatedTicket.technician
        updatedTicket.technicianAssigned = true;
        if (updatedTicket.ticketId === ticketId) {
            throw new BadRequestError(`Ticket id already is ${ticketId}`);
        }

        updatedTicket.ticketId = ticketId;
        const result = await this.ticketCommand.updateTicket(updatedTicket);
        if (result.err) {
            throw new ConflictError('Update Ticket ID failed');
        }
        return wrapper.data(result.data.result);
    }

    async closeOrReopenTicket(ticketId, userId, action) {
        let query = {
            match: {
                'ticketId.keyword': ticketId
            }
        };

        let ticket = await this.ticketQuery.getTicketByParam(query);
        if(ticket.data.userId !== userId){
            throw new ConflictError('Ticket mismatch with userId not found');
        }

        if (ticket.err) {
            throw new NotFoundError('Ticket not found');
        }
        const updatedTicket = ticket.data;
        updatedTicket.updatedAt = new Date().toISOString();

        //change open status to 1. false to close OR 2. true to reopen the ticket
        updatedTicket.open = action;
        const result = await this.ticketCommand.updateTicket(updatedTicket);
        if (result.err) {
            throw new ConflictError('Closing/reopening ticket failed');
        }
        return wrapper.data(updatedTicket);
    }

    async addTicketRequest(payload) {
        const now = new Date().toISOString();
        payload.createdAt = now;
        payload.updatedAt = now;
        let result = await this.command.insertTicketRequest(payload);
        if (result.err) {
            return wrapper.error(new ConflictError('Create Ticket request Failed'));
        }
        return await wrapper.data(result.data);
    }

    async updateTicketRequest(userId, ticketType, pending) {
        const query = {
            bool: {
                must: [
                    {
                        match: {
                            'userId.keyword': userId
                        }
                    },
                    {
                        match: {
                            'ticketType.keyword': ticketType
                        }
                    },
                    {
                        match: {
                            'pending': true
                        }
                    }
                ]
            }
        };

        let ticketReq = await this.ticketQuery.getTicketPendingTicketReq(query);
        if(ticketReq.data.length > 1){
            throw new ConflictError(`Multiple create ticket of type ${ticketType} requests are pending.`);
        }

        if (ticketReq.err) {
            throw new NotFoundError('Ticket request not found');
        }

         let updatedTicketReq = ticketReq.data[0];
        // in case get failed
        if(updatedTicketReq === undefined) {
            updatedTicketReq = {
                userId: userId,
                ticketType: ticketType,
            };
        }

        updatedTicketReq.updatedAt = new Date().toISOString();
        updatedTicketReq.pending = pending;

        const result = await this.ticketCommand.updateTicketRequest(updatedTicketReq);
        if (result.err) {
            throw new ConflictError(`Error in updating ticket creation request status. Err: ${JSON.stringify(result.err)}`);
        }
        return wrapper.data(result.data);
    }
}

module.exports = Ticket;

