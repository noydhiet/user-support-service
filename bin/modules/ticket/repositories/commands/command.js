/**
 * Created by nishanif on 15/11/19
 */
const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

/**
 * ES functions for Support Ticket
 */
class Ticket {

    constructor() {
        this.elastic = elastic;
    }

    async insertTicket(payload) {
        const data = {
            index: config.get('/elasticIndexes/ticket'),
            type: '_doc',
            id: payload.EXTTransactionID,
            body: payload
        };
        return this.elastic.insertData(config.get('/elasticsearch'), data);
    }

    async updateTicket(payload) {
        const ticketId = payload.EXTTransactionID;
        const ticket = {
            index: config.get('/elasticIndexes/ticket'),
            type: '_doc',
            id: ticketId,
            body: {
                doc: payload
            }
        };
        return this.elastic.updateData(config.get('/elasticsearch'), ticket);
    }

    /*
        ticketRequests methods
     */
    async insertTicketRequest(payload) {
        const data = {
            index: config.get('/elasticIndexes/ticketRequests'),
            type: '_doc',
            id: payload.userId + payload.ticketType,
            body: payload
        };
        return this.elastic.insertData(config.get('/elasticsearch'), data);
    }

    async updateTicketRequest(payload) {
        const ticketReq = {
            index: config.get('/elasticIndexes/ticketRequests'),
            type: '_doc',
            id: payload.userId + payload.ticketType,
            body: {
                doc: payload
            }
        };
        return this.elastic.updateData(config.get('/elasticsearch'), ticketReq);
    }  
}

module.exports = Ticket;
