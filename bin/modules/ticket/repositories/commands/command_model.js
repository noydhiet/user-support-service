/**
 * Created by nishanif on 15/11/19
 */
const joi = require('@hapi/joi');
const {TICKET_REQ_TYPES} = require('../../../../config/const/ticketConstants');

/**
 * Object model for ES Ticket
 */
const ticket = joi.object({
    EXTTransactionID: joi.string().required(),
    userId: joi.string().required(),
    locale: joi.string().required(),
    incidentList: joi.object().keys({
        incident: joi.array().items(joi.object())
    }).required(),
    open: joi.boolean().required(),
    ticketStatus: joi.object().keys({
        status: joi.string(),
        title: joi.string(),
        text: joi.string(),
        steps: joi.array().items(joi.object())
    }).required(),
    bookingSlot: joi.object().optional(),
    technician: joi.object().optional(),
    technicianAssigned: joi.boolean().optional(),
    category: joi.string().optional(),
    scheduledTime: joi.string().optional(),
    categoryType: joi.string().optional(),
    rescheduleAttempts: joi.number().integer().optional()
});

const ticketReq = joi.object({
    userId: joi.string().required(),
    ticketType: joi.string().valid(TICKET_REQ_TYPES.ADMIN, TICKET_REQ_TYPES.FISIKLOGIC).required(),
    pending: joi.boolean().required()
});

module.exports = {
    ticket,
    ticketReq
};
