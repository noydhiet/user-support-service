/**
 * Created by nishanif on 15/11/19
 */
const TicketCommand = require('../repositories/commands/domain');
const TicketQuery = require('../repositories/queries/domain');
const validator = require('../../../helpers/utils/validator');
const commandModel = require('../repositories/commands/command_model');
const wrapper = require('../../../helpers/utils/wrapper');

/**
 * Create Support Ticket on es
 * @param {Object} payload - request payload
 * @returns {Promise<*>}
 */
const createTicket = async (payload) => {
    const validatePayload = validator.isValidPayload(payload, commandModel.ticket);
    const ticketCommand = new TicketCommand();
    const postRequest = async (result) => {
        if (result.err) {
            return result;
        }
        return ticketCommand.createTicket(payload);
    };
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : result.data;
    };

    return sendResponse(await postRequest(validatePayload));
};

/**
 * Get Support Ticket by ticketId
 * @param {String} ticketId - ticket Id
 * @returns {Promise<void>}
 */
const getTicketByTicketId = async (ticketId) => {
    const ticketQuery = new TicketQuery();
    const getRequest = async () => ticketQuery.getTicketByTicketId(ticketId);
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };
    return sendResponse(await getRequest());
};

/**
 * Get Support Ticket by transactionId
 * @param {String} transactionId - transaction Id
 * @returns {Promise<void>}
 */
const getTicketByTransactionId = async (transactionId) => {
    const ticketQuery = new TicketQuery();
    const getRequest = async () => ticketQuery.getTicketByTransactionId(transactionId);
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };
    return sendResponse(await getRequest());
};

/**
 * Get all Support Tickets
 * @returns {Promise<void>}
 */
const getAllTickets = async (openStatus) => {
    const ticketQuery = new TicketQuery();
    const getRequest = async () => ticketQuery.getAllTickets(openStatus);
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };
    return sendResponse(await getRequest());
};

/**
 * Get all Support Tickets by userId
 * @param {String} userId - User Id
 * @returns {Promise<*|{err, data}>}
 */
const getAllTicketsByUserId = async (userId, openStatus) => {
    const ticketQuery = new TicketQuery();
    const getRequest = async () => ticketQuery.getAllTicketsByUserId(userId, openStatus);

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };

    return sendResponse(await getRequest());
};

/**
 * Get all pending Support Tickets by userId by category type
 * @param {String} userId - User Id
 * @param {String} categoryType - Ticket category: Logic, Fisik
 * @returns {Promise<*|{err, data}>}
 */
const getPendingTicketsByType = async (userId, categoryType) => {
    const ticketQuery = new TicketQuery();
    const getRequest = async () => ticketQuery.getPendingTicketsByType(userId, categoryType);

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };

    return sendResponse(await getRequest());
};

/**
 * Get Ticket status
 * @param {String} ticketId - ticket Id
 * @returns {Promise<*|{err, data}>}
 */
const getTicketStatus = async (ticketId) => {
    const ticketQuery = new TicketQuery();
    const getRequest = async () => ticketQuery.getTicketStatus(ticketId);

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };

    return  sendResponse(await getRequest());
};

/**
 * Get Ticket status by TransactionId
 * @param {String} transactionId - transaction Id
 * @returns {Promise<*|{err, data}>}
 */
const getTicketStatusByTransactionId = async (transactionId) => {
    const ticketQuery = new TicketQuery();
    const getRequest = async () => ticketQuery.getTicketStatusByTransactionId(transactionId);

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };

    return  sendResponse(await getRequest());
};

/**
 * Update Ticket status by ticketId
 * @param {String} ticketOrTransactionId - ticket Id or transaction Id
 * @param {String} ticketStatus - ticket Status
 * @param {Boolean} isDraft - true: is draft ticket, false: if real ticket
 * @returns {Promise<*|{err, data}>}
 */
const updateTicketStatus = async (ticketOrTransactionId, ticketStatus, isDraft) => {
    const ticketCommand = new TicketCommand();
    const putRequest = async () => ticketCommand.updateTicketStatus(ticketOrTransactionId, ticketStatus, isDraft);

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };

    return  sendResponse(await putRequest());
};

/**
 * Update Full Ticket by ticketId
 * @param {Object} data - ticket data
 * @returns {Promise<*|{err: null, data: *}>}
 */
const updateTicket = async (data) => {
    const ticketCommand = new TicketCommand();
    const putRequest = async () => ticketCommand.updateTicket(data);

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };

    return  sendResponse(await putRequest());
};

/**
 * Update Ticket ID by TransactionId(EXTTransactionID)
 * @param {String} transactionId - ticket Id
 * @param {String} ticketId - ticket Status
 * @returns {Promise<*|{err, data}>}
 */
const updateTicketId = async (transactionId, ticketId) => {
    const ticketCommand = new TicketCommand();
    const putRequest = async () => ticketCommand.updateTicketId(transactionId, ticketId);

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };

    return  sendResponse(await putRequest());
};

/**
 * Close ticket by ticketId
 * @param {String} ticketId - ticket Id
 * @param {String} userId - user Id
 * @param {Boolean} openStatus - close(false) or reopen(true) openStatus action
 * @returns {Promise<*|{err, data}>}
 */
const closeOrReopenTicket = async (ticketId, userId, openStatus) => {
    const ticketCommand = new TicketCommand();
    const putRequest = async () => ticketCommand.closeOrReopenTicket(ticketId, userId, openStatus);

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? result.err : wrapper.data(result.data);
    };

    return  sendResponse(await putRequest());
};

/**
 * Add ticket creation request
 * @param {Object} payload - creation request data
 * @returns {Promise<*|{err: null, data: *}>}
 */
const addTicketRequest = async (payload) => {
    const validatePayload = validator.isValidPayload(payload, commandModel.ticketReq);

    const ticketCommand = new TicketCommand();
    const putRequest = async () => ticketCommand.addTicketRequest(payload);

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err ? result.err : wrapper.data(result.data);
    };

    return sendResponse(await putRequest(validatePayload));
};

/**
 * Update ticket creation request
 * @param {String} userId - user Id
 * @param {String} ticketType - ticket req type
 * @param {Boolean} pending - true, false status
 * @returns {Promise<*|{err: null, data: *}>}
 */
const updateTicketRequestStatus = async (userId, ticketType, pending) => {
    const ticketCommand = new TicketCommand();
    const putRequest = async () => ticketCommand.updateTicketRequest(userId, ticketType, pending);

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err ? result.err : wrapper.data(result.data);
    };

    return sendResponse(await putRequest());
};

/**
 * Get pending Ticket creation requests userId
 * @param {String} userId - user Id
 * @param {String} ticketType - ticket type
 * @returns {Promise<void>}
 */
const getTicketPendingTicketReq = async (userId, ticketType) => {
    const ticketQuery = new TicketQuery();
    const getRequest = async () => ticketQuery.getTicketPendingTicketReq(userId, ticketType);
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        return result.err? wrapper.error(result.err) : wrapper.data(result.data);
    };
    return sendResponse(await getRequest());
};


module.exports = {
    createTicket,
    getTicketByTicketId,
    getTicketByTransactionId,
    getAllTickets,
    getAllTicketsByUserId,
    getPendingTicketsByType,
    getTicketStatus,
    getTicketStatusByTransactionId,
    updateTicketStatus,
    updateTicket,
    updateTicketId,
    closeOrReopenTicket,
    addTicketRequest,
    updateTicketRequestStatus,
    getTicketPendingTicketReq
};
