const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

class Query {

    constructor() {
        this.elastic = elastic;
    }

    async getAllIssues() {
        const query = {
            index: config.get('/elasticIndexes/issue'),
            type: '_doc',
            body: {
                query: {
                    match_all: {}
                }
            }
        };
        const result = this.elastic.findAll(config.get('/elasticsearch'), query);
        return result;
    }

    async getIssueByParam(param) {
        const query = {
            index: config.get('/elasticIndexes/issue'),
            type: '_doc',
            body: {
                query: {
                    match: param
                }
            }
        };
        const result = this.elastic.findOne(config.get('/elasticsearch'), query);
        return result;
    }

    async getIssuesFilteredByQueryParam(param) {
        const query = {
            index: config.get('/elasticIndexes/issue'),
            type: '_doc',
            body: {
                query: param
            }
        };
        const result = this.elastic.findAll(config.get('/elasticsearch'), query);
        return result;
    }

}

module.exports = Query;
