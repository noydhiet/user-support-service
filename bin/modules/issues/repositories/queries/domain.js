const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const {NotFoundError} = require('../../../../helpers/error');

class Issue {

    constructor() {
        this.query = new Query();
    }

    async getAllIssues(query) {
        let result;
        if(query){
            result = await this.query.getIssuesFilteredByQueryParam(query);
        }
        else{
            result = await this.query.getAllIssues();
        }

        if (result.err) {
            return wrapper.error(new NotFoundError('Issue data not found'));
        }

        const data = result.data.map(v => {
            const {issueId, issue, category, answerIds} = v;
            return {
                issueId,
                issue,
                category,
                answerLength: answerIds.length
            };
        });

        return wrapper.data(data);

    }

    async getAllIssuesFilteredByCategory(category){
        const query = {
            match: {
                'category': category
            }
        };
        return this.getAllIssues(query);
    }

}

module.exports = Issue;
