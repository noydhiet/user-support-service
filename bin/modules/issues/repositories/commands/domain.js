const uuid = require('uuid/v1');
const validate = require('validate.js');
const wrapper = require('../../../../helpers/utils/wrapper');
const { ConflictError, NotFoundError } = require('../../../../helpers/error');
const Command = require('./command');
const Query = require('../queries/query');
const AnswerCommand = require('../../../answers/repositories/commands/command');
const AnswerQuery = require('../../../answers/repositories/queries/query');

class Issue {

    constructor() {
        this.command = new Command();
        this.query = new Query();
        this.answerCommand = new AnswerCommand();
        this.answerQuery = new AnswerQuery();
    }

    async createIssue(payload) {
        const now = new Date().toISOString();
        payload.issueId = uuid();
        payload.answerIds = [];
        payload.createdAt = now;
        payload.updatedAt = now;
        let result = await this.command.insertIssue(payload);
        if (result.err) {
            return wrapper.error(new ConflictError('Create issue Failed'));
        }
        return await wrapper.data(result.data);
    }

    async updateIssue(payload) {
        const { issueId, issue, category, visibility } = payload;
        const issueData = await this.query.getIssueByParam({ issueId });

        if(issueData.err) {
            return wrapper.error(new NotFoundError('Issue not found'));
        }

        const data = issueData.data;
        data.issue = validate.isEmpty(issue) ? data.issue : issue;
        data.category = validate.isEmpty(category) ? data.category : category;
        data.visibility = validate.isEmpty(visibility) ? data.visibility : visibility;
        data.updatedAt = new Date().toISOString();

        let result = await this.command.updateIssue(data);
        if (result.err) {
            return wrapper.error(new ConflictError('Update issue Failed'));
        }
        return await wrapper.data(result.data);
    }

    async deleteIssue(issueId) {
        const issue = await this.query.getIssueByParam({ issueId });
        if(issue.err) {
            return wrapper.error(new NotFoundError('Issue not found'));
        }

        const result = await this.command.deleteIssue(issueId);
        if(result.err) {
            return wrapper.error(new ConflictError('Delete issue failed'));
        }

        issue.data.answerIds.map((answerId) => {
            this.deleteIssueOnAnswer(issueId, answerId);
        });

        return result;
    }

    async deleteIssueOnAnswer(issueId, answerId) {
        const query = {
            match: {'answerId.keyword': answerId}
        };
        const answer = await this.answerQuery.getAnswersByParam(query);
        if (answer.err) {
            return wrapper.error(new NotFoundError('Answer not found'));
        }
        let issueIds = answer.data.issueIds;
        issueIds = issueIds.filter(item => item !== issueId);
        const result = await this.answerCommand.updateAnswer({answerId, issueIds});
        if (result.err) {
            return wrapper.error(new ConflictError('Update asnwer failed'));
        }
    }

}

module.exports = Issue;
