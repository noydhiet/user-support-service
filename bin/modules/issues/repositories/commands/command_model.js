const joi = require('@hapi/joi');

const issue = joi.object({
    issue: joi.string().required(),
    category: joi.string().required()
});

const updatedIssue = joi.object({
    issueId: joi.string().required(),
    issue: joi.string().optional(),
    category: joi.string().optional(),
    visibility: joi.boolean().optional()
});

module.exports = {
    issue,
    updatedIssue
};
