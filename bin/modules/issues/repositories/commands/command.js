const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

class Command {

    constructor() {
        this.elastic = elastic;
    }

    async insertIssue(payload) {
        const data = {
            index: config.get('/elasticIndexes/issue'),
            type: '_doc',
            id: payload.issueId,
            body: payload
        };
        const result = this.elastic.insertData(config.get('/elasticsearch'), data);
        return result;
    }

    async updateIssue(payload) {
        const {issueId, ...data} = payload;
        const issue = {
            index: config.get('/elasticIndexes/issue'),
            type: '_doc',
            id: issueId,
            body: {
                doc: data
            }
        };
        const result = this.elastic.updateData(config.get('/elasticsearch'), issue);
        return result;
    }

    async deleteIssue(id) {
        const data = {
            index: config.get('/elasticIndexes/issue'),
            type: '_doc',
            id
        };
        const result = this.elastic.deleteData(config.get('/elasticsearch'), data);
        return result;
    }
}

module.exports = Command;
