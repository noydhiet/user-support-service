const wrapper = require('../../../helpers/utils/wrapper');
const IssueCommand = require('../repositories/commands/domain');
const IssueQuery = require('../repositories/queries/domain');
const validator = require('../../../helpers/utils/validator');
const commandModel = require('../repositories/commands/command_model');

const createIssue = async (req, res) => {
    const payload = req.body;
    const validatePayload = validator.isValidPayload(payload, commandModel.issue);
    const issueCommand = new IssueCommand();
    const postRequest = async (result) => {
        if (result.err) {
            return result;
        }
        return issueCommand.createIssue(payload);
    };
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Create issue successfull');
    };
    sendResponse(await postRequest(validatePayload));
};

const updateIssue = async (req, res) => {
    let payload = req.body;
    payload.issueId = req.params.issueId;
    const validatePayload = validator.isValidPayload(payload, commandModel.updatedIssue);
    const issueCommand = new IssueCommand();
    const postRequest = async (result) => {
        if (result.err) {
            return result;
        }
        return issueCommand.updateIssue(payload);
    };
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Update issue successfull');
    };
    sendResponse(await postRequest(validatePayload));
};

const getAllIssue = async (req, res) => {
    const issueQuery = new IssueQuery();
    let category = req.query.category;
    let postRequest;

    if(category){
        postRequest = async () => issueQuery.getAllIssuesFilteredByCategory(category);
    }else{
        postRequest = async () => issueQuery.getAllIssues();
    }

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Create elastic index successfull');
    };
    sendResponse(await postRequest());
};

const deleteIssue = async (req, res) => {
    const issueId = req.params.issueId;
    const issueCommand = new IssueCommand();
    const postRequest = async () => issueCommand.deleteIssue(issueId);
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Delete issue successfull');
    };
    sendResponse(await postRequest());
};

module.exports = {
    createIssue,
    getAllIssue,
    updateIssue,
    deleteIssue
};
