const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

class Query {

    constructor() {
        this.elastic = elastic;
    }

    async getAllCategory() {
        const query = {
            index: config.get('/elasticIndexes/categories'),
            type: '_doc',
            body: {
                query: {
                    match_all: {}
                }
            }
        };
        const result = this.elastic.findAll(config.get('/elasticsearch'), query);

        return result;
    }

}

module.exports = Query;
