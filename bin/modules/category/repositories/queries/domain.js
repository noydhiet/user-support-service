const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const {NotFoundError} = require('../../../../helpers/error');

class Category {

    constructor() {
        this.query = new Query();
    }

    async getAllCategory() {
        let result = await this.query.getAllCategory();
        if (result.err) {
            return wrapper.error(new NotFoundError('Category data not found'));
        }

        const data = result.data.map(v => {
            const {category} = v;
            return category;
        });

        return wrapper.data(data);

    }

}

module.exports = Category;
