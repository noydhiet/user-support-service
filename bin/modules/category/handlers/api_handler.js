const wrapper = require('../../../helpers/utils/wrapper');
const CategoryQuery = require('../repositories/queries/domain');

const getAllCategory = async (req, res) => {
    const categoryQuery = new CategoryQuery();
    let postRequest = async () => categoryQuery.getAllCategory();

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Get category successfull');
    };
    sendResponse(await postRequest());
};

module.exports = {
    getAllCategory
};
