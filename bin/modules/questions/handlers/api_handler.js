const wrapper = require('../../../helpers/utils/wrapper');
const QuestionCommand = require('../repositories/commands/domain');
const QuestionQuery = require('../repositories/queries/domain');
const validator = require('../../../helpers/utils/validator');
const commandModel = require('../repositories/commands/command_model');

const createIndexES = async (req, res) => {
    const payload = req.body;
    const questionCommand = new QuestionCommand();
    const postRequest = async (result) => {
        if (result.err) {
            return result;
        }
        return questionCommand.createIndex(payload);
    };
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Create elastic index Successfull');
    };
    sendResponse(await postRequest(payload));
};

const createQuestion = async (req, res) => {
    const payload = req.body;
    const validatePayload = validator.isValidPayload(payload, commandModel.question);
    const questionCommand = new QuestionCommand();
    const postRequest = async (result) => {
        if (result.err) {
            return result;
        }
        return questionCommand.createQuestion(payload);
    };
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Create question successfull');
    };
    sendResponse(await postRequest(validatePayload));
};

const getAllQuestion = async (req, res) => {
    let category = req.query.category;
    let postRequest;
    const questionQuery = new QuestionQuery();

    if(category){
        postRequest = async () => questionQuery.getAllQuestionsFilteredByCategory(category);
    }else{
        postRequest = async () => questionQuery.getAllQuestions();
    }

    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Get question successfull');
    };
    sendResponse(await postRequest());
};

const updateQuestion = async (req, res) => {
    let payload = req.body;
    payload.questionId = req.params.questionId;
    const validatePayload = validator.isValidPayload(payload, commandModel.updatedQuestion);
    const questionCommand = new QuestionCommand();
    const postRequest = async (result) => {
        if (result.err) {
            return result;
        }
        return questionCommand.updateQuestion(payload);
    };
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Update question successfull');
    };
    sendResponse(await postRequest(validatePayload));
};

const deleteQuestion = async (req, res) => {
    const questionId = req.params.questionId;
    const questionCommand = new QuestionCommand();
    const postRequest = async () => questionCommand.deleteQuestion(questionId);
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Delete question successfull');
    };
    sendResponse(await postRequest());
};

const getFrequentlyAskQuestion = async (req, res) => {
    const questionQuery = new QuestionQuery();
    const postRequest = async () => questionQuery.getFrequentlyAskQuestion();
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Get question successfull');
    };
    sendResponse(await postRequest());
};

module.exports = {
    createIndexES,
    createQuestion,
    getAllQuestion,
    updateQuestion,
    deleteQuestion,
    getFrequentlyAskQuestion
};
