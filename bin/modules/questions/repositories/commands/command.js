const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

class Command {

    constructor() {
        this.elastic = elastic;
    }

    async createIndex(statement) {
        const result = this.elastic.createIndex(config.get('/elasticsearch'), statement);
        return result;
    }

    async insertQuestion(payload) {
        const data = {
            index: config.get('/elasticIndexes/question'),
            type: '_doc',
            id: payload.questionId,
            body: payload
        };
        const result = this.elastic.insertData(config.get('/elasticsearch'), data);
        return result;
    }

    async updateQuestion(payload) {
        const {questionId, ...data} = payload;
        const question = {
            index: config.get('/elasticIndexes/question'),
            type: '_doc',
            id: questionId,
            body: {
                doc: data
            }
        };
        const result = this.elastic.updateData(config.get('/elasticsearch'), question);
        return result;
    }

    async deleteQuestion(id) {
        const data = {
            index: config.get('/elasticIndexes/question'),
            type: '_doc',
            id
        };
        const result = this.elastic.deleteData(config.get('/elasticsearch'), data);
        return result;
    }

}

module.exports = Command;
