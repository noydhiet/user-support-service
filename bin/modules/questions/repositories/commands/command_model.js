const joi = require('@hapi/joi');

const question = joi.object({
    question: joi.string().required(),
    category: joi.string().required()
});

const updatedQuestion = joi.object({
    questionId: joi.string().required(),
    question: joi.string().optional(),
    category: joi.string().optional(),
    visibility: joi.boolean().optional()
});

module.exports = {
    question,
    updatedQuestion
};
