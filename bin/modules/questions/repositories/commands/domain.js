const uuid = require('uuid/v1');
const validate = require('validate.js');
const wrapper = require('../../../../helpers/utils/wrapper');
const { ConflictError, NotFoundError } = require('../../../../helpers/error');
const Command = require('./command');
const Query = require('../queries/query');
const AnswerCommand = require('../../../answers/repositories/commands/command');
const AnswerQuery = require('../../../answers/repositories/queries/query');

class Question {

    constructor() {
        this.command = new Command();
        this.query = new Query();
        this.answerCommand = new AnswerCommand();
        this.answerQuery = new AnswerQuery();
    }

    async createIndex(payload) {
        let result = await this.command.createIndex(payload);
        if (result.err) {
            return wrapper.error(new ConflictError('Create Index Failed'));
        }
        return await wrapper.data(result.data);
    }

    async createQuestion(payload) {
        const now = new Date().toISOString();
        payload.questionId = uuid();
        payload.answerIds = [];
        payload.createdAt = now;
        payload.updatedAt = now;
        payload.counter = 0;
        let result = await this.command.insertQuestion(payload);
        if (result.err) {
            return wrapper.error(new ConflictError('Create Question Failed'));
        }
        return await wrapper.data(result.data);
    }

    async updateQuestion(payload) {
        const { questionId, question, category, visibility } = payload;
        const questionData = await this.query.getQuestionByParam({ questionId });

        if(questionData.err) {
            return wrapper.error(new NotFoundError('Question not found'));
        }

        const data = questionData.data;
        data.question = validate.isEmpty(question) ? data.question : question;
        data.category = validate.isEmpty(category) ? data.category : category;
        data.visibility = validate.isEmpty(visibility) ? data.visibility : visibility;
        data.updatedAt = new Date().toISOString();

        let result = await this.command.updateQuestion(data);
        if (result.err) {
            return wrapper.error(new ConflictError('Update Question Failed'));
        }
        return await wrapper.data(result.data);
    }

    async deleteQuestion(questionId) {
        const question = await this.query.getQuestionByParam({ questionId });
        if(question.err) {
            return wrapper.error(new NotFoundError('Question not found'));
        }

        const result = await this.command.deleteQuestion(questionId);
        if(result.err) {
            return wrapper.error(new ConflictError('Delete question failed'));
        }

        question.data.answerIds.map((answerId) => {
            this.deleteQuestionOnAnswer(questionId, answerId);
        });

        return result;
    }

    async deleteQuestionOnAnswer(questionId, answerId) {
        const query = {
            match: {'answerId.keyword': answerId}
        };
        const answer = await this.answerQuery.getAnswersByParam(query);
        if (answer.err) {
            return wrapper.error(new NotFoundError('Answer not found'));
        }
        let questionIds = answer.data.questionIds;
        questionIds = questionIds.filter(item => item !== questionId);
        const result = await this.answerCommand.updateAnswer({answerId, questionIds});
        if (result.err) {
            return wrapper.error(new ConflictError('Update asnwer failed'));
        }
    }

}

module.exports = Question;
