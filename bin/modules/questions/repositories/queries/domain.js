const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const {NotFoundError} = require('../../../../helpers/error');

class Question {

    constructor() {
        this.query = new Query();
    }

    async getAllQuestions(query) {
        let result;
        if(query){
            result = await this.query.getAllQuestionsFilteredByQueryParameters(query);
        }else{
            result = await this.query.getAllQuestions();
        }

        if (result.err) {
            return wrapper.error(new NotFoundError('Question data not found'));
        }

        const data = result.data.map(v => {
            const {questionId, question, category, answerIds} = v;
            return {
                questionId,
                question,
                category,
                answerLength: answerIds.length
            };
        });

        return wrapper.data(data);

    }


    async getAllQuestionsFilteredByCategory(category){
        const query = {
            match: {
                'category': category
            }
        };
        return this.getAllQuestions(query);
    }

    async getFrequentlyAskQuestion(){
        const query = {
            'sort' : { 'counter' : {'order' : 'desc'}},
            'aggregations' : {
                'counters' : {
                    'terms' : {
                        'field' : 'counter',
                        'size' : 10
                    }
                }
            }
        };

        const result = await this.query.getAllQuestionsByParam(query);
        if (result.err) {
            return wrapper.error(new NotFoundError('Question data not found'));
        }

        const data = result.data.map(v => {
            const {questionId, question, category, answerIds} = v;
            return {
                questionId,
                question,
                category,
                answerLength: answerIds.length
            };
        });

        return wrapper.data(data);
    }
}

module.exports = Question;
