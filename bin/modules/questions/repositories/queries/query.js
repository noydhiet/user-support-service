const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

class Query {

    constructor() {
        this.elastic = elastic;
    }

    async getAllQuestions() {
        const query = {
            index: config.get('/elasticIndexes/question'),
            type: '_doc',
            body: {
                sort: [
                    {counter: 'desc'}
                ],
                query: {
                    match_all: {}
                }
            }
        };
        const result = this.elastic.findAll(config.get('/elasticsearch'), query);
        return result;
    }

    async getQuestionByParam(param) {
        const query = {
            index: config.get('/elasticIndexes/question'),
            type: '_doc',
            body: {
                query: {
                    match: param
                }
            }
        };
        const result = this.elastic.findOne(config.get('/elasticsearch'), query);
        return result;
    }

    async getAllQuestionsByParam(param) {
        const query = {
            index: config.get('/elasticIndexes/question'),
            type: '_doc',
            body: param
        };
        const result = this.elastic.findAll(config.get('/elasticsearch'), query);
        return result;
    }

    async getAllQuestionsFilteredByQueryParameters(param) {
        const query = {
            index: config.get('/elasticIndexes/question'),
            type: '_doc',
            body: {
                query: param
            }
        };
        const result = this.elastic.findAll(config.get('/elasticsearch'), query);
        return result;
    }
}

module.exports = Query;
