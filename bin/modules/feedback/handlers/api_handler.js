/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 13/11/2019, 3:40 PM
 */

const wrapper = require('../../../helpers/utils/wrapper');
const validator = require('../../../helpers/utils/validator');
const commandModel = require('../repositories/commands/command_model');
const queryModel = require('../repositories/queries/query_model');
const FeedbackQuery = require('../repositories/queries/domain');
const FeedbackCommand = require('../repositories/commands/domain');
const feedbackCommand = new FeedbackCommand();
const feedbackQuery = new FeedbackQuery();

const getAllTopic = async (req, res) => {
    const postRequest = async () => {
        return feedbackQuery.getAllTopic();
    };
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Get all feedback');
    };
    sendResponse(await postRequest());
};

const createTopic = async (req, res) => {
    const payload = req.body;
    const validatePayload = validator.isValidPayload(payload, commandModel.createTopic);
    const postRequest = async (result) => {
        if (result.err) {
            return result;
        }
        return feedbackCommand.createTopic(payload);
    };
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Create topic successfull');
    };
    sendResponse(await postRequest(validatePayload));
};

const sendFeedback = async (req, res) => {

    let payload = {};
    payload.user = req.userData;
    payload.topic = req.body.topic;
    payload.feedback = req.body.feedback;
    const validatePayload = validator.isValidPayload(payload, commandModel.sendFeedback);
    const postRequest = async (result) => {
        if (result.err) {
            return result;
        }
        return feedbackCommand.sendFeedback(payload);
    };
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Send feedback successfull');
    };
    sendResponse(await postRequest(validatePayload));
};

const getAllFeedback = async (req, res) => {
    const payload = req.query;
    const validatePayload = validator.isValidPayload(payload, queryModel.getAllFeedback);
    const postRequest = async (result) => {
        if (result.err) {
            return result;
        }
        return feedbackQuery.getAllFeedback(payload);
    };
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.paginationResponse(res, 'success', result, 'Get all feedback');
    };
    sendResponse(await postRequest(validatePayload));
};

module.exports = {
    getAllTopic,
    createTopic,
    sendFeedback,
    getAllFeedback
};
