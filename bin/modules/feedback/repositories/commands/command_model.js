const joi = require('@hapi/joi');

const createTopic = joi.object({
    topicName: joi.string().required()
});

const sendFeedback = joi.object({
    feedback: joi.string().required(),
    topic: joi.object({
        topicId: joi.string().required(),
        topicName: joi.string().required()
    }),
    user:joi.any()
});

module.exports = {
    createTopic,
    sendFeedback
};
