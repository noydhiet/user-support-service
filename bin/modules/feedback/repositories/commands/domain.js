/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 13/11/2019, 3:41 PM
 */

const uuid = require('uuid/v1');
const wrapper = require('../../../../helpers/utils/wrapper');
const {InternalServerError} = require('../../../../helpers/error');
const Command = require('./command');
const Query = require('../queries/query');

class Feeedback {

    constructor() {
        this.command = new Command();
        this.query = new Query();
    }

    async createTopic(payload) {
        const {topicName} = payload;
        const now = new Date().toISOString();
        const topicId = uuid();
        let topic = {
            topicId,
            topicName,
            createdAt: now,
            updatedAt: now
        };

        const result = await this.command.insertTopic(topic);
        if (result.err) {
            return wrapper.error(new InternalServerError('Create Topic Failed'));
        }
        return await wrapper.data(topic);
    }

    async sendFeedback(payload) {
        const now = new Date().toISOString();
        const feedbackId = uuid();
        let feedback = {
            feedbackId,
            topic: payload.topic,
            user: payload.user.email,
            messages: payload.feedback,
            createdAt: now,
            updatedAt: now
        };

        const result = await this.command.insertFeedback(feedback);
        if (result.err) {
            return wrapper.error(new InternalServerError('Send Feedback Failed'));
        }
        return await wrapper.data(feedback);
    }
}

module.exports = Feeedback;
