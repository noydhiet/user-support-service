/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 13/11/2019, 3:41 PM
 */

const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

class Command {

    constructor() {
        this.elastic = elastic;
    }

    async insertTopic(payload) {
        const data = {
            index: config.get('/elasticIndexes/topic'),
            type: '_doc',
            id: payload.topicId,
            body: payload
        };
        const result = this.elastic.insertData(config.get('/elasticsearch'), data);
        return result;
    }

    async insertFeedback(payload) {
        const data = {
            index: config.get('/elasticIndexes/feedback'),
            type: '_doc',
            id: payload.feedbackId,
            body: payload
        };
        const result = this.elastic.insertData(config.get('/elasticsearch'), data);
        return result;
    }
}

module.exports = Command;
