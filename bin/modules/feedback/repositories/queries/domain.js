/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 13/11/2019, 3:41 PM
 */

const Query = require('./query');
const model = require('./query_model');
const wrapper = require('../../../../helpers/utils/wrapper');
const { NotFoundError, BadRequestError } = require('../../../../helpers/error');
const validate = require('validate.js');

class Feedback {

    constructor() {
        this.query = new Query();
    }

    async getAllTopic() {
        const result = await this.query.getAllTopic();
        if (result.err) {
            return wrapper.error(new NotFoundError('Topic data not found'));
        }
        return wrapper.data(result.data);
    }

    async getAllFeedback(data) {
        let page = validate.isEmpty(data.page) ? 1 : data.page;
        let size = validate.isEmpty(data.size) ? 1 : data.size;
        if (page == 0) {
            return wrapper.error(new BadRequestError('Page not allow 0'));
        }
        page = size * (page - 1);
        let payload = {};
        if (data.search) {
            payload = {
                from: page,
                size: size,
                sort: { createdAt: 'asc' },
                query: {
                    bool: {
                        must: [
                            {
                                query_string: {
                                    fields: [
                                        'messages',
                                        'user.name'
                                    ],
                                    query: `*${data.search}*`,
                                    minimum_should_match: '100%'
                                }
                            }
                        ]
                    }
                }
            };
        } else {
            payload = {
                from: page,
                size: size,
                sort: { createdAt: 'asc' },
                query: {
                    match_all: {}
                }
            };
        }
        const result = await this.query.getAllFeedback(payload);
        if (result.err) {
            return wrapper.error(new NotFoundError('Feedback data not found'));
        }
        const metaData = model.metaModel();
        metaData.page = parseInt(data.page);
        metaData.count = result.data.response.length;
        metaData.totalPage = Math.ceil(result.data.total.value / size);
        metaData.totalData = result.data.total.value;

        return wrapper.paginationData(result.data.response, metaData);
    }
}

module.exports = Feedback;
