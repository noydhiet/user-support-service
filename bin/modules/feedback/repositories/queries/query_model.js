const joi = require('@hapi/joi');

const getAllFeedback = joi.object({
    page: joi.number().required(),
    size: joi.number().required(),
    search: joi.string().optional().allow('')
});

const metaModel = () => {
    const metaModel = {
        page: 0,
        count: 0,
        totalPage: 0,
        totalData: 0
    };
    return metaModel;
};

module.exports = {
    getAllFeedback,
    metaModel
};
