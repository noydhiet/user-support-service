/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 13/11/2019, 3:41 PM
 */

const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

class Query {

    constructor() {
        this.elastic = elastic;
    }

    async getAllTopic() {
        const query = {
            index: config.get('/elasticIndexes/topic'),
            type: '_doc',
            body: {
                size: 100,
                sort: [
                    {createdAt: 'desc'}
                ],
                query: {
                    match_all: {}
                }
            }
        };
        const result = this.elastic.findAll(config.get('/elasticsearch'), query);
        return result;
    }

    async getAllFeedback(param) {
        const query = {
            index: config.get('/elasticIndexes/feedback'),
            type: '_doc',
            body: param
        };
        const result = this.elastic.findAllPaging(config.get('/elasticsearch'), query);
        return result;
    }

}

module.exports = Query;
