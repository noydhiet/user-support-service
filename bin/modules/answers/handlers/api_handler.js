/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 13/11/2019, 3:40 PM
 */

const wrapper = require('../../../helpers/utils/wrapper');
const AnswerCommand = require('../repositories/commands/domain');
const AnswerQuery = require('../repositories/queries/domain');
const validator = require('../../../helpers/utils/validator');
const commandModel = require('../repositories/commands/command_model');

const createAnswer = async (req, res) => {
    const payload = req.body;
    const validatePayload = validator.isValidPayload(payload, commandModel.answer);
    const answerCommand = new AnswerCommand();
    const postRequest = async (result) => {
        if (result.err) {
            return result;
        }
        return answerCommand.createAnswer(payload);
    };
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Create answer successfull');
    };
    sendResponse(await postRequest(validatePayload));
};

const updateAnswer = async (req, res) => {
    const payload = {
        ...req.body,
        ...req.params
    };
    const validatePayload = validator.isValidPayload(payload, commandModel.updatedAnswer);
    const answerCommand = new AnswerCommand();
    const postRequest = async (result) => {
        if (result.err) {
            return result;
        }
        return answerCommand.updateAnswer(payload);
    };
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Create answer successfull');
    };
    sendResponse(await postRequest(validatePayload));
};

const getAnswerByQuestionId = async (req, res) => {
    const payload = req.params;
    const answerQuery = new AnswerQuery();
    const postRequest = async () => answerQuery.getAnswerByQuestionId(payload);
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Create elastic index successfull');
    };
    sendResponse(await postRequest());
};

const getAnswerByIssueId = async (req, res) => {
    const payload = req.params;
    const answerQuery = new AnswerQuery();
    const postRequest = async () => answerQuery.getAnswerByIssueId(payload);
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Create elastic index successfull');
    };
    sendResponse(await postRequest());
};

const deleteAnswer = async (req, res) => {
    const answerId = req.params.answerId;
    const answerCommand = new AnswerCommand();
    const postRequest = async () => answerCommand.deleteAnswer(answerId);
    const sendResponse = async (result) => {
        /* eslint no-unused-expressions: [2, { allowTernary: true }] */
        (result.err) ? wrapper.response(res, 'fail', result)
            : wrapper.response(res, 'success', result, 'Delete answer successfull');
    };
    sendResponse(await postRequest());
};

module.exports = {
    createAnswer,
    getAnswerByQuestionId,
    getAnswerByIssueId,
    updateAnswer,
    deleteAnswer
};
