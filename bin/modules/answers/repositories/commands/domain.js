/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 13/11/2019, 3:41 PM
 */

const uuid = require('uuid/v1');
const validate = require('validate.js');
const wrapper = require('../../../../helpers/utils/wrapper');
const {ConflictError, NotFoundError} = require('../../../../helpers/error');
const Command = require('./command');
const Query = require('../queries/query');
const IssueCommand = require('../../../issues/repositories/commands/command');
const IssueQuery = require('../../../issues/repositories/queries/query');
const QuestionCommand = require('../../../questions/repositories/commands/command');
const QuestionQuery = require('../../../questions/repositories/queries/query');

class Answer {

    constructor() {
        this.command = new Command();
        this.query = new Query();
        this.issueCommand = new IssueCommand();
        this.issueQuery = new IssueQuery();
        this.questionCommand = new QuestionCommand();
        this.questionQuery = new QuestionQuery();
    }

    async createAnswer(payload) {
        const {questionId, issueId, number, details, type} = payload;
        const now = new Date().toISOString();
        const answerId = uuid();
        let answer = {
            answerId,
            questionIds: [],
            issueIds: [],
            number,
            details,
            type,
            createdAt: now,
            updatedAt: now
        };

        let query = {
            bool: {
                must: [
                    {
                        match: {
                            number
                        }
                    }
                ]
            }
        };

        if (!validate.isEmpty(questionId)) {
            const updatedQuestion = await this.updateQuestion({questionId, answerId});
            if (updatedQuestion.err) {
                return wrapper.error(new ConflictError('Update Question Failed'));
            }
            answer.questionIds.push(questionId);
            query.bool.must.push({
                match: {
                    'questionIds.keyword': questionId
                }
            });
        }

        if (!validate.isEmpty(issueId)) {
            const updatedIssue = await this.updateIssue({issueId, answerId});
            if (updatedIssue.err) {
                this.deleteAnswerOnQuestion(questionId, answerId);
                return wrapper.error(new ConflictError('Update Issue Failed'));
            }
            answer.issueIds.push(issueId);
            query.bool.must.push({
                match: {
                    'issueIds.keyword': issueId
                }
            });
        }

        const isExistQuestion = await this.query.getAnswersByParam(query);
        if (!isExistQuestion.err) {
            this.deleteAnswerOnIssue(issueId, answerId);
            this.deleteAnswerOnQuestion(questionId, answerId);
            return wrapper.error(new ConflictError('question/issue already have an answer'));
        }

        const result = await this.command.insertAnswer(answer);
        if (result.err) {
            this.deleteAnswerOnIssue(issueId, answerId);
            this.deleteAnswerOnQuestion(questionId, answerId);
            return wrapper.error(new ConflictError('Create Answer Failed'));
        }

        return await result;
    }

    async updateQuestion(payload) {
        const {questionId, answerId} = payload;
        const question = await this.questionQuery.getQuestionByParam({'questionId.keyword': questionId});
        if (question.err) {
            return wrapper.error(new NotFoundError('Question not found'));
        }

        let answerIds = question.data.answerIds;
        if (answerIds.findIndex(a => a === answerId) === -1) {
            answerIds.push(answerId);
        }
        const result = await this.questionCommand.updateQuestion({questionId, answerIds});
        if (result.err) {
            return wrapper.error(new ConflictError('Update question failed'));
        }

        return wrapper.data('Update question successfull');
    }

    async deleteAnswerOnQuestion(questionId, answerId) {
        const question = await this.questionQuery.getQuestionByParam({'questionId.keyword': questionId});
        if (question.err) {
            return wrapper.error(new NotFoundError('Question not found'));
        }
        let answerIds = question.data.answerIds;
        answerIds = answerIds.filter(item => item !== answerId);
        const result = await this.questionCommand.updateQuestion({questionId, answerIds});
        if (result.err) {
            return wrapper.error(new ConflictError('Update question failed'));
        }
    }

    async updateIssue(payload) {
        const {issueId, answerId} = payload;
        const issue = await this.issueQuery.getIssueByParam({'issueId.keyword': issueId});
        if (issue.err) {
            return wrapper.error(new NotFoundError('Issue not found'));
        }

        let answerIds = issue.data.answerIds;
        if (answerIds.findIndex(a => a === answerId) === -1) {
            answerIds.push(answerId);
        }
        const result = await this.issueCommand.updateIssue({issueId, answerIds});
        if (result.err) {
            return wrapper.error(new ConflictError('Update issue failed'));
        }

        return wrapper.data('Update issue successfull');
    }

    async deleteAnswerOnIssue(issueId, answerId) {
        const issue = await this.issueQuery.getIssueByParam({'issueId.keyword': issueId});
        if (issue.err) {
            return wrapper.error(new NotFoundError('Issue not found'));
        }
        let answerIds = issue.data.answerIds;
        answerIds = answerIds.filter(item => item !== answerId);
        const result = await this.issueCommand.updateIssue({issueId, answerIds});
        if (result.err) {
            return wrapper.error(new ConflictError('Update issue failed'));
        }
    }

    async updateAnswer(payload) {
        const {answerId, number, details, title, type} = payload;
        const query = {
            match: {
                'answerId.keyword': answerId
            }
        };
        const answer = await this.query.getAnswersByParam(query);
        if(answer.err) {
            return wrapper.error(new NotFoundError('Answer not found'));
        }

        const data = answer.data;
        // data.questionIds = validate.isEmpty(questionIds) ? data.questionIds : questionIds;
        // data.issueIds = validate.isEmpty(issueIds) ? data.issueIds : issueIds;
        data.number = validate.isEmpty(number) ? data.number : number;
        data.details = validate.isEmpty(details) ? data.details : details;
        data.title = validate.isEmpty(title) ? data.title : title;
        data.type = validate.isEmpty(type) ? data.type : type;
        data.updatedAt = new Date().toISOString();

        let result = await this.command.updateAnswer(data);
        if (result.err) {
            return wrapper.error(new ConflictError('Update issue Failed'));
        }
        return await wrapper.data(result.data);
    }

    async deleteAnswer(answerId) {
        const query = {
            match: {
                'answerId.keyword': answerId
            }
        };
        const answer = await this.query.getAnswersByParam(query);
        if(answer.err) {
            return wrapper.error(new NotFoundError('Answer not found'));
        }

        const result = await this.command.deleteAnswer(answerId);
        if(result.err) {
            return wrapper.error(new ConflictError('Delete answer failed'));
        }

        answer.data.issueIds.map((issueId) => {
            this.deleteAnswerOnIssue(issueId, answerId);
        });

        answer.data.questionIds.map((questionId) => {
            this.deleteAnswerOnQuestion(questionId, answerId);
        });

        return result;
    }
}

module.exports = Answer;
