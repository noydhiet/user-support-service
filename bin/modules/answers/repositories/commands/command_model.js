/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 13/11/2019, 4:08 PM
 */

const joi = require('@hapi/joi');

const INFORMATION = 'INFORMATION';
const TROUBLESHOOTING = 'TROUBLESHOOTING';

const detail = joi.object({
    step: joi.number().integer().strict().required(),
    text: joi.string().required(),
    url: joi.string().allow('').required()
});

const answer = joi.object({
    questionId: joi.string().optional(),
    issueId: joi.string().optional(),
    title: joi.string().optional(),
    type: joi.string().valid(INFORMATION, TROUBLESHOOTING).optional(),
    number: joi.number().integer().strict().required(),
    details: joi.array().items(detail).required()
});

const updatedAnswer = joi.object({
    answerId: joi.string().required(),
    title: joi.string().optional(),
    type: joi.string().valid(INFORMATION, TROUBLESHOOTING).optional(),
    number: joi.number().integer().strict().optional(),
    details: joi.array().items(detail).optional()
});

module.exports = {
    answer,
    updatedAnswer
};
