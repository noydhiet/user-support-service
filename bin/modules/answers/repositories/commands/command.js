/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 13/11/2019, 3:41 PM
 */

const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

class Command {

    constructor() {
        this.elastic = elastic;
    }

    async insertAnswer(payload) {
        const data = {
            index: config.get('/elasticIndexes/answer'),
            type: '_doc',
            id: payload.answerId,
            body: payload
        };
        const result = this.elastic.insertData(config.get('/elasticsearch'), data);
        return result;
    }

    async deleteAnswer(id) {
        const data = {
            index: config.get('/elasticIndexes/answer'),
            type: '_doc',
            id
        };
        const result = this.elastic.deleteData(config.get('/elasticsearch'), data);
        return result;
    }

    async updateAnswer(payload) {
        const { answerId, ...data } = payload;
        const answer = {
            index: config.get('/elasticIndexes/answer'),
            type: '_doc',
            id: answerId,
            body: {
                doc: data
            }
        };
        const result = this.elastic.updateData(config.get('/elasticsearch'), answer);
        return result;
    }


}

module.exports = Command;
