/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 13/11/2019, 3:41 PM
 */

const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

class Query {

    constructor() {
        this.elastic = elastic;
    }

    async getAnswersByParam(param) {
        const query = {
            index: config.get('/elasticIndexes/answer'),
            type: '_doc',
            body: {
                query: param
            }
        };
        const result = this.elastic.findOne(config.get('/elasticsearch'), query);
        return result;
    }

}

module.exports = Query;
