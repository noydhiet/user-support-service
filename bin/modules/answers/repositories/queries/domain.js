/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 13/11/2019, 3:41 PM
 */

const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const {NotFoundError, ConflictError} = require('../../../../helpers/error');
const QuestionCommand = require('../../../questions/repositories/commands/command');
const QuestionQuery = require('../../../questions/repositories/queries/query');

class Question {

    constructor() {
        this.query = new Query();
        this.questionCommand = new QuestionCommand();
        this.questionQuery = new QuestionQuery();
    }

    async getAnswerByQuestionId(payload) {
        const { questionId, answerNumber } = payload;
        const query = {
            bool: {
                must: [
                    {
                        match: {
                            'questionIds.keyword': questionId
                        }
                    },
                    {
                        match: {
                            'number': parseInt(answerNumber)
                        }
                    }
                ]
            }
        };

        const result = await this.query.getAnswersByParam(query);
        if (result.err) {
            return wrapper.error(new NotFoundError('Answer data not found'));
        }

        const question = await this.questionQuery.getQuestionByParam({'questionId.keyword': questionId});
        if (question.err) {
            return wrapper.error(new NotFoundError('Question not found'));
        }

        let counter = question.data.counter;
        const questionObj = {
            questionId,
            counter: counter + 1
        };
        const updatedQuestion = await this.questionCommand.updateQuestion(questionObj);
        if (updatedQuestion.err) {
            return wrapper.error(new ConflictError('Update question failed'));
        }

        const {number, details, title, type} = result.data;
        return wrapper.data({number, details, title, type});

    }

    async getAnswerByIssueId(payload) {
        const { issueId, answerNumber } = payload;
        const query = {
            bool: {
                must: [
                    {
                        match: {
                            'issueIds.keyword': issueId
                        }
                    },
                    {
                        match: {
                            'number': parseInt(answerNumber)
                        }
                    }
                ]
            }
        };

        const result = await this.query.getAnswersByParam(query);
        if (result.err) {
            return wrapper.error(new NotFoundError('Answer data not found'));
        }

        const {number, details, title, type} = result.data;
        return wrapper.data({number, details, title, type});

    }
}

module.exports = Question;
