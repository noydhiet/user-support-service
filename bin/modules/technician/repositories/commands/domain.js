/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 15/11/2019, 3:09 PM
 */

const wrapper = require('../../../../helpers/utils/wrapper');
const {ConflictError, ForbiddenError} = require('../../../../helpers/error');
const Command = require('./command');
const moment = require('moment');
const {ServiceUnavailableError} = require('../../../../helpers/error');

const {TECHNICIAN} = require('../../../../config/const/commonConstants');
const {ON_THE_WAY, IN_PROGRESS, COMPLETED} = require('../../../../config/const/technicianStatus');

const {
    SUBMITTED: SERVICE_SUBMITTED,
    IN_PROGRESS: SERVICE_IN_PROGRESS,
    COMPLETED: SERVICE_COMPLETED,
    RESOLVED: SERVICE_RESOLVED
} = require('../../../../config/const/fisikTicketStatus');

function findStatus(status) {
    switch (status) {
        case ON_THE_WAY.status: {
            return ON_THE_WAY;
        }
        case IN_PROGRESS.status: {
            return IN_PROGRESS;
        }
        case COMPLETED.status: {
            return COMPLETED;
        }
        default: {
            return null;
        }
    }
}

function findServiceStatus(status) {
    switch (status) {
    case SERVICE_SUBMITTED.status: {
        return SERVICE_SUBMITTED;
    }
    case SERVICE_IN_PROGRESS.status: {
        return SERVICE_IN_PROGRESS;
    }
    case SERVICE_COMPLETED.status: {
        return SERVICE_COMPLETED;
    }
    case SERVICE_RESOLVED.status: {
        return SERVICE_RESOLVED;
    }
    default: {
        return null;
    }
    }
}

/**
 * ES Support ticket
 */
class InstallationBooking {

    constructor() {
        this.command = new Command();
    }

    async addBooking(booking) {
        let result = await this.command.addBooking(booking);
        if (result.err) {
            throw new ForbiddenError('Create Booking Failed');
        }
        return wrapper.data(result.data);
    }

    async addBookingConfirmation(booking) {
        let result = await this.command.addBookingConfirmation(booking);
        if (result.err) {
            throw new ForbiddenError('Create Booking Confirmation Failed');
        }
        return wrapper.data(result.data);
    }

    async addServiceBooking(booking) {
        let result = await this.command.addServiceBooking(booking);
        if (result.err) {
            throw new ForbiddenError('Create Booking Failed');
        }
        return wrapper.data(result.data);
    }

    async updateInstallationBookingStatus(transactionId, status, technician) {
        let bookingResult = {};
        let booking = {};
        if (transactionId) {

            bookingResult = await this.findBookingByTransactionId(transactionId);
            if (bookingResult.data && bookingResult.data.length === 1) {
                booking = bookingResult.data[0];
            } else {
                throw new ForbiddenError('no unique bookings found');
            }
        }
        if (bookingResult.err) {
            throw new ConflictError('Appointment Find Error');
        } else {

            if (technician) {
                booking['technician'] = technician;
            }
            let statusDescription = findStatus(status);
            booking['status'] = status;
            booking['currentStatus'] = statusDescription;
            let now = new Date().toISOString();
            booking['updatedTime'] = now;
            let statusUpdates = new Set(booking['statusUpdates']);
            statusUpdates.add({
                ...statusDescription,
                updated: now
            });
            booking['statusUpdates'] = Array.from(statusUpdates);

            let result = await this.command.updateBooking(booking);
            if (result.err) {
                throw new ConflictError('Update Booking Failed');
            }
            return wrapper.data(result.data);
        }
    }

    async updateBookingsOrderNumber(transactionId, orderNumber) {

        let bookingResult = await this.findBookingByTransactionId(transactionId);

        if (bookingResult.data && bookingResult.data.length === 1) {
            let booking = bookingResult.data[0];
            booking['orderNumber'] = orderNumber;
            booking['updatedTime'] = new Date().toISOString();

            let result = await this.command.updateBooking(booking);
            if (result.err) {
                throw new ConflictError('Update Booking Failed');
            }
            return wrapper.data(result.data);
        }
        //TODO: preconditions not met
        throw new ForbiddenError(`no unique bookings found for: ${JSON.stringify({transactionId, orderNumber})}`);
    }

    async updateServiceBookingStatus(bookingId, userId, status) {
        let bookingResult;
        let booking = {};
        if (userId) {
            bookingResult = await this.findServiceBookingExact(bookingId, userId);
            booking = bookingResult.data;
        } else {
            bookingResult = await this.findServiceBookingByBookingId(bookingId);
            if (bookingResult.data && bookingResult.data.length === 1) {
                booking = bookingResult.data[0];
            } else {
                throw new ForbiddenError('no unique bookings found');
            }
        }
        if (bookingResult.err) {
            throw new ConflictError('Appointment Find Error');
        } else {
            let statusDescription = findServiceStatus(status);
            booking['status'] = status;
            booking['currentStatus'] = statusDescription;
            let now = new Date().toISOString();
            booking['updatedTime'] = now;
            let statusUpdates = new Set(booking['statusUpdates']);
            statusUpdates.add({
                ...statusDescription,
                updated: now
            });
            booking['statusUpdates'] = Array.from(statusUpdates);

            let result = await this.command.updateServiceBooking(booking);
            if (result.err) {
                throw new ConflictError('Update Booking Failed');
            }
            return wrapper.data(result.data);
        }
    }

    async closeBooking(bookingId, userId, closed) {
        const bookingResult = await this.findBookingExact(bookingId, userId);
        if (bookingResult.err) {
            throw new ConflictError('Appointment Find Error');
        }
        if (bookingResult.data) {
            let booking = bookingResult.data;
            booking['closed'] = closed;

            let result = await this.command.updateBooking(booking);
            if (result.err) {
                throw new ConflictError('close Booking in system Failed');
            }
            return wrapper.data(result.data);
        }
        throw new ConflictError(`No matching booking found. ${JSON.stringify({bookingId, userId, closed})}`);
    }


    async updateBooking(booking) {
        let result = await this.command.updateBooking(booking);
        if (result.err) {
            throw new ConflictError('update Booking in system Failed' + JSON.stringify(result.err));
        }
        return wrapper.data(result.data);
    }

    async rescheduleBooking(bookingId, userId, status, newSlot, scheduledTime) {
        const bookings = await this.findBookingExact(bookingId, userId);
        if (bookings.err) {
            return wrapper.error(new ConflictError('Appointment Find Error'));
        }
        if (bookings.data) {
            let newBooking = bookings.data;
            const now = new Date().toISOString();
            newBooking.status = status;
            newBooking.updatedTime = now;
            newBooking.timeSlot = newSlot;
            newBooking.scheduledTime = scheduledTime;
            let statusUpdates = newBooking['statusUpdates'] ? newBooking['statusUpdates'] : [];
            statusUpdates.push({
                status,
                action: TECHNICIAN.APPOINTMENT.STATUS.RESCHEDULED,
                updated: now
            });
            newBooking['statusUpdates'] = statusUpdates;
            let result = await this.command.updateBooking(newBooking);
            if (result.err) {
                return wrapper.error(new ConflictError('Update Booking Failed'));
            }
            return wrapper.data(result.data);
        }
        throw new ConflictError('multiple bookings with same booking id');
    }

    async findBookingByBookingId(bookingId) {
        let result = await this.command.findBooking({'bookingId.keyword': bookingId});
        if (result.err) {
            return wrapper.error(new ConflictError('Appointment Find Error'));
        }
        return wrapper.data(result.data);
    }

    async findBookingByTransactionId(transactionId) {
        let result = await this.command.findBookingExact([
            {match: {'transactionId.keyword': transactionId}},
            {match: {closed: false}}
        ]);
        if (result.err) {
            return wrapper.error(new ConflictError('Appointment Find Error'));
        }
        return wrapper.data(result.data);
    }

    async findServiceBookingByBookingId(bookingId) {
        let result = await this.command.findServiceBooking({bookingId});
        if (result.err) {
            return wrapper.error(new ConflictError('Appointment Find Error'));
        }
        return wrapper.data(result.data);
    }

    async findBookingByUserId(userId, closed = false) {
        let result = await this.command.findBookingExact([{match: {userId}}, {match: {closed}}]);
        if (result.err) {
            throw new ServiceUnavailableError(`Appointment Find Error: ${result.err.message}`);
        }
        return wrapper.data(result.data);
    }

    async findServiceBookingByUserId(userId) {
        let result = await this.command.findServiceBooking({userId});
        if (result.err) {
            throw new ServiceUnavailableError(`Appointment Find Error: ${result.err.message}`);
        }
        return wrapper.data(result.data);
    }

    async findBookingExact(bookingId, userId) {
        let result = await this.command.findBookingExact([{match: {'bookingId.keyword': bookingId}}, {match: {userId}}, {match: {closed: false}}]);
        if (result.err) {
            throw new ServiceUnavailableError(`Appointment Find Error: ${result.err.message}`);
        }
        return wrapper.data(result.data[0]);
    }

    async findBookingExactByTransactionId(transactionId, userId) {
        let result = await this.command.findBookingExact([
            {match: {'transactionId.keyword': transactionId}},
            {match: {userId}}, {match: {closed: false}}
        ]);
        if (result.err) {
            throw new ServiceUnavailableError(`Appointment Find Error: ${result.err.message}`);
        }
        return wrapper.data(result.data[0]);
    }

    async findServiceBookingExact(bookingId, userId) {
        let result = await this.command.findServiceBookingExact([{match: {'bookingId.keyword': bookingId}}, {match: {userId}}]);
        if (result.err) {
            return wrapper.error(new ConflictError('Appointment Find Error'));
        }
        return wrapper.data(result.data);
    }


    async addAvailableSlots(payload) {
        let result = await this.command.addAvailableSlots(payload);
        if (result.err) {
            return wrapper.error(new ConflictError('Create Slot Failed'));
        }
        return wrapper.data(result.data);
    }

    async addAvailableServiceSlots(payload) {
        let result = await this.command.addAvailableServiceSlots(payload);
        if (result.err) {
            return wrapper.error(new ConflictError('Create Slot Failed'));
        }
        return wrapper.data(result.data);
    }

    async findSlotByBookingId(bookingId) {
        const query = {
            match: {
                'bookingId.keyword': bookingId
            }
        };

        let result = await this.command.findSlot(query);
        if (result.err) {
            //fixme: throw new ServiceUnavailableError(`Slot Find Error: ${result.err.message}`);
            return wrapper.error(new ConflictError('Slot Find Error'));
        }
        return wrapper.data(result.data);
    }

    async findServiceSlotByBookingId(bookingId) {
        const query = {
            match: {
                'bookingId.keyword': bookingId
            }
        };

        let result = await this.command.findServiceSlot(query);
        if (result.err) {
            //fixme: throw new ServiceUnavailableError(`Slot Find Error: ${result.err.message}`);
            return wrapper.error(new ConflictError('Slot Find Error'));
        }
        return wrapper.data(result.data);
    }

    async findEarlySlots(date) {
        const query = {
            'range': {
                'timeSlot.date': {
                    'gt': moment(date, 'DD-MM-YYYY').subtract(1, 'day').format('DD-MM-YYYY'),
                    'lt': date,
                    'format': 'dd-MM-yyyy'
                }
            }
        };
        let result = await this.command.findSlots(query);
        if (result.err) {
            //fixme: throw new ServiceUnavailableError(`Slot Find Error: ${result.err.message}`);
            return wrapper.error(new ConflictError('Slot Find Error'));
        }
        return wrapper.data(result.data);
    }

    async getRescheduleAttempts(userId) {
        const query = {
            bool: {
                must: [
                    {
                        match: {
                            'userId': userId
                        }
                    }
                ],
            }
        };
        const result = await this.command.findRescheduleAttempts(query);
        if (result.err) {
            return wrapper.error(new ConflictError('Reschedule Attempts Find Error'));
        }
        return wrapper.data(result.data);
    }

    async upsertRescheduleAttempts(attempts) {
        const result = await this.command.upsertInstallationAttempts(attempts);
        if (result.err) {
            return wrapper.error(new ConflictError('Update attempts Failed'));
        }
        return wrapper.data(result.data);
    }

    async getBookings(time){
        const query = {
            bool: {
                must_not: [
                    {
                        match: {
                            'currentStatus.status': 'COMPLETED'
                        }
                    },
                    {
                        match: {
                            'currentStatus.status': 'ON_THE_WAY'
                        }
                    },
                    {
                        match: {
                            'closed': true
                        }
                    }],
                filter: [{
                    range: {
                        scheduledTime: {
                            gte: 'now',
                            lt: time
                        }
                    }
                }]
            },
        };

        const result = await this.command.getBookings(query);
        if (result.err) {
            return wrapper.error(new ConflictError('Get bookings Failed'));
        }
        return wrapper.data(result.data);
    }

    async getSolvedTickets(time){
        const query = {
            bool: {
                must: [
                    {
                        match: {
                            'ticketStatus.status': 'SOLVED'
                        }
                    },
                    {
                        match: {
                            'open': true
                        }
                    },
                    {
                        match: {
                            'categoryType': 'FISIK'
                        }
                    }
                ],
                filter: [{
                    range: {
                        updatedAt: {
                            gte: 'now',
                            lt: time
                        }
                    }
                }]
            },
        };

        const result = await this.command.getTickets(query);
        if (result.err) {
            return wrapper.error(new ConflictError('Get tickets Failed'));
        }
        return wrapper.data(result.data);
    }

    async getSubmittedTicketsForTime(time){
        const query = {
            bool: {
                must: [
                    {
                        match: {
                            'ticketStatus.status': 'SUBMITTED'
                        }
                    },
                    {
                        match: {
                            'open': true
                        }
                    },
                    {
                        match: {
                            'categoryType': 'FISIK'
                        }
                    }
                ],
                filter: [{
                    range: {
                        updatedAt: {
                            gte: 'now',
                            lt: time
                        }
                    }
                }]
            },
        };

        const result = await this.command.getTickets(query);
        if (result.err) {
            return wrapper.error(new ConflictError('Get tickets Failed'));
        }
        return wrapper.data(result.data);
    }
}

module.exports = InstallationBooking;
