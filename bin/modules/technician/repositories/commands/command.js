/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 15/11/2019, 3:09 PM
 */

const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

/**
 * ES functions for technician schedules
 */
class InstallationTicket {

    constructor() {
        this.elastic = elastic;
    }

    async addBooking(booking) {
        const data = {
            index: config.get('/elasticIndexes/installationBooking'),
            type: '_doc',
            id: booking.transactionId,
            body: booking
        };
        return this.elastic.insertData(config.get('/elasticsearch'), data);
    }

    async addBookingConfirmation(booking) {
        const data = {
            index: config.get('/elasticIndexes/bookingConfirmation'),
            type: '_doc',
            id: booking.data.external.trackId,
            body: booking
        };
        return this.elastic.insertData(config.get('/elasticsearch'), data);
    }

    async addServiceBooking(booking) {
        const data = {
            index: config.get('/elasticIndexes/serviceBooking'),
            type: '_doc',
            id: booking.transactionId,
            body: booking
        };
        return this.elastic.insertData(config.get('/elasticsearch'), data);
    }

    async updateBooking(booking) {
        const data = {
            index: config.get('/elasticIndexes/installationBooking'),
            type: '_doc',
            id: booking.transactionId,
            body: {
                doc: booking
            }
        };
        return this.elastic.updateData(config.get('/elasticsearch'), data);
    }

    async updateServiceBooking(booking) {
        const data = {
            index: config.get('/elasticIndexes/serviceBooking'),
            type: '_doc',
            id: booking.transactionId,
            body: {
                doc: booking
            }
        };
        return this.elastic.updateData(config.get('/elasticsearch'), data);
    }

    async findBooking(match) {
        const query = {
            index: config.get('/elasticIndexes/installationBooking'),
            type: '_doc',
            body: {
                query: {match}
            }
        };
        return this.elastic.findAll(config.get('/elasticsearch'), query);
    }

    async findServiceBooking(match) {
        const query = {
            index: config.get('/elasticIndexes/serviceBooking'),
            type: '_doc',
            body: {
                query: {match}
            }
        };
        return this.elastic.findAll(config.get('/elasticsearch'), query);
    }

    /**
     *
     * @param must {[]}
     * @returns {Promise<unknown>}
     */
    async findBookingExact(must) {
        const query = {
            index: config.get('/elasticIndexes/installationBooking'),
            type: '_doc',
            body: {
                query: {
                    bool: {
                        must
                    }
                }
            }
        };
        return this.elastic.findAll(config.get('/elasticsearch'), query);
    }

    /**
     *
     * @param must {[]}
     * @returns {Promise<unknown>}
     */
    async findServiceBookingExact(must) {
        const query = {
            index: config.get('/elasticIndexes/serviceBooking'),
            type: '_doc',
            body: {
                query: {
                    bool: {
                        must
                    }
                }
            }
        };
        return this.elastic.findOne(config.get('/elasticsearch'), query);
    }

    async removeBooking(bookingId) {
        const query = {
            index: config.get('/elasticIndexes/installationBooking'),
            type: '_doc',
            body: {
                query: {
                    match: {bookingId}
                }
            }
        };
        return this.elastic.deleteData(config.get('/elasticsearch'), query);
    }

    /**
     * Find an installation booking slot
     * @param {Object} match - match query
     * @returns {Promise<unknown>}
     */
    async findSlot(match) {
        const query = {
            index: config.get('/elasticIndexes/installationSlot'),
            type: '_doc',
            body: {
                query: match
            }
        };
        return this.elastic.findOne(config.get('/elasticsearch'), query);
    }

    /**
     * Find an installation booking slot
     * @param {Object} match - match query
     * @returns {Promise<unknown>}
     */
    async findServiceSlot(match) {
        const query = {
            index: config.get('/elasticIndexes/serviceSlot'),
            type: '_doc',
            body: {
                query: match
            }
        };
        return this.elastic.findOne(config.get('/elasticsearch'), query);
    }

    /**
     * Find all installation booking slots
     * @param match - match query
     * @returns {Promise<unknown>}
     */
    async findSlots(match) {
        const query = {
            index: config.get('/elasticIndexes/installationSlot'),
            type: '_doc',
            body: {
                from: 0,
                size: config.get('/elasticsearch/size'),
                query: match
            }
        };
        return this.elastic.findAll(config.get('/elasticsearch'), query);
    }

    /**
     * Insert available slots
     * @param {Object} payload - data of slots
     * @returns {Promise<unknown>}
     */
    async addAvailableSlots(payload) {
        const index = config.get('/elasticIndexes/installationSlot');
        const type = '_doc';
        let request = '';

        payload.forEach((doc, idx) => {
            request = request
                .concat(JSON.stringify({index: {'_index': index, '_type': type, '_id': doc.bookingId}}))
                .concat('\n').concat(JSON.stringify(doc));
            if (idx < payload.length - 1) {
                request = request.concat('\n');
            }
        });

        return this.elastic.insertMany(config.get('/elasticsearch'), {body: request});
    }

    /**
     * Insert available Service slots
     * @param {Object} payload - data of slots
     * @returns {Promise<unknown>}
     */
    async addAvailableServiceSlots(payload) {
        const index = config.get('/elasticIndexes/serviceSlot');
        const type = '_doc';
        let request = '';

        payload.forEach((doc, idx) => {
            request = request
                .concat(JSON.stringify({index: {'_index': index, '_type': type, '_id': doc.bookingId}}))
                .concat('\n').concat(JSON.stringify(doc));
            if (idx < payload.length - 1) {
                request = request.concat('\n');
            }
        });

        return this.elastic.insertMany(config.get('/elasticsearch'), {body: request});
    }

    /**
     * Find reschedule attempts
     * @param match - match query
     * @returns {Promise<unknown>}
     */
    async findRescheduleAttempts(match) {
        const query = {
            index: config.get('/elasticIndexes/installationRescheduleAttempts'),
            type: '_doc',
            body: {
                query: match
            }
        };
        return this.elastic.findOne(config.get('/elasticsearch'), query);
    }

    /**
     * Update or insert Installation attempts
     * @param attempts
     * @returns {Promise<unknown>}
     */
    async upsertInstallationAttempts(attempts) {
        const data = {
            index: config.get('/elasticIndexes/installationRescheduleAttempts'),
            type: '_doc',
            id: attempts.userId,
            body: attempts
        };
        return this.elastic.insertData(config.get('/elasticsearch'), data);
    }

    /**
     * Get Bookings within next given minutes
     * @returns {Promise<unknown>}
     */
    async getBookings(query) {
        const data = {
            index: config.get('/elasticIndexes/installationBooking'),
            type: '_doc',
            body: {
                query: query,
                _source:{
                    excludes: ['statusUpdates']
                }
            }
        };
        return this.elastic.findAll(config.get('/elasticsearch'), data);
    }

    /**
     * Get tickets within next given minutes
     * @returns {Promise<unknown>}
     */
    async getTickets(query) {
        const data = {
            index: config.get('/elasticIndexes/tickets'),
            type: '_doc',
            body: {
                query: query
            }
        };
        return this.elastic.findAll(config.get('/elasticsearch'), data);
    }
}

module.exports = InstallationTicket;
