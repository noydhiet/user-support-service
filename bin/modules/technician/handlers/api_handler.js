/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Dulaj Pathirana on 15/11/2019, 3:07 PM
 */

const BookingCommand = require('../repositories/commands/domain');
const {BadRequestError} = require('../../../helpers/error');
const wrapper = require('../../../helpers/utils/wrapper');

/**
 * Create booking on es
 * @param {Object} booking - booking entity
 * @returns {Promise<*>}
 */
module.exports.reserveBooking = async (booking) => {
    return new BookingCommand().addBooking(booking);
};

module.exports.confirmBooking = async (confirmation) => {
    return new BookingCommand().addBookingConfirmation(confirmation);
};

/**
 * Create booking on es
 * @param {Object} booking - booking entity
 * @returns {Promise<*>}
 */
module.exports.reserveServiceBooking = async (booking) => {
    return new BookingCommand().addServiceBooking(booking);
};

/**
 * update booking status on ES
 *
 * @param transactionId
 * @param status
 * @param technician
 * @returns {Promise<{err, data}>}
 */
module.exports.updateInstallationBookingStatus = async (transactionId, status, technician) => {
    if (transactionId && status) {
        return new BookingCommand().updateInstallationBookingStatus(transactionId, status, technician);
    }
    throw new BadRequestError('transaction id or status is invalid');
};

/**
 * update booking status on ES
 *
 * @param transactionId
 * @param orderNumber
 * @returns {Promise<{err, data}>}
 */
module.exports.updateBookingsOrderNumber = async (transactionId, orderNumber) => {
    if (transactionId && orderNumber) {
        return new BookingCommand().updateBookingsOrderNumber(transactionId, orderNumber);
    }
    return new BadRequestError('transaction id or status is invalid');
};

/**
 * update service booking status on ES
 * @deprecated
 *
 * @param bookingId
 * @param userId
 * @param status
 * @returns {Promise<{err, data}>}
 */
module.exports.updateServiceBookingStatus = async (bookingId, userId, status) => {
    if (status) {
        return new BookingCommand().updateServiceBookingStatus(bookingId, userId, status);
    }
    throw new BadRequestError();
};

/**
 * Update theBooking
 * @param {Object} booking - booking data
 * @returns {Promise<{err, data}|undefined>}
 */
module.exports.updateBooking = async (booking) => {
    return new BookingCommand().updateBooking(booking);
};

/**
 * Update the Close status of Booking
 * @param bookingId
 * @param userId
 * @param closed
 * @returns {Promise<{err, data}|undefined>}
 */
module.exports.closeBooking = async (bookingId, userId, closed) => {
    return new BookingCommand().closeBooking(bookingId, userId, closed);
};

/**
 * update booking status on ES
 *
 * @param bookingId
 * @param userId
 * @param status
 * @param schedule
 * @param scheduledTime
 * @returns {Promise<{err, data}>}
 */
module.exports.rescheduleBooking = async (bookingId, userId, status, schedule, scheduledTime) => {
    if (status) {
        return new BookingCommand().rescheduleBooking(bookingId, userId, status, schedule, scheduledTime);
    }
    return new BadRequestError();
};

/**
 * findBookingByBookingId on ES
 * @param bookingId
 * @returns {Promise<{err, data}>}
 */
module.exports.findBookingByBookingId = async bookingId => {
    return new BookingCommand().findBookingByBookingId(bookingId);
};

/**
 * findBookingByTransactionId on ES
 * @param transactionId
 * @returns {Promise<{err, data}>}
 */
module.exports.findBookingByTransactionId = async transactionId => {
    return new BookingCommand().findBookingByTransactionId(transactionId);
};

/**
 * @param bookingId
 * @returns {Promise<{err, data}>}
 */
module.exports.findServiceBookingByBookingId = async bookingId => {
    return new BookingCommand().findServiceBookingByBookingId(bookingId);
};

/**
 * find the exact booking
 * @param bookingId
 * @param userId
 * @returns {Promise<{err, data}>}
 */
module.exports.findBookingExact = async (bookingId, userId) => {
    return new BookingCommand().findBookingExact(bookingId, userId);
};

/**
 * find the exact booking
 * @param bookingId
 * @param userId
 * @returns {Promise<{err, data}>}
 */
module.exports.findServiceBookingExact = async (bookingId, userId) => {
    return new BookingCommand().findServiceBookingExact(bookingId, userId);
};

/**
 * findBookingByUserId on ES
 * @param userId
 * @param closed
 * @returns {Promise<{err, data}>}
 */
module.exports.findBookingByUserId = async (userId, closed = false) => {
    return new BookingCommand().findBookingByUserId(userId, closed);
};

/**
 * findBookingByUserId on ES
 * @param userId
 * @returns {Promise<{err, data}>}
 */
module.exports.findServiceBookingByUserId = async userId => {
    return new BookingCommand().findServiceBookingByUserId(userId);
};


module.exports.getAppointmentStatus = async (userId, bookingId) => {
    if (userId && bookingId) {
        let booking = await new BookingCommand().findBookingExact(bookingId, userId);
        if (!booking.err && booking.data) {
            return wrapper.data([booking.data]);
        }
    } else if (userId) {
        let bookings = await new BookingCommand().findBookingByUserId(userId, false);
        if (!bookings.err && bookings.data) {
            return wrapper.data(bookings.data);
        }
    } else if (bookingId) {
        return new BookingCommand().findBookingByBookingId(bookingId);
    }
    return wrapper.data(null);
};

module.exports.getServiceAppointmentStatus = async (userId, bookingId) => {
    if (userId && bookingId) {
        let booking = await new BookingCommand().findServiceBookingExact(bookingId, userId);
        if (!booking.err && booking.data) {
            return wrapper.data([booking.data]);
        }
    } else if (userId) {
        let bookings = await new BookingCommand().findServiceBookingByUserId(userId);
        if (!bookings.err && bookings.data) {
            return wrapper.data(bookings.data.filter(booking => !booking['closed']));
        }
    } else if (bookingId) {
        return new BookingCommand().findServiceBookingByBookingId(bookingId);
    }
    return wrapper.data(null);
};

/**
 * Insert available slots as a bulk
 * @param {Object} slots - slots data
 * @returns {Promise<{err, data}>}
 */
module.exports.addAvailableSlots = async (slots) => {
    return new BookingCommand().addAvailableSlots(slots);
};

/**
 * Insert available service slots as a bulk
 * @param {Object} slots - slots data
 * @returns {Promise<{err, data}>}
 */
module.exports.addAvailableServiceSlots = async (slots) => {
    return new BookingCommand().addAvailableServiceSlots(slots);
};

/**
 * Find slot by Booking Id
 * @param {String} bookingId - booking Id
 * @returns {Promise<{err, data}>}
 */
module.exports.findSlotByBookingId = async bookingId => {
    return new BookingCommand().findSlotByBookingId(bookingId);
};

/**
 * Find Service slot by Booking Id
 * @param {String} bookingId - booking Id
 * @returns {Promise<{err, data}>}
 */
module.exports.findServiceSlotByBookingId = async bookingId => {
    return new BookingCommand().findServiceSlotByBookingId(bookingId);
};

/**
 * Find early slots by date
 * @param {Date} date - date to find new bookings slots
 * @returns {Promise<{err, data}>}
 */
module.exports.findEarlySlots = async (date) => {
    return new BookingCommand().findEarlySlots(date);
};

/**
 * Get Reschedule attempts by user Id
 * @param {String} userId - user Id
 * @returns {Promise<{err, data}>}
 */
module.exports.getRescheduleAttempts = async (userId) => {
    return new BookingCommand().getRescheduleAttempts(userId);
};

/**
 * Update or insert reschedule attempts
 * @param {Object} attempts - attempts object
 * @returns {Promise<{err, data}>}
 */
module.exports.upsertRescheduleAttempts = async (attempts) => {
    return new BookingCommand().upsertRescheduleAttempts(attempts);
};

/**
 * Get bookings within given time in minutes
 * @param {Object} time - time range
 * @returns {Promise<{err, data}>}
 */
module.exports.getBookings = async (time) => {
    return new BookingCommand().getBookings('now+' + time + 'm');
};

/**
 * Get tickets within given time in minutes
 * @param {Object} time - time range
 * @returns {Promise<{err, data}>}
 */
module.exports.getTicketsForTime = async (time) => {
    return new BookingCommand().getSolvedTickets('now+' + time + 'd');
};

/**
 * Get tickets within given time in minutes
 * @param {Object} time - time range
 * @returns {Promise<{err, data}>}
 */
module.exports.getSubmittedTicketsForTime = async (time) => {
    return new BookingCommand().getSubmittedTicketsForTime('now+' + time + 'd');
};
