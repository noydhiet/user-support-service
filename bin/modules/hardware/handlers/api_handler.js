/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 12/23/2019
 */

const Command = require('../repositories/commands/domain');
const {BadRequestError, ForbiddenError} = require('../../../helpers/error');
const command = new Command();
const wrapper = require('../../../helpers/utils/wrapper');

/**
 * Save modem reset status
 * @param {Object} data - modem reset snapshot info
 * @returns {Promise<{err: null, data: *}>}
 */
module.exports.resetModemStatus = async (data) => {
    return command.resetModemStatus(data);
};

module.exports.getModemStatus = async (indiHomeNum) => {
    return command.getModemStatus(indiHomeNum);
};
