/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 12/23/2019
 */

const wrapper = require('../../../../helpers/utils/wrapper');
const {ConflictError, ForbiddenError} = require('../../../../helpers/error');
const Command = require('./command');

/**
 * ES Support ticket
 */
class HardwareInfo {

    constructor() {
        this.command = new Command();
    }

    async resetModemStatus(data) {
        let result = await this.command.resetModemStatus(data);
        if (result.err) {
            throw new ForbiddenError('Save modem status Failed');
        }
        return wrapper.data(result.data);
    }

    async getModemStatus(indiHomeNum) {
        let result = await this.command.getModemStatus({indiHomeNum});
        if (result.err) {
            // no data found scenario
            if(result.err === 'failed to find data') {
                result.data = {};
                result.err = null;
                return wrapper.data(result.data);
            }
            return wrapper.error(new ConflictError('Failed to get modem status data'));
        }
        return wrapper.data(result.data);
    }
}

module.exports = HardwareInfo;
