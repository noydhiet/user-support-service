/*
 * Copyright (c) 2019. PCCW Global Pte Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by Nishani Fernando on 12/23/2019
 */

const config = require('../../../../config');
const elastic = require('../../../../helpers/databases/elasticsearch/db');

/**
 * ES functions for hardware related data management
 */
class Hardware {
    constructor() {
        this.elastic = elastic;
    }

    async resetModemStatus(resetData) {
        const data = {
            index: config.get('/elasticIndexes/hardware'),
            type: '_doc',
            id: resetData.indiHomeNum,
            body: resetData
        };
        return this.elastic.insertData(config.get('/elasticsearch'), data);
    }

    async getModemStatus(match) {
        const query = {
            index: config.get('/elasticIndexes/hardware'),
            type: '_doc',
            body: {
                query: {
                    match
                }
            }
        };
        return this.elastic.findOne(config.get('/elasticsearch'), query);
    }

}
module.exports = Hardware;
