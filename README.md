## User Support Service

[![pipeline status](https://gitlab.mihpccw.com/client-services/user-support-service/badges/development/pipeline.svg)](https://gitlab.mihpccw.com/client-services/user-support-service/commits/development)
[![coverage report](https://gitlab.mihpccw.com/client-services/user-support-service/badges/development/coverage.svg)](https://gitlab.mihpccw.com/client-services/user-support-service/commits/development)

###Configurations

Needs to setup the env for the local run. Sample dev env file is located.

Change the URL to AWS SonarQube

[![Quality Gate Status](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Auser-support-service&metric=alert_status)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Auser-support-service)
[![Code Smells](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Auser-support-service&metric=code_smells)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Auser-support-service)
[![Vulnerabilities](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Auser-support-service&metric=vulnerabilities)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Auser-support-service)
[![Duplicated Lines (%)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Auser-support-service&metric=duplicated_lines_density)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Auser-support-service)
[![Maintainability Rating](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Auser-support-service&metric=sqale_rating)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Auser-support-service)
[![Technical Debt](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Auser-support-service&metric=sqale_index)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Auser-support-service)
[![Lines of Code](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/api/project_badges/measure?project=myind%3Aclient-services%3Auser-support-service&metric=ncloc)](http://sonarqube-http-sonarqube.18.140.86.30.xip.io/dashboard?id=myind%3Aclient-services%3Auser-support-service)

Start Service
```
npm start
```
To view the Swagger UI interface:

```
open http://localhost:8080/docs
```
